<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>{{ config('app.name') }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/limitless_assets/js/main/jquery.min.js"></script>
    <script src="/limitless_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/limitless_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/limitless_assets/js/app.js"></script>
    <!-- /theme JS files -->

</head>

<body class="sign">


    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login form -->
                {{ Form::open(array('url' => 'login', 'class' => 'login-form')) }}
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <img src="/limitless_assets/images/LexMart_Logo_sm.png" class="mb-3 mt-1">
                                <!--i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i-->
                                <h5 class="mb-0">Login to your account</h5>
                                <span class="d-block text-muted">Enter your credentials below</span>
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                {{ Form::text('email', null, array('placeholder'=>'Email','class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '' ))) }}
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                                @if($errors->has('email'))
                                    @foreach ($errors->get('email') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                {{ Form::password('password', array('placeholder'=>'Password','class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                                @if($errors->has('password'))
                                    @foreach ($errors->get('password') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                            <div class="text-center">
                                <a href="{{route('password.request')}}">Forgot password?</a>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
                <!-- /login form -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</body>
</html>
