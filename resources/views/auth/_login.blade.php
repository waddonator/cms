<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>{{ config('app.name') }}</title>
    <link href="/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sb-admin.css" rel="stylesheet">
    <link href="/assets/css/common.css" rel="stylesheet">
</head>

<body class="bg-andro">
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header"><img src="/assets/img/favicon.png"> Login</div>
            <div class="card-body">
                {{ Form::open(array('url' => 'login')) }}
                    <div class="form-group">
                        {{ Form::label('email', 'Email address', []) }}
                        {{ Form::text('email', null, array('placeholder'=>'Enter email','class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '' ))) }}
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('password', 'Password', []) }}
                        {{ Form::password('password', array('placeholder'=>'Password','class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="form-group">

                        <div class="custom-control custom-checkbox">
                            {{ Form::checkbox('remember',  'remember' , null, ['id' => 'remember', 'class' => 'custom-control-input']) }}
                            {{ Form::label('remember', 'Remember Password', ['class' => 'custom-control-label']) }}
                        </div>

                    </div>
                    {{ Form::submit('Login', ['class' => 'btn btn-primary btn-block']) }}
                {{ Form::close() }}

                <div class="text-center">
                    <!--a class="d-block small mt-3" href="{{route('register')}}">Register an Account</a-->
                    <a class="d-block small" href="{{route('password.request')}}">Forgot Password?</a>
                </div>
            </div>
        </div>
    </div>

    <script src="/assets/lib/jquery/jquery.min.js"></script>
    <script src="/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/lib/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
