@extends('layouts.layout')

@section('content')
    <div class="card mb-3">
        <div class="card-header bg-dark text-white">{{$model->cause_num}}</div>
        <div class="card-body bg-light" style="white-space: pre-wrap;">{{$model->text}}</div>
        <div class="card-footer small text-muted font-italic text-right bg-dark">{{trans('table.updated',['date' => date("d.m.Y"), 'time' => date("H:i")])}}</div>
    </div>

@endsection
