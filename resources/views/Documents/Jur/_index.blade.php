@extends('layouts.layout')


@section('content')

    <div class="card mb-3">
        <div class="card-header">{{trans('main.permissions')}}
            @include('layouts.minimize')
        </div>
        <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead class="thead-dark table-bordered">
                            <tr>
                                <th>{{trans('table.permissions')}}</th>
                                <th>{{trans('table.actions')}}</th>
                            </tr>
                            <tr>
                                {!! Form::open(['method' => 'GET', 'route' => ['jurs.index'] ]) !!}
                                    <th>{{ Form::text('sname', null, array('class' => 'form-control form-control-sm')) }}</th>
                                    <th><button class="btn btn-sm btn-filter btn-primary" title="{{trans('buttons.get-filters')}}"><i class="fa fa-fw fa-filter"></i></button></th>
                                {!! Form::close() !!}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($models as $model)
                            <tr>
                                <td>{{ $model->name }}</td> 
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('jurs.edit', $model->id) }}" class="btn btn-info btn-sm" title="{{trans('buttons.edit')}}"><i class="fa fa-pencil fa-fw"></i></a>
                                        <a href="{{route('jurs.destroy', $model->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="{{trans('buttons.delete')}}"><i class="fa fa-trash-o fa-fw"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $models->appends($pagination_add)->links() }}

                    <a href="{{ route('jurs.create') }}" class="btn btn-success">{{trans('buttons.append')}}</a>
        </div>
        <div class="card-footer small text-muted font-italic text-right">{{trans('table.updated',['date' => date("d.m.Y"), 'time' => date("H:i")])}}</div>
    </div>

@endsection