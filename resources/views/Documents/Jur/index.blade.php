@extends('layouts.layout')

@section('content')
    <div class="card mb-3">
        <div class="card-header bg-dark text-white">Категорії справ</div>
        <div class="card-body bg-light">
                    <table class="table table-striped table-hover small">
                        <thead class="bg-secondary text-white table-bordered">
                            <tr>
                                <th>{{trans('table.key')}}</th>
                                <th>{{trans('table.num')}}</th>
                                <th>{{trans('table.adjudication_date')}}</th>
                                <th>{{trans('table.justice_kind')}}</th>
                                <th>{{trans('table.judgment_code')}}</th>
                                <th>{{trans('table.court_code')}}</th>
                                <th>{{trans('table.judge')}}</th>
                                <th>{{trans('table.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($models as $model)
                            <tr>
                                <td class="text-center">{{ $model->id }}</td>
                                <td>{{ $model->cause_num }}</td>
                                <td>{{ substr($model->adjudication_date,0,10) }}</td>
                                <td>{{ $model->justice_kind_name }}</td>
                                <td>{{ $model->judgment_code_name }}</td>
                                <td>{{ $model->court_code_id }}</td>
                                <td>{{ $model->judge }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('jurs.show', $model->id) }}" class="btn btn-info btn-sm" title="{{trans('buttons.show')}}"><i class="fa fa-eye fa-fw"></i></a>
                                        <a href="{{ route('jurs.edit', $model->id) }}" class="btn btn-info btn-sm" title="{{trans('buttons.edit')}}"><i class="fa fa-pencil fa-fw"></i></a>
                                        <a href="{{route('jurs.destroy', $model->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="{{trans('buttons.delete')}}"><i class="fa fa-trash-o fa-fw"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $models->links() }}

                    <a href="{{ route('cause-category.create') }}" class="btn btn-primary">{{trans('buttons.append')}}</a>

        </div>
        <div class="card-footer small text-muted font-italic text-right bg-dark">{{trans('table.updated',['date' => date("d.m.Y"), 'time' => date("H:i")])}}</div>
    </div>

@endsection