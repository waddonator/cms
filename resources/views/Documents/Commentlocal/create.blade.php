@extends('layouts.limitless-layout')

@section('content')

{{ Form::open(array('url' => 'documents/acts/comments/commentlocals', 'id' => 'creator')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
            <div class="card mb-3">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{trans('cms.title-parameters')}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{ Form::hidden('comment_id', $comment->id) }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('name', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('name'))
                                    @foreach ($errors->get('name') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('tocunit', trans('cms.column-tocunit'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('tocunit', $tocunits, null, array('class' => 'form-control'. ($errors->has('tocunit') ? ' is-invalid' : ''))) }}
                                @if($errors->has('tocunit'))
                                    @foreach ($errors->get('tocunit') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('content', trans('cms.column-content'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::textarea('content', null, ['class' => 'form-control tinymce' . ($errors->has('content') ? ' is-invalid' : '')]) }}
                        @if($errors->has('content'))
                            @foreach ($errors->get('content') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class='form-group'>
                        {{ Form::label('actversions', trans('cms.doc-actver'), ['class'=>'small text-muted font-italic']) }}<br>
                        @foreach ($actversions as $actversion)
                            <div class="custom-control custom-checkbox">
                                {{ Form::checkbox('actversions[]',  $actversion->id , null, ['id' => $actversion->id, 'class' => 'custom-control-input'. ($errors->has('actversions') ? ' is-invalid' : '' )]) }}
                                {{ Form::label($actversion->id, ucfirst($actversion->date_acc->format('Y-m-d')), ['class' => 'custom-control-label']) }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="small text-muted font-italic">{{trans('cms.column-export')}}</div>
                        <div class="form-group">
                            <select class="form-control form-control-sm" name="export">
                                <option value="false" selected>Нет</option>
                                <option value="true">Да</option>
                            </select>
                        </div>
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('comments.show',$comment->id) }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

@section('scripts')
<script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
<script>tinymce.init({selector:'.tinymce',height : "400",plugins: "code",});</script>
@endsection
