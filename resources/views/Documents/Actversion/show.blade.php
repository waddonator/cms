@extends('layouts.limitless-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Содержание</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                {{--@php(dump($model->get_metadata(2, $model->date_acc)))--}}
                <h3 class="text-center">{!! $model->act->atype->name !!}</h3>
                <h5 class="text-center">{!! $model->act->name !!}</h3>
                @can('admin')
                    @php(dump($model->get_metadata()))
                    @php(dump($model->get_content()))
                @else
                    @if ($model->format != 0)
                        <div>{!! $model->content !!}</div>
                    @else
                        <pre>{!! $model->content !!}</pre>
                    @endif
                @endcan
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Навигация</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">Создание:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">Изменение:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('actversions.edit',$model->id) }}" class="btn btn-light w-50" title="Редактирование"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('acts.show',$model->act_id) }}" class="btn btn-danger w-50" title="Выход"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

    <!--div class="col-lg-3">
        <div class="card mb-3">
            <div class="card-header">{{trans('table.actions')}}
                @include('layouts.minimize')
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="small text-muted font-italic">Created</div>
                    <p>{{$model->created_at}}</p>
                    <div class="small text-muted font-italic">Last updated</div>
                    <p>{{$model->updated_at}}</p>
                    <div class="btn-group" style="width: 100%;">
                        <a href="{{ route('actversions.edit', $model->id) }}" class="btn btn-light" title="{{trans('buttons.edit')}}" style="width:50%;" title="{{trans('buttons.save')}}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('acts.show',$model->act_id) }}" class="btn btn-danger" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="fa fa-sign-out fa-fw"></i></a>
                        <!--button type="button" class="btn btn-danger" onclick="window.history.back()" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="fa fa-sign-out fa-fw"></i></button-->
                    </div>
                </div>
            </div>
        </div>
    </div-->

</div>

@endsection

