@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('actversions.update', $model->id), 'method' => 'PUT')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Параметры</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {{ Form::label('date_acc', 'Дата принятия', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_acc', $model->date_acc, array('class' => 'form-control form-control-sm'. ($errors->has('date_acc') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_acc'))
                                    @foreach ($errors->get('date_acc') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {{ Form::label('modifier_id', 'Код документа об изменении', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('modifier_id', null, array('class' => 'form-control form-control-sm'. ($errors->has('modifier_id') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('modifier_id'))
                                    @foreach ($errors->get('modifier_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {{ Form::label('versionid', 'VersionId', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('versionid', null, array('class' => 'form-control form-control-sm'. ($errors->has('versionid') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('versionid'))
                                    @foreach ($errors->get('versionid') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                {{ Form::label('date_start', trans('cms.column-date_start'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_start', $model->date_start, array('class' => 'form-control form-control-sm'. ($errors->has('date_start') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_start'))
                                    @foreach ($errors->get('date_start') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                                <p class="small text-muted font-italic">(если не известно - оставьте поле пустым)</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {{ Form::label('date_end', trans('cms.column-date_end'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_end', $model->date_end, array('class' => 'form-control form-control-sm'. ($errors->has('date_end') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_end'))
                                    @foreach ($errors->get('date_end') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                                <p class="small text-muted font-italic">(если не известно - оставьте поле пустым)</p>
                            </div>
                        </div>
                        <div class="col-sm-4 @cannot('admin'){{'d-none'}}@endcan">
                            <div class="form-group">
                                {{ Form::label('format', 'Формат контента', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('format', $formats, null, array('class' => 'form-control form-control-sm'. ($errors->has('format') ? ' is-invalid' : ''))) }}
                                @if($errors->has('format'))
                                    @foreach ($errors->get('format') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        {{ Form::label('content', 'Содержание', ['class'=>'small text-muted font-italic']) }}
                        {{ Form::textarea('content', null, ['contentEditable' => 'true','class' => 'form-control' . ($model->format!=0 ? ' trumbowyg' : '') . ($errors->has('content') ? ' is-invalid' : '')]) }}
                        @if($errors->has('content'))
                            @foreach ($errors->get('content') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>

        </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Навигация</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">Создание:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">Изменение:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="small text-muted font-italic">{{trans('cms.column-export')}}</div>
                        <div class="form-group">
                            <select class="form-control" name="export">
                                <option value="false" @if(!$model->export){{'selected'}}@endif>Нет</option>
                                <option value="true" @if($model->export){{'selected'}}@endif>Да</option>
                            </select>
                        </div>
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="Сохранить"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('acts.show',$model->act_id) }}" class="btn btn-danger w-50" title="Выход"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

@section('styles')
<link rel="stylesheet" href="/assets/lib/trumbowyg/dist/ui/trumbowyg.min.css">
@endsection

@section('scripts')
<script src="/assets/lib/trumbowyg/dist/trumbowyg.min.js"></script>
<script src="/assets/lib/trumbowyg/plugins/myplugin/trumbowyg.myplugin.js"></script>
<script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector:'.tinymce',
        height : "400",
        plugins : 'code toc',
        //toolbar: 'code | undo redo | bold italic | alignleft aligncenter alignright alignjustify | ',
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.trumbowyg').trumbowyg({
            semantic: false,
            btns: [
                ['viewHTML'],
                ['undo', 'redo'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['formatting'],
                // ['strong', 'em', 'del'],
                ['link'],
                ['insertImage'],
                // ['unorderedList', 'orderedList'],
                // ['horizontalRule'],
                // ['removeformat'],
                ['myplugin'],
                ['fullscreen']
            ],
        });
    });
</script>
@endsection
