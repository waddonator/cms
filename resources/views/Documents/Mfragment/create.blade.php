@extends('layouts.layout')

@section('content')

{{ Form::open(array('url' => 'documents/monographs/mfragments', 'id' => 'creator')) }}

    <div class="row">
        <div class="col-lg-9">
            <div class="card mb-3">
                <div class="card-header">{{trans('main.parameters')}}
                    @include('layouts.minimize')
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::hidden('monograph_id', $monograph_id) }}
                        {{ Form::label('name', trans('table.name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('mnodetype_id', trans('table.mnodetype'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('mnodetype_id', $mnodetypes, null, array('class' => 'form-control form-control-sm'. ($errors->has('mnodetype_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('mnodetype_id'))
                                    @foreach ($errors->get('mnodetype_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('parent_id', trans('table.parent_id'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('parent_id', $parents, null, array('class' => 'form-control form-control-sm'. ($errors->has('parent_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('parent_id'))
                                    @foreach ($errors->get('parent_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('type', trans('table.type'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('type', null, array('class' => 'form-control form-control-sm'. ($errors->has('type') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('type'))
                                    @foreach ($errors->get('type') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('nr', trans('table.nr'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('nr', null, array('class' => 'form-control form-control-sm'. ($errors->has('nr') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('nr'))
                                    @foreach ($errors->get('nr') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card mb-3">
                <div class="card-header">{{trans('table.actions')}}
                    @include('layouts.minimize')
                </div>
                <div class="card-body">
                        <div class="btn-group" style="width: 100%;">
                            <button type="submit" id="submit" class="btn btn-success" style="width:50%;"title="{{trans('buttons.save')}}"><i class="fa fa-save fa-fw"></i></button>
                            <a href="{{ route('monographs.show',$monograph_id) }}" class="btn btn-danger btn-block" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="fa fa-sign-out fa-fw"></i></a>
                        </div>
                </div>
            </div>
        </div>

    </div>

{{ Form::close() }}

@endsection
