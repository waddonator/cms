@extends('layouts.limitless-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">
    <div class="order-2 order-md-1 w-100">
        <div class="card">
            @php(dump( json_decode($model->rmeta) ))
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-created')}}:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-updated')}}:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('racts.show',$model->ract->id) }}" class="btn btn-danger w-100" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

@endsection

