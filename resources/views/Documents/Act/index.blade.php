@extends('layouts.limitless-layout')

@section('content')
    <div class="card mb-3">

        <div class="card-header header-elements-inline">
            <h5 class="card-title">Акты</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>{{trans('cms.column-key')}}</th>
                            <th>{{trans('cms.column-type')}}</th>
                            <th>{{trans('cms.column-number')}}</th>
                            <th>{{trans('cms.column-name')}}</th>
                            <th>{{trans('cms.column-export')}}</th>
                            <th>{{trans('cms.column-actions')}}</th>
                        </tr>
                        <tr class="filter-line">
                            {!! Form::open(['method' => 'GET', 'route' => ['acts.index'], 'id'=>'filter-form' ]) !!}
                                <th>{{ Form::text('sid', isset($_GET['sid']) ? $_GET['sid'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>
                                    <select class="form-control form-control-sm" name="stype">
                                        @if(isset($types))
                                            @foreach($types as $key => $type)
                                                <option value="{{$key}}" @if(isset($_GET['stype']) && $_GET['stype']==$key){{'selected'}}@endif>{{$type}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </th>
                                <th>{{ Form::text('snumber', isset($_GET['snumber']) ? $_GET['snumber'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>
                                    <select class="form-control form-control-sm" name="sexport">
                                        <option value="0"></option>
                                        <option value="1" @if(isset($_GET['sexport']) && $_GET['sexport']==1){{'selected'}}@endif>Нет</option>
                                        <option value="2" @if(isset($_GET['sexport']) && $_GET['sexport']==2){{'selected'}}@endif>Да</option>
                                    </select>
                                </th>
                                <th>
                                    <div class="list-icons">
                                        <a href="#" class="list-icons-item text-primary-600 btn-filter" title="фильтровать"><i class="icon-filter3"></i></a>
                                    </div>
                                </th>
                            {!! Form::close() !!}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($models as $model)
                        <tr>
                            <td class="text-center">{{ $model->id }}</td>
                            <td class="text-center">{{ $model->atype->name }}</td>
                            <td class="text-center">{{ $model->number }}</td>
                            <td>{{ $model->name }}</td>
                            <td class="text-center">@if($model->export)<i class="icon-upload text-success"></i>@endif</td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="{{ route('acts.edit', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                                    <a href="{{ route('acts.show', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-versions')}}"><i class="icon-stack-text"></i></a>
                                    <a href="{{route('acts.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-trash" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex mt-3">
                <a href="{{ route('acts.create') }}" class="btn btn-success" title="{{trans('cms.btn-append')}}"><i class="icon-file-plus"></i></a>
                {{ $models->appends($pagination_add)->links() }}
            </div>
        </div>

    </div>

@endsection