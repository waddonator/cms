@extends('layouts.limitless-layout')

@section('content')
    <div class="card mb-3">

        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{ $data['subtitle'] OR ''}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

    @if($models->count() > 0)
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Код</th>
                            <th>Тип</th>
                            <th>Номер</th>
                            <th>Название</th>
                            <th>Действия</th>
                        </tr>
                        <tr class="filter-line">
                            {!! Form::open(['method' => 'GET', 'route' => ['act-trash'], 'id'=>'filter-form' ]) !!}
                                <th>{{ Form::text('sid', isset($_GET['sid']) ? $_GET['sid'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>
                                    <select class="form-control form-control-sm" name="stype">
                                        @if(isset($types))
                                            @foreach($types as $key => $type)
                                                <option value="{{$key}}" @if(isset($_GET['stype']) && $_GET['stype']==$key){{'selected'}}@endif>{{$type}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </th>
                                <th>{{ Form::text('snumber', isset($_GET['snumber']) ? $_GET['snumber'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>
                                    <div class="list-icons">
                                        <a href="#" class="list-icons-item text-primary-600 btn-filter" title="фильтровать"><i class="icon-filter3"></i></a>
                                    </div>
                                </th>
                            {!! Form::close() !!}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($models as $model)
                        <tr>
                            <td class="text-center">{{ $model->id }}</td>
                            <td class="text-center">{{ $model->atype->name }}</td>
                            <td class="text-center">{{ $model->number }}</td>
                            <td>{{ $model->name }}</td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="{{route('acts.destroy', $model->id)}}" class="list-icons-item btn-restore" title="восстановить" data-toggle="modal" data-target="#modal_restore"><i class="icon-redo"></i></a>
                                    @can('admin')
                                        <a href="{{route('acts.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-remove" title="удалить" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-x"></i></a>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex mt-3">
                {{ $models->appends($pagination_add)->links() }}
            </div>
        </div>
    @else
        <div class="card-body">
            <h5 class="text-center">Корзина пуста</h5>
        </div>
    @endif

    </div>

@endsection