@extends('layouts.limitless-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">
    <div class="order-2 order-md-1 w-100">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-stack-text"></i> Редакции</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h3 class="text-center">{!! $model->atype->name !!}</h3>
                <h5 class="text-center">{!! $model->name !!}</h5>
                <h3 class="text-center">Редакции:</h3>

                <div class="table-responsive mb-3">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{trans('cms.column-key')}}</th>
                                <th>{{trans('cms.column-actversion')}}</th>
                                <th>{{trans('cms.column-date')}}</th>
                                <th>{{trans('cms.column-date_start')}}</th>
                                <th>{{trans('cms.column-date_end')}}</th>
                                <th>{{trans('cms.column-export')}}</th>
                                <th>{{trans('cms.column-actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model->actversions->sortByDesc('date_acc') as $actversion)
                            <tr class="{{$actversion->trash != 0 ? 'text-muted' : ''}}">
                                <td class="text-center">{{$actversion->id}}</td>
                                <td class="text-center">{{$actversion->versionid}}</td>
                                <td class="text-center">{{$actversion->date_acc->format('Y-m-d')}}</td>
                                <td class="text-center">@if($actversion->date_start){{$actversion->date_start->format('Y-m-d')}}@endif</td>
                                <td class="text-center">@if($actversion->date_end){{$actversion->date_end->format('Y-m-d')}}@endif</td>
                                <td class="text-center">@if($actversion->export)<i class="icon-upload text-success"></i>@endif</td>
                                <td class="text-center">
                                    @if($actversion->trash==0)
                                    <div class="list-icons">
                                        <a href="{{ route('actversions.edit', $actversion->id) }}" class="list-icons-item" title="редактировать"><i class="icon-pencil7"></i></a>
                                        <a href="{{ route('actversions.show', $actversion->id) }}" class="list-icons-item" title="просмотр"><i class="icon-eye"></i></a>
                                        <a href="{{route('actversions.destroy', $actversion->id)}}" class="list-icons-item text-danger-600 btn-trash" title="удалить" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                    </div>
                                    @else
                                    <div class="list-icons">
                                        <a href="{{route('actversions.destroy', $actversion->id)}}" class="list-icons-item btn-restore" title="восстановить" data-toggle="modal" data-target="#modal_restore"><i class="icon-redo"></i></a>
                                        @can('admin')
                                            <a href="{{route('actversions.destroy', $actversion->id)}}" class="list-icons-item text-danger-600 btn-remove" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-x"></i></a>
                                        @endcan
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    
                <a href="{{ route('actversions.create', ['act_id'=> $model->id ]) }}" class="btn btn-success" title="Добавить"><i class="icon-file-plus"></i></a>
            </div>
        </div>
        @if(auth()->user()->can('admin') || auth()->user()->can('comments'))
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-comment"></i> Комментарии</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive mb-3">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{trans('cms.column-key')}}</th>
                                <th>{{trans('cms.column-name')}}</th>
                                <th>{{trans('cms.column-type')}}</th>
                                <th>{{trans('cms.column-commentlocals')}}</th>
                                <th>{{trans('cms.column-export')}}</th>
                                <th>{{trans('cms.column-actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model->comments as $comment)
                            <tr class="{{$comment->trash != 0 ? 'text-muted' : ''}}">
                                <td class="text-center">{{$comment->id}}</td>
                                <td class="text-center">{{$comment->name}}</td>
                                <td class="text-center">{{$comment->cdocsubtype->name}}</td>
                                <td class="text-center">{{$comment->commentlocals->count()}}</td>
                                <td class="text-center">@if($comment->export)<i class="icon-upload text-success"></i>@endif</td>
                                <td class="text-center">
                                    @if($comment->trash==0)
                                    <div class="list-icons">
                                        <a href="{{ route('comments.edit', $comment->id) }}" class="list-icons-item" title="редактировать"><i class="icon-pencil7"></i></a>
                                        <a href="{{ route('comments.show', $comment->id) }}" class="list-icons-item" title="{{trans('cms.btn-commentlocal')}}"><i class="icon-comment-discussion"></i></a>
                                        <a href="{{route('comments.destroy', $comment->id)}}" class="list-icons-item text-danger-600 btn-trash" title="удалить" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                    </div>
                                    @else
                                    <div class="list-icons">
                                        <a href="{{route('comments.destroy', $comment->id)}}" class="list-icons-item btn-restore" title="восстановить" data-toggle="modal" data-target="#modal_restore"><i class="icon-redo"></i></a>
                                        @can('admin')
                                            <a href="{{route('comments.destroy', $comment->id)}}" class="list-icons-item text-danger-600 btn-remove" title="удалить" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-x"></i></a>
                                        @endcan
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <a href="{{ route('comments.create', ['act_id'=> $model->id ]) }}" class="btn btn-success" title="Добавить"><i class="icon-file-plus"></i></a>
            </div>
        </div>
        @endcan
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-created')}}:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-updated')}}:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.doc-actver')}}:</span>
                            <div class="ml-auto">
                                <span class="badge badge-pill bg-warning-400">{{$model->actversions->where('trash','=',0)->count()}}</span>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.doc-com')}}:</span>
                            <div class="ml-auto">
                                <span class="badge badge-pill bg-primary-400">{{$model->comments->where('trash','=',0)->count()}}</span>
                            </div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('acts.edit', $model->id) }}" class="btn btn-light w-50" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('acts.index') }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

@endsection

