@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('publications.update', $model->id), 'method' => 'PUT')) }}

    <div class="d-flex align-items-start flex-column flex-md-row">
        <div class="order-2 order-md-1 w-100">

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{trans('cms.title-parameters')}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('abstracttext', trans('cms.column-desc'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::textarea('abstracttext', null, ['rows' => 3, 'class' => 'form-control' . ($errors->has('abstracttext') ? ' is-invalid' : '')]) }}
                        @if($errors->has('abstracttext'))
                            @foreach ($errors->get('abstracttext') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('content', trans('cms.column-content'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::textarea('content', null, ['class' => 'form-control tinymce' . ($errors->has('content') ? ' is-invalid' : '')]) }}
                        @if($errors->has('content'))
                            @foreach ($errors->get('content') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('subtitle', trans('cms.column-subtitle'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('subtitle', null, array('class' => 'form-control'. ($errors->has('subtitle') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('subtitle'))
                            @foreach ($errors->get('subtitle') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('pdocsubtype_id', trans('cms.column-subtype'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('pdocsubtype_id', $pdocsubtypes, null, array('class' => 'form-control'. ($errors->has('pdocsubtype_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('pdocsubtype_id'))
                                    @foreach ($errors->get('pdocsubtype_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('pglosstype_id', trans('cms.column-gloss'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('pglosstype_id', $pglosstypes, null, array('class' => 'form-control'. ($errors->has('pglosstype_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('pglosstype_id'))
                                    @foreach ($errors->get('pglosstype_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{trans('cms.title-sources')}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('psource_id', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('psource_id', $psources, null, array('class' => 'form-control'. ($errors->has('psource_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('psource_id'))
                                    @foreach ($errors->get('psource_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('novelty_date', trans('cms.column-date'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('novelty_date', $model->novelty_date, array('class' => 'form-control'. ($errors->has('novelty_date') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('novelty_date'))
                                    @foreach ($errors->get('novelty_date') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('year', trans('cms.column-year'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('year', null, array('class' => 'form-control'. ($errors->has('year') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('year'))
                                    @foreach ($errors->get('year') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('position', trans('cms.column-number'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('position', null, array('class' => 'form-control'. ($errors->has('position') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('position'))
                                    @foreach ($errors->get('position') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('issuefrom', trans('cms.column-page-start'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('issuefrom', null, array('class' => 'form-control'. ($errors->has('issuefrom') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('issuefrom'))
                                    @foreach ($errors->get('issuefrom') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('issueto', trans('cms.column-page-end'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('issueto', null, array('class' => 'form-control'. ($errors->has('issueto') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('issueto'))
                                    @foreach ($errors->get('issueto') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{trans('cms.title-authors')}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <div class='form-group'>
                        <div class="row">
                            @foreach ($pauthors as $pauthor)
                                <div class="col-sm-12 col-md-4">
                                    <div class="custom-control custom-checkbox mb-1">
                                        {{ Form::checkbox('pauthors[]',  $pauthor->id , null, ['id' => $pauthor->name, 'class' => 'custom-control-input'. ($errors->has('pauthors') ? ' is-invalid' : '' ) ]) }}
                                        {{ Form::label($pauthor->name, $pauthor->name, ['class' => 'custom-control-label']) }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if($errors->has('pauthors'))
                            @foreach ($errors->get('pauthors') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>

        </div>

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="small text-muted font-italic">{{trans('cms.column-created')}}</div>
                            <p>{{$model->created_at}}</p>
                            <div class="small text-muted font-italic">{{trans('cms.column-updated')}}</div>
                            <p>{{$model->updated_at}}</p>
                            <div class="small text-muted font-italic">{{trans('cms.column-export')}}</div>
                            <div class="form-group">
                                <select class="form-control" name="export">
                                    <option value="false" @if(!$model->export){{'selected'}}@endif>Нет</option>
                                    <option value="true" @if($model->export){{'selected'}}@endif>Да</option>
                                </select>
                            </div>
                            <div class="btn-group w-100">
                                <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                                <a href="{{ route('publications.index') }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /right sidebar component -->

    </div>

{{ Form::close() }}

@endsection

@section('scripts')
<script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
<script>tinymce.init({selector:'.tinymce',height : "400",plugins: "code",});</script>
@endsection
