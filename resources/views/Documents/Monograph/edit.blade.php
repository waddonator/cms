@extends('layouts.layout')

@section('content')

{{ Form::model($model, array('route' => array('monographs.update', $model->id), 'method' => 'PUT')) }}

    <div class="row">

        <div class="col-lg-9">
            <div class="card mb-3">
                <div class="card-header">{{trans('main.parameters')}}
                    @include('layouts.minimize')
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('table.name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('mauthor_id', trans('table.mauthor'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('mauthor_id', $mauthors, null, array('class' => 'form-control'. ($errors->has('mauthor_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('mauthor_id'))
                                    @foreach ($errors->get('mauthor_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {{ Form::label('novelty_date', trans('table.novelty_date'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('novelty_date', $model->novelty_date, array('class' => 'form-control'. ($errors->has('novelty_date') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('novelty_date'))
                                    @foreach ($errors->get('novelty_date') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card mb-3">
                <div class="card-header">{{trans('table.actions')}}
                    @include('layouts.minimize')
                </div>
                <div class="card-body">
                    <div class="small text-muted font-italic">Created</div>
                    <p>{{$model->created_at}}</p>
                    <div class="small text-muted font-italic">Last updated</div>
                    <p>{{$model->updated_at}}</p>
                    <div class="btn-group" style="width: 100%;">
                        <button type="submit" id="submit" class="btn btn-success" style="width:50%;"title="{{trans('buttons.save')}}"><i class="fa fa-save fa-fw"></i></button>
                        <a href="{{ route('monographs.index') }}" class="btn btn-danger btn-block" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="fa fa-sign-out fa-fw"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>

{{ Form::close() }}

@endsection
