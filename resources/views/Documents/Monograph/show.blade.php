@extends('layouts.layout')

@section('content')
<style type="text/css">
    .hpl-0{padding-left: 0;}
    .hpl-1{padding-left: 30px;}
    .hpl-2{padding-left: 60px;}
    .hpl-3{padding-left: 90px;}
</style>

<div class="row">
    <div class="col-lg-9">
        <div class="card mb-3">
            <div class="card-header">{{$model->name}}
                @include('layouts.minimize')
            </div>
            <div class="card-body">
                <h4 class="text-center">{!! $model->mauthor->name !!}</h3>
                <h2 class="text-center">{!! $model->name !!}</h3>
                <h5 class="text-center">Зміст</h3>
                <table class="small mb-3" style="width: 100%;">
                    @foreach($model->mfragments->sortby('id') as $mfragment)
                    <tr>
                        <td class="hpl-{{$mfragment->level}}">{{$mfragment->type}} {{$mfragment->nr}} {{$mfragment->name}}</td>
                        <td class="text-center">{{$mfragment->ordinal}}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('mfragments.edit', $mfragment->id) }}" class="btn btn-light btn-sm" title="{{trans('buttons.edit')}}"><i class="fa fa-pencil fa-fw"></i></a>
                                <a href="{{route('mfragments.destroy', $mfragment->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="{{trans('buttons.delete')}}"><i class="fa fa-trash-o fa-fw"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <a href="{{ route('mfragments.create', ['monograph_id'=> $model->id ]) }}" class="btn btn-primary">{{trans('buttons.append')}}</a>
            </div>
        </div>
    </div>

    <div class="col-lg-3"> <!-- start sidebar-->
        <div class="card mb-3">
            <div class="card-header">{{trans('table.actions')}}
                @include('layouts.minimize')
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="small text-muted font-italic">Created</div>
                    <p>{{$model->created_at}}</p>
                    <div class="small text-muted font-italic">Last updated</div>
                    <p>{{$model->updated_at}}</p>
                    <div class="btn-group" style="width: 100%;">
                        <a href="{{ route('monographs.edit', $model->id) }}" class="btn btn-light" title="{{trans('buttons.edit')}}" style="width:50%;" title="{{trans('buttons.save')}}"><i class="fa fa-pencil fa-fw"></i></a>
                        <a href="{{ route('monographs.index') }}" class="btn btn-danger" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="fa fa-sign-out fa-fw"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end sidebar-->

</div>

@endsection

