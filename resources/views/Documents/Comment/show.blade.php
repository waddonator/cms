@extends('layouts.limitless-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">
    <div class="order-2 order-md-1 w-100">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-comment-discussion"></i> {{trans('cms.doc-comloc')}}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h3 class="text-center">{{ $model->act->name }}</h3>
                <h3 class="text-center">{{ $model->name }}</h3>

                <div class="table-responsive mb-3">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{trans('cms.column-key')}}</th>
                                <th>{{trans('cms.column-name')}}</th>
                                <th>{{trans('cms.column-tocunit')}}</th>
                                <th>{{trans('cms.column-export')}}</th>
                                <th>{{trans('cms.column-actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($model->commentlocals->sortBy('id') as $commentlocal)
                            <tr class="{{$commentlocal->trash != 0 ? 'text-muted' : ''}}">
                                <td class="text-center">{{$commentlocal->nro}}</td>
                                <td>{{$commentlocal->name}}</td>
                                <td class="text-center">{{$commentlocal->tocunit}}</td>
                                <td class="text-center">@if($commentlocal->export)<i class="icon-upload text-success"></i>@endif</td>
                                <td class="text-center">
                                    @if($commentlocal->trash==0)
                                    <div class="list-icons">
                                        <a href="{{ route('commentlocals.edit', $commentlocal->id) }}" class="list-icons-item" title="редактировать"><i class="icon-pencil7"></i></a>
                                        <a href="{{ route('commentlocals.show', $commentlocal->id) }}" class="list-icons-item" title="просмотр"><i class="icon-eye"></i></a>
                                        <a href="{{route('commentlocals.destroy', $commentlocal->id)}}" class="list-icons-item text-danger-600 btn-trash" title="удалить" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                    </div>
                                    @else
                                    <div class="list-icons">
                                        <a href="{{route('commentlocals.destroy', $commentlocal->id)}}" class="list-icons-item btn-restore" title="{{trans('cms.btn-restore')}}" data-toggle="modal" data-target="#modal_restore"><i class="icon-redo"></i></a>
                                        @can('admin')
                                            <a href="{{route('commentlocals.destroy', $commentlocal->id)}}" class="list-icons-item text-danger-600 btn-remove" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-x"></i></a>
                                        @endcan
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <a href="{{ route('commentlocals.create', ['comment_id'=> $model->id ]) }}" class="btn btn-success" title="{{trans('cms.btn-append')}}"><i class="icon-file-plus"></i></a>
                @can('admin')
                    <div class="mb-3"></div>
                    @foreach($model->act->actversions->where('trash','=',false) as $key => $actversion)
                        @php(dump($model->get_metadata( $actversion->id )))
                    @endforeach
                @endcan
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                                    <span class="font-weight-semibold">{{trans('cms.column-created')}}:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                                    <span class="font-weight-semibold">{{trans('cms.column-updated')}}:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                        <li class="list-group-item">
                                    <span class="font-weight-semibold">{{trans('cms.column-commentlocals')}}:</span>
                            <div class="ml-auto">
                                <span class="badge badge-pill bg-blue-400">{{$model->commentlocals->where('trash','=',0)->count()}}</span>
                            </div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('comments.edit', $model->id) }}" class="btn btn-light w-50" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                            <a href="{{ route('acts.show',$model->act->id) }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

@endsection

