@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('comments.update', $model->id), 'method' => 'PUT')) }}

    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="order-2 order-md-1 w-100">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Параметры</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('csource_id', trans('cms.column-source'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('csource_id', $csources, null, array('class' => 'form-control form-control-sm'. ($errors->has('csource_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('csource_id'))
                                    @foreach ($errors->get('csource_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('cdocsubtype_id', trans('cms.column-subtype'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('cdocsubtype_id', $cdocsubtypes, null, array('class' => 'form-control form-control-sm'. ($errors->has('cdocsubtype_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('cdocsubtype_id'))
                                    @foreach ($errors->get('cdocsubtype_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('year', trans('cms.column-year'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('year', null, array('class' => 'form-control form-control-sm'. ($errors->has('year') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('year'))
                                    @foreach ($errors->get('year') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('issuefrom', trans('cms.column-issuefrom'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('issuefrom', null, array('class' => 'form-control form-control-sm'. ($errors->has('issuefrom') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('issuefrom'))
                                    @foreach ($errors->get('issuefrom') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('issueto', trans('cms.column-issueto'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('issueto', null, array('class' => 'form-control form-control-sm'. ($errors->has('issueto') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('issueto'))
                                    @foreach ($errors->get('issueto') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('position', trans('cms.column-position'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('position', null, array('class' => 'form-control form-control-sm'. ($errors->has('position') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('position'))
                                    @foreach ($errors->get('position') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('string', trans('cms.column-string'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('string', null, array('class' => 'form-control form-control-sm'. ($errors->has('string') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('string'))
                            @foreach ($errors->get('string') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                </div> <!-- /card-body -->
            </div>

                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{trans('cms.title-authors')}}</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body scroll-box">
                        <div class='form-group'>
                            @foreach ($cauthors as $cauthor)
                                <div class="custom-control custom-checkbox">
                                    {{ Form::checkbox('cauthors[]',  $cauthor->id , null, ['id' => $cauthor->name, 'class' => 'custom-control-input'. ($errors->has('cauthors') ? ' is-invalid' : '' ) ]) }}
                                    {{ Form::label($cauthor->name, $cauthor->name, ['class' => 'custom-control-label']) }}
                                </div>
                            @endforeach
                            @if($errors->has('cauthors'))
                                @foreach ($errors->get('cauthors') as $message)
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

        </div>
        <!-- /left content -->

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                            <ul class="list-group list-group-flush rounded-bottom">
                                <li class="list-group-item">
                                    <span class="font-weight-semibold">{{trans('cms.column-created')}}:</span>
                                    <div class="ml-auto">{{$model->created_at}}</div>
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-semibold">{{trans('cms.column-updated')}}:</span>
                                    <div class="ml-auto">{{$model->updated_at}}</div>
                                </li>
                                <li class="list-group-item">
                                    <span class="font-weight-semibold">{{trans('cms.column-commentlocals')}}:</span>
                                    <div class="ml-auto">
                                        <span class="badge badge-pill bg-blue-400">{{$model->commentlocals->count()}}</span>
                                    </div>
                                </li>
                            </ul>
                        <div class="card-body">
                            <div class="small text-muted font-italic">{{trans('cms.column-export')}}</div>
                            <div class="form-group">
                                <select class="form-control" name="export">
                                    <option value="false" @if(!$model->export){{'selected'}}@endif>Нет</option>
                                    <option value="true" @if($model->export){{'selected'}}@endif>Да</option>
                                </select>
                            </div>
                            <div class="btn-group w-100">
                                <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                                <a href="{{ route('acts.show',$model->act->id) }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /right sidebar component -->

    </div>

{{ Form::close() }}

@endsection

