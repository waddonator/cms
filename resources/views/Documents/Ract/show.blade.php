@extends('layouts.limitless-layout')

@section('content')
<div class="d-flex align-items-start flex-column flex-md-row">
    <div class="order-2 order-md-1 w-100">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><i class="icon-stack-text"></i> Редакции</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <h5 class="text-center">{!! $model->name !!}</h5>
                <h3 class="text-center">Редакции:</h3>

                <div class="table-responsive mb-3">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{trans('cms.column-key')}}</th>
                                <th>{{trans('cms.column-actversion')}}</th>
                                <th>{{trans('cms.column-date')}}</th>
                                <th>Статус</th>
                                <th>{{trans('cms.column-actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{--@foreach($model->ractversions->sortByDesc('versionid') as $ractversion)--}}
                            @foreach($ractversions as $ractversion)
                            <tr>
                                <td class="text-center">{{$ractversion->id}}</td>
                                <td class="text-center">{{$ractversion->versionid}}</td>
                                <td class="text-center">{{$ractversion->ed}}</td>
                                <td class="text-center">{{$ractversion->rbotstatus->name}}</td>
                                <td class="text-center">
                                    <div class="list-icons">
                                        <a href="{{ route('ractversions.rmeta', $ractversion->id) }}" class="list-icons-item" title="редактировать"><i class="icon-tree6"></i></a>
                                        <a href="{{ route('ractversions.jcontent', $ractversion->id) }}" class="list-icons-item" title="импортированный контент"><i class="icon-file-css"></i></a>
                                        <a href="{{ route('ractversions.show', $ractversion->id) }}" class="list-icons-item" title="контент"><i class="icon-file-text2"></i></a>
                                        <a href="{{ route('ractversions.rwork', $ractversion->id) }}" class="list-icons-item" title="контент для экспорта"><i class="icon-file-xml"></i></a>
                                        <a href="{{route('ractversions.destroy', $ractversion->id)}}" class="list-icons-item text-danger-600 btn-trash" title="удалить" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-created')}}:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-updated')}}:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.doc-actver')}}:</span>
                            <div class="ml-auto">
                                <span class="badge badge-pill bg-warning-400">{{$model->versionsCount()}}</span>
                            </div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <a href="{{ route('racts.index') }}" class="btn btn-danger w-100" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

@endsection

