@extends('layouts.limitless-layout')

@section('content')

{{ Form::open(array('url' => 'documents/acts', 'id' => 'creator')) }}

    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="order-2 order-md-1 w-100">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Параметры</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{ Form::hidden('sources', null, ['id'=>'json-sources', 'class' => 'form-control form-control-sm']) }}
                    {{ Form::hidden('keywords', null, ['id'=>'json-keywords', 'class' => 'form-control form-control-sm']) }}
                    <div class="form-group">
                        {{ Form::label('name', 'Название', ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('atype_id', 'Тип', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('atype_id', $types, null, array('class' => 'form-control form-control-sm'. ($errors->has('atype_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('atype_id'))
                                    @foreach ($errors->get('atype_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('number', 'Номер', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('number', null, array('class' => 'form-control form-control-sm'. ($errors->has('number') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('number'))
                                    @foreach ($errors->get('number') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                {{ Form::label('asubact_id', 'Подтип', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::select('asubact_id', $asubacts, null, array('class' => 'form-control form-control-sm'. ($errors->has('asubact_id') ? ' is-invalid' : ''))) }}
                                @if($errors->has('asubact_id'))
                                    @foreach ($errors->get('asubact_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                {{ Form::label('date_acc', 'Дата принятия', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_acc', null, array('class' => 'form-control form-control-sm'. ($errors->has('date_acc') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_acc'))
                                    @foreach ($errors->get('date_acc') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                {{ Form::label('date_force', 'Дата вступления в силу', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_force', null, array('class' => 'form-control form-control-sm'. ($errors->has('date_force') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_force'))
                                    @foreach ($errors->get('date_force') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                {{ Form::label('reestr_code', 'Код в реестре', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('reestr_code', null, array('class' => 'form-control form-control-sm'. ($errors->has('reestr_code') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('reestr_code'))
                                    @foreach ($errors->get('reestr_code') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                {{ Form::label('reestr_date', 'Дата внесения в реестр', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('reestr_date', null, array('class' => 'form-control form-control-sm'. ($errors->has('reestr_date') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('reestr_date'))
                                    @foreach ($errors->get('reestr_date') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                {{ Form::label('reg_date', 'Дата в МинЮсте', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('reg_date', null, array('class' => 'form-control form-control-sm'. ($errors->has('reg_date') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('reg_date'))
                                    @foreach ($errors->get('reg_date') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {{ Form::label('reg_number', 'Номер в МинЮсте', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('reg_number', null, array('class' => 'form-control form-control-sm'. ($errors->has('reg_number') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('reg_number'))
                                    @foreach ($errors->get('reg_number') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {{ Form::label('finisher_id', 'Код док. приостановления', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::number('finisher_id', null, array('class' => 'form-control form-control-sm'. ($errors->has('finisher_id') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('finisher_id'))
                                    @foreach ($errors->get('finisher_id') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                {{ Form::label('finish_date', 'Дата приостановления', ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('finish_date', null, array('class' => 'form-control form-control-sm'. ($errors->has('finish_date') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('finish_date'))
                                    @foreach ($errors->get('finish_date') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div> <!-- /card-body -->
            </div>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Издатели</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body scroll-box">
                    <div class='form-group'>
                        @foreach ($publishers as $publisher)
                            <div class="custom-control custom-checkbox">
                                {{ Form::checkbox('publishers[]',  $publisher->id , null, ['id' => $publisher->name, 'class' => 'custom-control-input'. ($errors->has('publishers') ? ' is-invalid' : '' ) ]) }}
                                {{ Form::label($publisher->name, $publisher->name, ['class' => 'custom-control-label']) }}
                            </div>
                        @endforeach
                        @if($errors->has('publishers'))
                            @foreach ($errors->get('publishers') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Издания</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped small">
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Дата</th>
                                <th>Номер журнала</th>
                                <th>Номер статьи</th>
                                <th>Действие</th>
                            </tr>
                        </thead>
                        <tbody class="body-sources">
                        </tbody>
                    </table>
                    <a href="#" class="btn btn-success mt-3" data-toggle="modal" data-target="#appendModal"><i class="icon-file-plus"></i></a>
                </div>
            </div>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Ключевые слова</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="small w-100 table-striped">
                        <tbody class="body-keywords">
                        </tbody>
                    </table>
                    <a href="#" class="btn btn-success mt-3" data-toggle="modal" data-target="#appendwordModal"><i class="icon-file-plus"></i></a>
                </div>
            </div>

        </div>
        <!-- /left content -->


        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">Навигация</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                                <div class="small text-muted font-italic">{{trans('cms.column-export')}}</div>
                                <div class="form-group">
                                    <select class="form-control" name="export">
                                        <option value="false" selected>Нет</option>
                                        <option value="true">Да</option>
                                    </select>
                                </div>
                                <div class="btn-group" style="width: 100%;">
                                    <button type="submit" id="submit" class="btn btn-success" style="width:50%;" title="Сохранить"><i class="icon-floppy-disk"></i></button>
                                    <!--button type="button" class="btn btn-danger" onclick="window.history.back()" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="icon-esc"></i></button-->
                                    <a href="{{ route('acts.index') }}" class="btn btn-danger btn-block" style="width:50%;" title="Выход"><i class="icon-esc"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /right sidebar component -->

    </div>

{{ Form::close() }}

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var sources = $.parseJSON(("{{ $sources }}").replace(/&quot;/g,'"'));
        decode_source();
        $(document).delegate( '.remove-source', 'click', function(e) {
            e.preventDefault();
            $(this).parents('tr').remove();
        });
        $(".append-source").click(function(){
            temp = '<tr class="element-source" data-id="'+$('.list-sources').val()+'"><td>'+$('.list-sources option:selected').text()+'</td><td><input class="form-control form-control-sm f1" type="date"></td><td><input class="form-control form-control-sm f2" type="number"></td><td><input class="form-control form-control-sm f3" type="number"></td><td class="text-center"><a href="#" class="text-danger-600 remove-source"><i class="icon-trash"></i></a></td></tr>';
            $('.body-sources').append( temp );
        });

        $(".append-keyword").click(function(){
            temp = '<tr class="element-keyword"><td class="word">'+$('#new-keyword').val()+'</td><td class="text-center"><a href="#" class="text-danger-600 remove-source"><i class="icon-trash"></i></a></td></tr>';
            $('.body-keywords').append( temp );
        });

        $("#submit").click(function(){
            $('#json-sources').val(encode_source());
            $('#json-keywords').val(encode_keywords());
        });

        function encode_source(){
            obj = {}
            $('.element-source').each(function( index ){
                var str = $( this ).attr('data-id');
                obj[str] = {'f1':$( this ).find('.f1').first().val(), 'f2':$( this ).find('.f2').first().val(), 'f3':$( this ).find('.f3').first().val()}
            });
            return JSON.stringify(obj);
        }

        function decode_source(){
            if ($('#json-sources').val() != '') {obj = $.parseJSON($('#json-sources').val());}
            if(typeof obj =='object')
            {
                console.log(obj);
                $.each( obj, function( key, value ) {
                    console.log(value);
                    temp = '<tr class="element-source" data-id="'+key+'"><td>'+sources[key]+'</td>';
                    temp += '<td><input class="form-control form-control-sm f1" type="date" value="'+value.f1+'"></td>';
                    temp += '<td><input class="form-control form-control-sm f2" type="number" value="'+value.f2+'"></td>';
                    temp += '<td><input class="form-control form-control-sm f3" type="number"  value="'+value.f3+'"></td>';
                    temp += '<td class="text-center"><a href="#" class="text-danger-600 remove-source"><i class="icon-trash"></i></a></td></tr>';
                    $('.body-sources').append( temp );
                });
            }
        }

        function encode_keywords(){
            obj = []
            $('.element-keyword').each(function( index ){
                obj.push( $( this ).find('.word').first().text() );
            });
            return JSON.stringify(obj);
        }

        var search = document.querySelector('#new-keyword');
        var results = document.querySelector('#searchresults');
        var templateContent = document.querySelector('#resultstemplate').content;
        $('#new-keyword').keyup(function(){
            while (results.children.length) results.removeChild(results.firstChild);
            var inputVal = new RegExp(search.value.trim(), 'i');
            var clonedOptions = templateContent.cloneNode(true);
            var set = Array.prototype.reduce.call(clonedOptions.children, function searchFilter(frag, el) {
                if (inputVal.test(el.textContent) && frag.children.length < 5) frag.appendChild(el);
                return frag;
            }, document.createDocumentFragment());
            results.appendChild(set);
        });

    });
</script>
@endsection

@section('modals')
    <div class="modal fade" id="appendModal" tabindex="-1" role="dialog" aria-labelledby="appendModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title" id="appendModalLabel">Добавление издания</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                </div>
                <div class="modal-body">
                    <select class="form-control form-control-sm list-sources">
                        @foreach($sources as $key => $source)
                            <option value="{{$key}}">{{$source}}</option>
                        @endforeach
                    </select>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-primary append-source" type="button" data-dismiss="modal">Добавить</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="appendwordModal" tabindex="-1" role="dialog" aria-labelledby="appendwordModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary text-white">
                    <h5 class="modal-title" id="appendwordModalLabel">Добавление ключевого слова</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                </div>
                <div class="modal-body">
                    <template id="resultstemplate">
                        @foreach($akeywords as $key => $akeyword)
                            <option>{{$akeyword}}</option>
                        @endforeach
                    </template>
                    <input type="text" id="new-keyword" list="searchresults" autocomplete="off" class="form-control form-control-sm">
                    <datalist id="searchresults"></datalist>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-primary append-keyword" type="button" data-dismiss="modal">Добавить</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection
