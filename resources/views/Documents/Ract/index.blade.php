@extends('layouts.limitless-layout')

@section('content')
    <div class="card mb-3">

        <div class="card-header header-elements-inline">
            <h5 class="card-title">Акты</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>{{trans('cms.column-key')}}</th>
                            <th>Nreg</th>
                            <th>Тип</th>
                            <th>Подтип</th>
                            <th>{{trans('cms.column-name')}}</th>
                            <th>Редакций</th>
                            <th>Статус</th>
                            <th>{{trans('cms.column-actions')}}</th>
                        </tr>
                        <tr class="filter-line">
                            {!! Form::open(['method' => 'GET', 'route' => ['racts.index'], 'id'=>'filter-form' ]) !!}
                                <th>{{ Form::text('sid', isset($_GET['sid']) ? $_GET['sid'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>{{ Form::text('snreg', isset($_GET['snreg']) ? $_GET['snreg'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th></th>
                                <th></th>
                                <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th></th>
                                <th></th>
                                <th>
                                    <div class="list-icons">
                                        <a href="#" class="list-icons-item text-primary-600 btn-filter" title="фильтровать"><button><i class="icon-filter3"></i></button></a>
                                    </div>
                                </th>
                            {!! Form::close() !!}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($models as $model)
                        <tr>
                            <td class="text-center">{{ $model->id }}</td>
                            <td class="text-center">{{ $model->nreg }}</td>
                            <td class="text-center">{{ $model->typs() }}</td>
                            <td class="text-center">{{ $model->rsubact() }}</td>
                            <td title="{{$model->hint}}">{{ $model->shortname }}</td>
                            <td class="text-center">{{ $model->versionsCount() }}</td>
                            <td class="text-center status{{$model->rbotstatus_id}}">{{$model->rbotstatus->name}}</td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="{{ route('racts.rmeta', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-edit')}}"><i class="icon-tree6"></i></a>
                                    <a href="{{ route('racts.show', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-versions')}}"><i class="icon-stack-text"></i></a>
                                    <a href="{{ route('racts.repllink', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-edit')}}"><i class="icon-link2"></i></a>
                                    <!--a href="{{route('racts.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-trash" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a-->
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex mt-3">
                {{ $models->appends($pagination_add)->links() }}
            </div>
        </div>

    </div>

@endsection