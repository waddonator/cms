@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('racts.update', $model->id), 'method' => 'PUT')) }}

<div class="d-flex align-items-start flex-column flex-md-row">
    <div class="order-2 order-md-1 w-100">
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Параметры</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{trans('cms.column-key')}}</th>
                                <th>Nreg</th>
                                <th>Anchor</th>
                                <th>Tocunit</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{--@if($model->repllink)--}}
                                @foreach($model->repllinks as $key => $repllink)
                                    <tr>
                                        <td>{{$repllink->id}}</td>
                                        <td>{{$repllink->nreg}}</td>
                                        <td>{{$repllink->anchor}}</td>
                                        <td><select name="elements[{{$repllink->id}}]" class="form-control">
                                            <option value=""></option>
                                            @foreach($tocunits as $key2 => $tocunit)
                                                <option value="{{$tocunit}}">{{$tocunit}}</option>
                                            @endforeach
                                        </select></td>
                                    </tr>
                                @endforeach
                            {{--@endif--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush rounded-bottom">
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-created')}}:</span>
                            <div class="ml-auto">{{$model->created_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.column-updated')}}:</span>
                            <div class="ml-auto">{{$model->updated_at}}</div>
                        </li>
                        <li class="list-group-item">
                            <span class="font-weight-semibold">{{trans('cms.doc-actver')}}:</span>
                            <div class="ml-auto">
                                <span class="badge badge-pill bg-warning-400">{{$model->versionsCount()}}</span>
                            </div>
                        </li>
                    </ul>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('racts.index') }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

