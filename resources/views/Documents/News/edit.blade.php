@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('news.update', $model->id), 'method' => 'PUT')) }}

    <div class="d-flex align-items-start flex-column flex-md-row">
        <div class="order-2 order-md-1 w-100">

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{trans('cms.title-parameters')}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('date_effective', trans('cms.column-effective'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_effective', $model->date_effective, array('class' => 'form-control'. ($errors->has('date_effective') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_effective'))
                                    @foreach ($errors->get('date_effective') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::label('date_event', trans('cms.column-event'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::date('date_event', $model->date_event, array('class' => 'form-control'. ($errors->has('date_event') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('date_event'))
                                    @foreach ($errors->get('date_event') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('content', trans('cms.column-content'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::textarea('content', null, ['class' => 'form-control tinymce' . ($errors->has('content') ? ' is-invalid' : '')]) }}
                        @if($errors->has('content'))
                            @foreach ($errors->get('content') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">{{trans('cms.title-ncats')}}</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    <div class='form-group'>
                        <div class="row">
                            @foreach ($ncats as $ncat)
                                <div class="col-sm-12 col-md-4">
                                    <div class="custom-control custom-checkbox mb-1">
                                        {{ Form::checkbox('ncats[]',  $ncat->id , null, ['id' => $ncat->name, 'class' => 'custom-control-input'. ($errors->has('ncats') ? ' is-invalid' : '' ) ]) }}
                                        {{ Form::label($ncat->name, $ncat->name, ['class' => 'custom-control-label']) }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @if($errors->has('ncats'))
                            @foreach ($errors->get('ncats') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>

                </div>
            </div>

        </div>

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">{{trans('cms.title-navigation')}}</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="small text-muted font-italic">{{trans('cms.column-created')}}</div>
                            <p>{{$model->created_at}}</p>
                            <div class="small text-muted font-italic">{{trans('cms.column-updated')}}</div>
                            <p>{{$model->updated_at}}</p>
                            <div class="small text-muted font-italic">{{trans('cms.column-export')}}</div>
                            <div class="form-group">
                                <select class="form-control" name="export">
                                    <option value="false" @if(!$model->export){{'selected'}}@endif>Нет</option>
                                    <option value="true" @if($model->export){{'selected'}}@endif>Да</option>
                                </select>
                            </div>
                            <div class="btn-group w-100">
                                <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                                <a href="{{ route('news.index') }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /right sidebar component -->

    </div>

{{ Form::close() }}

@endsection

@section('scripts')
<script type="text/javascript" src="/assets/lib/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'.tinymce', height : "400" });</script>
@endsection
