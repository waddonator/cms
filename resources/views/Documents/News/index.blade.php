@extends('layouts.limitless-layout')

@section('content')
    <div class="card mb-3">

        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('cms.doc-news')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>{{trans('cms.column-key')}}</th>
                            <th>{{trans('cms.column-name')}}</th>
                            <th>{{trans('cms.column-categories')}}</th>
                            <th>{{trans('cms.column-actions')}}</th>
                        </tr>
                        <tr class="filter-line">
                            {!! Form::open(['method' => 'GET', 'route' => ['news.index'], 'id'=>'filter-form' ]) !!}
                                <th>{{ Form::text('snro', isset($_GET['snro']) ? $_GET['snro'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                                <th></th>
                                <th>
                                    <div class="list-icons">
                                        <button><a href="#" class="list-icons-item text-primary-600 btn-filter" title="фильтровать"><i class="icon-filter3"></i></a></button>
                                    </div>
                                </th>
                            {!! Form::close() !!}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($models as $model)
                        <tr>
                            <td class="text-center">{{ $model->nro }}</td>
                            <td>{{ $model->name }}</td>
                            <td>{{ $model->ncats()->pluck('name')->implode(', ') }}</td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="{{ route('news.edit', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                                    <a href="{{ route('news.show', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-view')}}"><i class="icon-eye"></i></a>
                                    <a href="{{route('news.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-trash" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex mt-3">
                <a href="{{ route('news.create') }}" class="btn btn-success" title="{{trans('cms.btn-append')}}"><i class="icon-file-plus"></i></a>
                {{ $models->appends($pagination_add)->links() }}
            </div>
        </div>
    </div>

@endsection