@extends('layouts.limitless-layout')

@section('content')

    <h1>Меняем ссылки</h1>
    <button id="start">Start</button> <button id="stop">Stop</button>

    <textarea id="log" class="w-100 mb-3 form-control form-control-sm text-success bg-dark" style="height: 500px;"></textarea>
@endsection

@section('scripts')
{{--<script src="/assets/js/chapel.min.js"></script>
<script src="/assets/js/redactor.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#get-text').click(function(){
            //$('.source').val('111');
            chapel.init(chapel);
            var t=$(".source").val().replace(/<\/?par[^>]*>/gi,""),e=/(\<\w*)((\s\/\>)|(.*\<\/\w*\>))*/gm.test(t),a=e?t:chapel.text.normalize(t);
            //$('.source').val(t);
        });
    });
</script>--}}
<script type="text/javascript">
    $(document).ready(function(){
        var ajax = false;
        var free = true;
        var time1 = setInterval(function() {
            function1();
        }, 2000);

        $("#start").click(function(){
            ajax = true;
        });
        $("#stop").click(function(){
            ajax = false;
        });

        function function1(){
            if (ajax){
                if (free){
                    $.ajax({
                        url: 'http://cms.activelex.com/api/changelinks',
                        type: 'GET',
                        dataType : "json",
                        beforeSend: function() {
                            free = false;
                        },
                        success: function (data) {
                            $('#log').prepend(data.result + ' ');
                            free = true;
                        },
                        error: function(){
                            $("#log").prepend('! ');
                            free = true;
                        }
                    });

                } else {
                    $("#log").prepend('. ');
                }
            }
        }

    });
</script>
@endsection
