@extends('layouts.limitless-layout')

@section('content')

    <h1>TOC</h1>
    <div class="redactor"></div>

@endsection

@section('scripts')
<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script-->
<script src="/assets/js/chapel.min.js"></script>
<script src="/assets/js/redactor.js"></script>
{{--<script type="text/javascript">
    $(document).ready(function(){
        $('#get-text').click(function(){
            //$('.source').val('111');
            chapel.init(chapel);
            var t=$(".source").val().replace(/<\/?par[^>]*>/gi,""),e=/(\<\w*)((\s\/\>)|(.*\<\/\w*\>))*/gm.test(t),a=e?t:chapel.text.normalize(t);
            //$('.source').val(t);
        });
    });
</script>--}}
@endsection
