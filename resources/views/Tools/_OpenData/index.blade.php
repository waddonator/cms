@extends('layouts.layout')

@section('content')
    <h1>Импорт данных с сервера OpenData</h1>
    {{--
    <p>{{$method OR ''}}</p>    

        <form method="POST" action="{{ route('opendata') }}">
            @csrf
            <button>111</button>
            <p></p>
        </form>
    @if($method=='POST')
        @php(dump($object))
    @endif
    --}}
<style type="text/css">
    .console{
        max-height: 500px;
        overflow-y: scroll;
        padding: 30px;
        background-color: #18171B;
        font-weight: bold;
        color: #56DB3A;
        line-height: 1.2em;
        font: 12px Menlo, Monaco, Consolas, monospace;
        word-wrap: break-word;
        white-space: pre-wrap;
        position: relative;
        z-index: 99999;
        word-break: break-all;
    }
    .console-block{
        margin-bottom:15px;
    }
    .ajax-loader{
        margin-top: 30px;
    }
</style>

    <div class="card mb-3">
        <div class="card-header bg-dark text-white">Сканирование базы судебных решений OpenData</div>
        <div class="card-body bg-light">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="count" class="small">Количество элементов</label>
                        <input class="form-control form-control-sm" type="number" name="count" id="count" value="1000">
                    </div>
                    <div class="form-group">
                        <label for="iteration" class="small">Итерация</label>
                        <input class="form-control form-control-sm" type="number" name="iteration" id="iteration" value="0">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="date_start" class="small">Дата начала сканирования</label>
                        <input class="form-control form-control-sm" type="text" name="date_start" id="date_start" value="{{now()->format("Y-m-d")}}">
                    </div>
                    <div class="form-group">
                        <label for="type" class="small">Тип сканирования</label>
                        <select class="form-control form-control-sm" name="type" id="type">
                            <option value="0">Только конкретная дата</option>
                            <option value="1">Начиная с указанной даты</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <button id="scan" class="btn btn-primary">Начать сканирование</button>
                    <button id="clear" class="btn btn-secondary">Очистить</button>
                    <img class="ajax-loader" src="/assets/img/ajax-loader.gif" style="display: none;">
                </div>
            </div>
            
            <pre class="console">Сканирование базы судебных решений OpenData</pre>
        </div>
        <div class="card-footer small text-muted font-italic text-right bg-dark">{{trans('table.updated',['date' => date("d.m.Y"), 'time' => date("H:i")])}}</div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var now = new Date();
        console.log( now );
        $("#scan").click(function(){
            scan();
        });
        $("#clear").click(function(){
            $(".console").empty();
        });


        function scan(){
            param = {count:$("#count").val(), iteration:$("#iteration").val(), date_start:$("#date_start").val()};
            $.ajax({
                beforeSend: function() {
                    //$(".console").append('<div class="console-block">Start</div>');
                    $("#scan").addClass('disabled');
                    $(".ajax-loader").show();
                },
                type: 'POST',
                url: '{{ route("opendata-scan") }}',
                dataType: 'json',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data: (param),
                success: function (result) {
                    temp = '<div class="console-block">';
                    $(".console").append();
                    $.each( result, function( key, value ) {
                        temp += key + ": " + value + "<br>";
                    });
                    temp += '</div>';
                    $(".console").prepend(temp);
                    iteration:$("#iteration").val(result.next_iteration);
                    date_start:$("#date_start").val(result.next_date);
                    if (result.next_date == result.date_start || $("#type").val() == 1){
                        scan();
                    } else {
                        $("#scan").removeClass('disabled');
                        $(".ajax-loader").hide();
                    }
                },
                error: function (result) {
                    //$(".console").append('<div class="console-block">Ajax error</div>');
                }
            }).always(function() {
                //$(".console").append('<div class="console-block">Complete</div>');
            });
        };
    });
</script>
@endsection
