@extends('layouts.limitless-layout')

@section('content')
<div class="card mb-3">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">{{ $data['title'] OR ''}}</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Код</th>
                        <th>Название</th>
                        <th>Тип</th>
                        <th>Множитель</th>
                        <th>Моделей</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($models as $model)
                    {{ Form::open(array('route' => 'export')) }}
                        {{ Form::hidden('id', $model->id) }}
                        <tr>
                            <td class="text-center">{{ $model->id }}</td>
                            <td>{{ $model->name }}</td>
                            <td>{{ $model->vocabulary_production_type }}</td>
                            <td class="text-center">{{ $model->factor }}</td>
                            {{-- $model->exportvocmodels()->pluck('name')->implode(', ') --}}
                            <td class="text-center">{{ $model->exportvocmodels->count() }}</td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="#" class="list-icons-item btn-exportvoc" title="{{trans('cms.btn-export')}}" data-toggle="modal" data-target="#modal_exportvoc" data-id="{{$model->id}}"><i class="icon-upload4"></i></a>
                                    <a href="{{ route('exportvocs.edit', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                                    <a href="{{route('exportvocs.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-trash" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    {{ Form::close() }}
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex mt-3">
            <a href="{{ route('exportvocs.create') }}" class="btn btn-success" title="{{ trans('cms.btn-create') }}"><i class="icon-file-plus"></i></a>
            {{ $models->appends($pagination_add)->links() }}
        </div>
    </div>
</div>

@endsection

@section('modals')
@include('template-parts.modal-exportvoc')
@endsection
