@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('exportvocs.update', $model->id), 'method' => 'PUT')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('cms.title-parameters')}}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    {{ Form::label('name', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                    {{ Form::text('name', null, array('class' => 'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                    @if($errors->has('name'))
                        @foreach ($errors->get('name') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
                <div class="form-group">
                    {{ Form::label('vocabulary_production_type', trans('cms.column-polishtype'), ['class'=>'small text-muted font-italic']) }}
                    {{ Form::text('vocabulary_production_type', null, array('class' => 'form-control form-control-sm'. ($errors->has('vocabulary_production_type') ? ' is-invalid' : '' ))) }}
                    @if($errors->has('vocabulary_production_type'))
                        @foreach ($errors->get('vocabulary_production_type') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('factor', trans('cms.column-factor'), ['class'=>'small text-muted font-italic']) }}
                            {{ Form::number('factor', null, array('class' => 'form-control form-control-sm'. ($errors->has('factor') ? ' is-invalid' : '' ))) }}
                            @if($errors->has('factor'))
                                @foreach ($errors->get('factor') as $message)
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {{ Form::label('type', trans('cms.column-type'), ['class'=>'small text-muted font-italic']) }}
                            {{ Form::text('type', null, array('class' => 'form-control form-control-sm'. ($errors->has('type') ? ' is-invalid' : '' ))) }}
                            @if($errors->has('type'))
                                @foreach ($errors->get('type') as $message)
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Навигация</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('exportvocs.index') }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection
