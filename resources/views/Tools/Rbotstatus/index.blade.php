@extends('layouts.limitless-layout')

@section('content')
<div class="card mb-3">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">{{ $data['title'] OR ''}}</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>
    <div class="card-body">

        <table class="table table-bordered table-striped mb-3" id="datatable">
            <thead>
                <tr>
                    <th>{{trans('cms.column-key')}}</th>
                    <th>{{trans('cms.column-name')}}</th>
                    <th>{{trans('cms.column-actions')}}</th>
                </tr>
            </thead>
        </table>

        <div class="d-flex mt-3">
            <a href="{{ route('rbotstatuses.create') }}" class="btn btn-success" title="{{ trans('cms.btn-create') }}"><i class="icon-file-plus"></i></a>
            {{-- $models->appends($pagination_add)->links() --}}
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="/limitless_assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script>
    var table = $("#datatable").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": 25,
        "ajax": {
            "url": "{{route('rbotstatuses.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}" },
        },
        "columns":[
            {"data":"id"},
            {"data":"name"},
            {"data":"actions","searchable":false,"orderable":false}
        ],
        'columnDefs': [
            {
                "targets": 0,
                "className": "text-center",
            },
            {
                "targets": 2,
                "className": "text-center",
            },
        ],
        "language": {
            "url": "/limitless_assets/js/plugins/tables/datatables/languages/russian.json"
        },
        // "buttons": [
        //     'copy', 'excel', 'pdf'
        // ]        
    });
</script>
@endsection