@extends('layouts.limitless-layout')

@section('content')
<div class="row">
    <div class="col-md-6 col-xl-3">@include('Widgets.reincrement')</div>
    <div class="col-md-6 col-xl-3">@include('Widgets.lexinform')</div>
    <div class="col-md-6 col-xl-3">@include('Widgets.blockmap')</div>
    <div class="col-md-6 col-xl-3">@include('Widgets.relations')</div>
    <div class="col-md-6 col-xl-3"></div>
</div>
@endsection
