@extends('layouts.limitless-layout')

@section('content')
    <div class="row mb-3">
        @foreach($models as $key => $model)
            <div class="col-md-3 text-center">
                <h5>{{$model->name}}</h5>
                <div class="btn-group w-100">
                    <button id="start{{$model->id}}" class="btn-start btn btn-primary w-50" title="Старт"><i class="icon-play3"></i></button>
                    <button id="stop{{$model->id}}" class="btn-stop btn btn-danger w-50" title="Стоп" disabled><i class="icon-stop"></i></button>
                    <button id="single{{$model->id}}" class="btn-single btn btn-primary w-50" title="1 запуск"><i class="icon-seven-segment-1"></i></button>
                </div>
            </div>
        @endforeach
    </div>
    <div class="text-right mb-3"><button id="clearlog" class="btn btn-success">Clear</button></div>
    <textarea id="log" class="w-100 mb-3 form-control form-control-sm text-success bg-dark" style="height: 500px;"></textarea>
@endsection

@section('scripts')
<script src="/assets/js/chapel.min.js"></script>
<script src="/assets/js/redactor.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#clearlog').click(function(){
            $('#log').text('');
        });
        @foreach($models as $key => $model)
            var timer{{$model->id}};
            $('#start{{$model->id}}').click(function(){
                $('#log').prepend('({{$model->name}} - started) ');
                $(this).siblings(".btn-stop").removeAttr('disabled');
                $(this).siblings(".btn-single").attr('disabled','disabled');
                $(this).attr('disabled','disabled');
                timer{{$model->id}} = setInterval(function() {
                    function{{$model->id}}();
                }, {{$model->sec*1000}});
            });
            $('#stop{{$model->id}}').click(function(){
                $('#log').prepend('({{$model->name}} - finished) ');
                $(this).siblings(".btn-start").removeAttr('disabled');
                $(this).siblings(".btn-single").removeAttr('disabled');
                $(this).attr('disabled','disabled');
                clearInterval(timer{{$model->id}});
            });
            $('#single{{$model->id}}').click(function(){
                $('#log').prepend('({{$model->name}} - single) ');
                function{{$model->id}}();
            });
            function function{{$model->id}}(){
                $.ajax({
                    url: '{{$model->url}}',
                    type: 'GET',
                    dataType : "json",
                    success: function (data) {
                        $('#log').prepend(data.result + ' ');
                    },
                    error: function(){
                        $('#log').prepend('(Ajax error) ');
                    }
                });
            }
        @endforeach

    });
</script>
@endsection
