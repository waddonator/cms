@extends('layouts.limitless-layout')

@section('content')
<div class="card mb-3">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">{{ $data['title'] OR ''}}</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Код</th>
                        <th>Название</th>
                        <th>Справочник</th>
                        <th>Модель</th>
                        <th>Слагаемое</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($models as $model)
                        <tr>
                            <td class="text-center">{{ $model->id }}</td>
                            <td>{{ $model->name }}</td>
                            <td>{{ $model->exportvoc->name }}</td>
                            <td class="text-center">{{ $model->model }}</td>
                            <td class="text-center">{{ $model->factor }}</td>
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="{{ route('exportvocmodels.edit', $model->id) }}" class="list-icons-item" title="{{trans('cms.btn-edit')}}"><i class="icon-pencil7"></i></a>
                                    <a href="{{route('exportvocmodels.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-trash" title="{{trans('cms.btn-remove')}}" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex mt-3">
            <a href="{{ route('exportvocmodels.create') }}" class="btn btn-success" title="{{ trans('cms.btn-create') }}"><i class="icon-file-plus"></i></a>
            {{ $models->appends($pagination_add)->links() }}
        </div>
    </div>
</div>

@endsection
