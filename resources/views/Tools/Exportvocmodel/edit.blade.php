@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($model, array('route' => array('exportvocmodels.update', $model->id), 'method' => 'PUT')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">{{trans('cms.title-parameters')}}</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    {{ Form::label('name', trans('cms.column-name'), ['class'=>'small text-muted font-italic']) }}
                    {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                    @if($errors->has('name'))
                        @foreach ($errors->get('name') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
                <div class="form-group">
                    {{ Form::label('model', trans('cms.column-model'), ['class'=>'small text-muted font-italic']) }}
                    {{ Form::text('model', null, array('class' => 'form-control'. ($errors->has('model') ? ' is-invalid' : '' ))) }}
                    @if($errors->has('model'))
                        @foreach ($errors->get('model') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
                <div class="form-group">
                    {{ Form::label('factor', 'Слагаемое', ['class'=>'small text-muted font-italic']) }}
                    {{ Form::number('factor', null, array('class' => 'form-control form-control-sm'. ($errors->has('factor') ? ' is-invalid' : '' ))) }}
                    @if($errors->has('factor'))
                        @foreach ($errors->get('factor') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
                <div class="form-group">
                    {{ Form::label('exportvoc_id', 'Справочник', ['class'=>'small text-muted font-italic']) }}
                    {{ Form::select('exportvoc_id', $exportvocs, null, array('class' => 'form-control form-control-sm'. ($errors->has('exportvoc_id') ? ' is-invalid' : ''))) }}
                    @if($errors->has('exportvoc_id'))
                        @foreach ($errors->get('exportvoc_id') as $message)
                            <div class="invalid-feedback">{{ $message }}</div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Навигация</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="{{trans('cms.btn-save')}}"><i class="icon-floppy-disk"></i></button>
                            <a href="{{ route('exportvocmodels.index') }}" class="btn btn-danger w-50" title="{{trans('cms.btn-exit')}}"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection
