@extends('layouts.limitless-layout')

@section('content')
    <div class="card mb-3">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Пользователи</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                    {{ $users->appends($pagination_add)->links() }}
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Имя</th>
                                <th>E-mail</th>
                                <th>Регистрация</th>
                                <th>Роли</th>
                                <th>Действия</th>
                            </tr>
                            <tr class="filter-line">
                                {!! Form::open(['method' => 'GET', 'route' => ['users.index'], 'id'=>'filter-form' ]) !!}
                                    <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : '', array('class' => 'form-control form-control-sm')) }}</th>
                                    <th>{{ Form::text('semail', isset($_GET['semail']) ? $_GET['semail'] : '', array('class' => 'form-control form-control-sm')) }}</th>
                                    <th><input type="text" class="form-control form-control-sm" disabled></th>
                                    <th>
                                        <select class="form-control form-control-sm" name="srole">
                                            @if(isset($roles))
                                                @foreach($roles as $key => $role)
                                                    <option value="{{$key}}" @if(isset($_GET['srole']) && $_GET['srole']==$key){{'selected'}}@endif>{{$role}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </th>
                                    <th>
                                        <div class="list-icons">
                                            <a href="#" class="list-icons-item text-primary-600 btn-filter" title="фильтровать"><i class="icon-filter3"></i></a>
                                        </div>
                                    </th>
                                {!! Form::close() !!}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                                <td>{{ $user->roles()->pluck('name')->implode(', ')}}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                                <td class="text-center">
                                    <div class="list-icons">
                                        <a href="{{ route('users.edit', $user->id) }}" class="list-icons-item" title="редактировать"><i class="icon-pencil7"></i></a>
                                        <a href="{{ route('users.show', $user->id) }}" class="list-icons-item" title="авторизоваться"><i class="icon-enter6"></i></a>
                                        <a href="{{route('users.destroy', $user->id)}}" class="list-icons-item text-danger-600 btn-remove" title="удалить" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
            </div>

            <a href="{{ route('users.create') }}" class="btn btn-success mt-3"><i class="icon-file-plus"></i></a>
        </div>
    </div>

@endsection

