@extends('layouts.limitless-layout')


@section('content')

    <div class="card mb-3">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Доступы</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                {{ $permissions->appends($pagination_add)->links() }}
                <table class="table">
                    <thead>
                        <tr>
                            <th>Доступ</th>
                            <th>Действие</th>
                        </tr>
                        <tr class="filter-line">
                            {!! Form::open(['method' => 'GET', 'route' => ['permissions.index'] ]) !!}
                                <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : '', array('class' => 'form-control form-control-sm')) }}</th>
                                <th>
                                    <div class="list-icons">
                                        <a href="#" class="list-icons-item text-primary-600 btn-filter" title="фильтровать"><i class="icon-filter3"></i></a>
                                    </div>
                                </th>
                            {!! Form::close() !!}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($permissions as $permission)
                        <tr>
                            <td>{{ $permission->name }}</td> 
                            <td class="text-center">
                                <div class="list-icons">
                                    <a href="{{ route('permissions.edit', $permission->id) }}" class="list-icons-item" title="редактировать"><i class="icon-pencil7"></i></a>
                                    <a href="{{route('permissions.destroy', $permission->id)}}" class="list-icons-item text-danger-600 btn-remove" title="удалить" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <a href="{{ route('permissions.create') }}" class="btn btn-success mt-3" title="добавить"><i class="icon-file-plus"></i></a>
        </div>
    </div>

@endsection

