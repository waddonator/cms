@extends('layouts.limitless-layout')

@section('content')

{{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="order-2 order-md-1 w-100">
            <div class="card" id="release">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Параметры</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('table.name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                    <div class='form-group'>
                        {{ Form::label('permissions', trans('table.permissions'), ['class'=>'small text-muted font-italic']) }}
                        @foreach ($permissions as $permission)
                            <div class="custom-control custom-checkbox">
                                {{ Form::checkbox('permissions[]',  $permission->id , null, ['id' => $permission->name, 'class' => 'custom-control-input'. ($errors->has('permissions') ? ' is-invalid' : '' ) ]) }}
                                {{ Form::label($permission->name, $permission->name, ['class' => 'custom-control-label']) }}
                            </div>
                        @endforeach
                        @if($errors->has('permissions'))
                            @foreach ($errors->get('permissions') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /left content -->

        <!--div class="col-lg-9">
            <div class="row">

                <div class="col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">{{trans('main.parameters')}}
                            @include('layouts.minimize')
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                {{ Form::label('name', trans('table.name'), ['class'=>'small text-muted font-italic']) }}
                                {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                                @if($errors->has('name'))
                                    @foreach ($errors->get('name') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">{{trans('main.permissions')}}
                            @include('layouts.minimize')
                        </div>
                        <div class="card-body">
                            <div class='form-group'>
                                @foreach ($permissions as $permission)
                                    <div class="custom-control custom-checkbox">
                                        {{ Form::checkbox('permissions[]',  $permission->id , null, ['id' => $permission->name, 'class' => 'custom-control-input'. ($errors->has('permissions') ? ' is-invalid' : '' ) ]) }}
                                        {{ Form::label($permission->name, $permission->name, ['class' => 'custom-control-label']) }}
                                    </div>
                                @endforeach
                                @if($errors->has('permissions'))
                                    @foreach ($errors->get('permissions') as $message)
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div-->

        <!-- Right sidebar component -->
        <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
            <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                <div class="sidebar-content">
                    <div class="card">
                        <div class="card-header bg-transparent header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">Навигация</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                                <div class="btn-group" style="width: 100%;">
                                    <button type="submit" id="submit" class="btn btn-success" style="width:50%;" title="Сохранить"><i class="icon-floppy-disk"></i></button>
                                    <a href="{{ route('roles.index') }}" class="btn btn-danger btn-block" style="width:50%;" title="Выход"><i class="icon-esc"></i></a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /right sidebar component -->

    </div>

{{ Form::close() }}

@endsection