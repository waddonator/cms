@extends('layouts.limitless-layout')

@section('content')
    <img class="under" src="/assets/img/under.png" style="max-width: 600px;width: 100%;display:none;">
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('.content').addClass('d-flex justify-content-center align-items-center');
        $('.under').show(500);
    });
</script>
@endsection
