@extends('layouts.limitless-layout')

@section('content')

<div class="row">
        @if(auth()->user()->can('admin'))
            <div class="col-md-4">
                @widget('rbot')
            </div>
            <div class="col-md-4">
                @widget('comments')
            </div>
        @endif
        @if(auth()->user()->can('admin') || auth()->user()->can('acts'))
            <div class="col-md-4">
                @include('Widgets.acts')
            </div>
        @endif
        @if(auth()->user()->can('admin') || auth()->user()->can('pubs'))
            <div class="col-md-4">
                @include('Widgets.publications')
            </div>
        @endif
        @if(auth()->user()->can('admin') || auth()->user()->can('news'))
            <div class="col-md-4">
                @include('Widgets.news')
            </div>
        @endif
</div>
@endsection
