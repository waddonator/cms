@if(auth()->user()->can('admin'))
    <li class="nav-item nav-item-submenu">
        <a href="#" class="nav-link"><i class="icon-gear"></i> <span>Разработка</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="Разработка">
            <li class="nav-item"><a href="{{ route('dev') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'dev' ? 'active' : '' !!}">Dev</a></li>
            <li class="nav-item"><a href="{{ route('link') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'link' ? 'active' : '' !!}">Link</a></li>
            <li class="nav-item"><a href="{{ route('toc') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'toc' ? 'active' : '' !!}">TOC</a></li>
        </ul>

    </li>
@endcan
