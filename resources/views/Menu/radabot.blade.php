@if(auth()->user()->can('admin'))
    <li class="nav-item nav-item-submenu">
        <a href="#" class="nav-link"><i class="icon-android"></i> <span>Радабот</span></a>

        <ul class="nav nav-group-sub" data-submenu-title="Радабот">
            <li class="nav-item"><a href="{{ route('radabot') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'radabot' ? 'active' : '' !!}">Консоль</a></li>
            <li class="nav-item"><a href="{{ route('ractions.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'ractions' ? 'active' : '' !!}">Запросы радабота</a></li>
            <li class="nav-item"><a href="{{ route('racts.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'racts' ? 'active' : '' !!}">Загруженные документы</a></li>
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link">Справочники</a>
                <ul class="nav nav-group-sub">
                    <li class="nav-item"><a href="{{ route('rbotstatuses.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'rbotstatuses' ? 'active' : '' !!}">Статусы радабота</a></li>
                </ul>
            </li>
        </ul>

    </li>
@endcan
