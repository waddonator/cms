@extends('layouts.limitless-layout')

@section('content')
	<h1>Ошибка 401</h1>
	<p>Вам отказано в доступе. Обратитеть к администратору системы для разрешения сложившейся ситуации.</p>
@endsection
