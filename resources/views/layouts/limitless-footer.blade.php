
            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                        Footer
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
                    <span class="navbar-text text-center w-100">
                        &copy; 2018. <a href="http://www.activemedia.ua/ua">ActiveMedia</a> by <a href="http://waddonator.ru/" target="_blank">Vadim A.Donets</a>
                    </span>

                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

@include('template-parts.modal-remove')
@include('template-parts.modal-trash')
@include('template-parts.modal-restore')
@section('modals')
@show

<script src="/limitless_assets/js/custom.js"></script>
<script type="text/javascript">
        /* Preloader */
        $(window).bind('load', function () {
            $('.loaderArea').fadeOut('fast');
        });
        /* End Preloader */    
</script>
@section('scripts')
@show

<div id="GotoTop"><i class="icon-arrow-up7"></i></div>

</body>
</html>
