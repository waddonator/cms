    </div>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <a class="btn btn-primary" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Remove Modal-->
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="removeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger text-white">
                    <h5 class="modal-title" id="removeModalLabel">Підтвердження видалення</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                </div>
                <div class="modal-body">Ви дійсно бажаєте видалити елемент без можливості відновлення?</div>
                <div class="modal-footer">
                    <a class="btn btn-danger" href="#" onclick="event.preventDefault(); document.getElementById('remove-form').submit();">{{trans('buttons.delete')}}</a>
                    <form id="remove-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
                    <input name="_method" type="hidden" value="DELETE"></form>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">{{trans('buttons.cancel')}}</button>
                </div>
            </div>
        </div>
    </div>

    @section('modals')
    @show

    <!-- Bootstrap core JavaScript-->
    <script src="/assets/lib/jquery/jquery.min.js"></script>
    <script src="/assets/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/assets/lib/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/assets/js/sb-admin.min.js"></script>
    <script src="/assets/js/common.js"></script>

    @section('scripts')
    @show

  </div>
</body>

</html>
