<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />
    <title>{{ config('app.name') }}</title>
    <link href="/assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/sb-admin.css" rel="stylesheet">
    <link href="/assets/css/common.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
        <a class="navbar-brand" href="{{route('home')}}"><img src="/assets/img/favicon.png"> {{config('app.name')}}</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
                    <a class="nav-link {!! Auth::user()->uriSegment(1) === '' ? 'active' : '' !!}" href="{{route('home')}}">
                        <i class="fa fa-fw fa-dashboard"></i>
                        <span class="nav-link-text">Главная</span>
                    </a>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Documents">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseDocuments" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-copy"></i>
                        <span class="nav-link-text">Документы</span>
                    </a>
                    <ul class="sidenav-second-level collapse @if( in_array(Auth::user()->uriSegment(1),['documents']) ){{'show'}}@endif" id="collapseDocuments">
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'acts' ? 'active' : '' !!}" href="{{ route('acts.index') }}">Правовые акты</a></li>
                        <li><a href="#">Новости</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'jurs' ? 'active' : '' !!}" href="{{ route('jurs.index') }}">Решения судов</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'monographs' ? 'active' : '' !!}" href="{{ route('monographs.index') }}">Монографии</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'publications' ? 'active' : '' !!}" href="{{ route('publications.index') }}">Публикации</a></li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Users">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUsers" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-user"></i>
                        <span class="nav-link-text">Users</span>
                    </a>
                    <ul class="sidenav-second-level collapse @if( in_array(Auth::user()->uriSegment(1),['users', 'roles', 'permissions']) ){{'show'}}@endif" id="collapseUsers">
                        <li><a class="{!! Auth::user()->uriSegment(1) === 'users' ? 'active' : '' !!}" href="{{ route('users.index') }}">Users</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(1) === 'roles' ? 'active' : '' !!}" href="{{ route('roles.index') }}">Roles</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(1) === 'permissions' ? 'active' : '' !!}" href="{{ route('permissions.index') }}">Permissions</a></li>
                    </ul>
                </li>

                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Vocabularies">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-book"></i>
                        <span class="nav-link-text">Vocabularies</span>
                    </a>
                    <ul class="sidenav-second-level collapse @if( in_array(Auth::user()->uriSegment(1),['vocabularies']) ){{'show'}}@endif" id="collapseMulti">
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseVocCommon">Common</a>
                            <ul class="sidenav-third-level collapse @if( in_array(Auth::user()->uriSegment(2),['regions']) ){{'show'}}@endif" id="collapseVocCommon">
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'regions' ? 'active' : '' !!}"href="{{ route('regions.index') }}">Регионы</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseVocAct">Acts</a>
                            <ul class="sidenav-third-level collapse @if( in_array(Auth::user()->uriSegment(2),['publishers', 'types', 'sources', 'asubacts', 'akeywords']) ){{'show'}}@endif" id="collapseVocAct">
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'publishers' ? 'active' : '' !!}"href="{{ route('publishers.index') }}">Авторы законодательных актов</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'types' ? 'active' : '' !!}"href="{{ route('types.index') }}">Типы актов</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'sources' ? 'active' : '' !!}"href="{{ route('sources.index') }}">Источники публикаций актов</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'asubacts' ? 'active' : '' !!}"href="{{ route('asubacts.index') }}">Подтипы актов</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'akeywords' ? 'active' : '' !!}"href="{{ route('akeywords.index') }}">Ключевые слова</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseVoc2">News</a>
                            <ul class="sidenav-third-level collapse" id="collapseVoc2">
                                <li>
                                    <a href="#">Third Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level Item</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseVoc3">Jurisprudence</a>
                            <ul class="sidenav-third-level collapse @if( in_array(Auth::user()->uriSegment(2),['justice-kind', 'judgment-code', 'court-code', 'cause-category', 'instances', 'judges']) ){{'show'}}@endif" id="collapseVoc3">
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'justice-kind' ? 'active' : '' !!}" href="{{ route('justice-kind.index') }}">Форми судових рішень</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'judgment-code' ? 'active' : '' !!}" href="{{ route('judgment-code.index') }}">Форми судочинства</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'court-code' ? 'active' : '' !!}" href="{{ route('court-code.index') }}">Суди</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'cause-category' ? 'active' : '' !!}" href="{{ route('cause-category.index') }}">Категорії справ</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'instances' ? 'active' : '' !!}" href="{{ route('instances.index') }}">Инстанции</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'judges' ? 'active' : '' !!}" href="{{ route('judges.index') }}">Судьи</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMonograph">Монографии</a>
                            <ul class="sidenav-third-level collapse @if( in_array(Auth::user()->uriSegment(2),['mauthors', 'mnodetypes']) ){{'show'}}@endif" id="collapseMonograph">
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'mauthors' ? 'active' : '' !!}" href="{{ route('mauthors.index') }}">Авторы монографий</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'mnodetypes' ? 'active' : '' !!}" href="{{ route('mnodetypes.index') }}">Типы узлов</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapsePublication">Публикации</a>
                            <ul class="sidenav-third-level collapse @if( in_array(Auth::user()->uriSegment(2),['pauthors', 'pauthorroles', 'pglosstypes', 'psources', 'pdocsubtypes']) ){{'show'}}@endif" id="collapsePublication">
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'pauthors' ? 'active' : '' !!}" href="{{ route('pauthors.index') }}">Авторы публикаций</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'pauthorroles' ? 'active' : '' !!}" href="{{ route('pauthorroles.index') }}">Роли авторов</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'pglosstypes' ? 'active' : '' !!}" href="{{ route('pglosstypes.index') }}">Gloss types</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'psources' ? 'active' : '' !!}" href="{{ route('psources.index') }}">Источники</a>
                                </li>
                                <li>
                                    <a class="{!! Auth::user()->uriSegment(2) === 'pdocsubtypes' ? 'active' : '' !!}" href="{{ route('pdocsubtypes.index') }}">Подтипы публикаций</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                </li>

                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tools">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseTools" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-wrench"></i>
                        <span class="nav-link-text">Tools</span>
                    </a>
                    <ul class="sidenav-second-level collapse @if( in_array(Auth::user()->uriSegment(1),['tools']) ){{'show'}}@endif" id="collapseTools">
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'opendata' ? 'active' : '' !!}" href="{{ route('opendata') }}">OpenData</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'exportdatas' ? 'active' : '' !!}" href="{{ route('exportdatas.index') }}">ExportData</a></li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Export">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExport" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-upload"></i>
                        <span class="nav-link-text">Export</span>
                    </a>
                    <ul class="sidenav-second-level collapse @if( in_array(Auth::user()->uriSegment(1),['export']) ){{'show'}}@endif" id="collapseExport">
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'jurs' ? 'active' : '' !!}" href="{{ route('export-jurs') }}">Jurisprudence</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'vocabularies' ? 'active' : '' !!}" href="{{ route('export-vocabularies') }}">Справочники</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'monographs' ? 'active' : '' !!}" href="{{ route('export-monographs') }}">Monographs</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'publications' ? 'active' : '' !!}" href="{{ route('export-publications') }}">Publications</a></li>
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'export' ? 'active' : '' !!}" href="{{ route('export') }}">Export</a></li>
                    </ul>
                </li>
                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Perseus">
                    <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapsePerseus" data-parent="#exampleAccordion">
                        <i class="fa fa-fw fa-database"></i>
                        <span class="nav-link-text">Perseus</span>
                    </a>
                    <ul class="sidenav-second-level collapse @if( in_array(Auth::user()->uriSegment(1),['perseus']) ){{'show'}}@endif" id="collapsePerseus">
                        <li><a class="{!! Auth::user()->uriSegment(2) === 'blocks' ? 'active' : '' !!}" href="{{ route('blocks.index') }}">Blocks</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="navbar-nav sidenav-toggler">
                <li class="nav-item">
                    <a class="nav-link text-center" id="sidenavToggler">
                        <i class="fa fa-fw fa-angle-left"></i></a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li><div class="nav-link">{{ auth()->user()->name }}</div></li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-fw fa-sign-out"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="content-wrapper">
        <div class="container-fluid">
