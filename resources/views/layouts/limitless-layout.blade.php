@include('layouts.limitless-header')

            <!-- Page header -->
            
            <div class="page-header page-header-dark has-cover">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        @if(isset($data['title']))
                        <h4>@if(isset($data['icon']))<i class="{{$data['icon']}} mr-2"></i>@endif @if(isset($data['title']))<span class="font-weight-semibold">{{$data['title']}}</span>@endif @if (isset($data['subtitle'])) - {{$data['subtitle']}}@endif</h4>
                        @else
                        <h4><i class="icon-eye-blocked mr-2"></i><span class="font-weight-semibold">{{trans('cms.error')}}</span</h4>
                        @endif
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>

                    </div>

                </div>
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            @if(isset($data['breadcrumbs']))
                                @foreach($data['breadcrumbs'] as $key => $breadcrumb)
                                    @if (count($data['breadcrumbs'])!=$key+1)
                                        <a href="{{$breadcrumb['url']}}" class="breadcrumb-item">{!!$breadcrumb['title']!!}</a>
                                    @else
                                        <span class="breadcrumb-item active">{!!$breadcrumb['title']!!}</span>
                                    @endif
                                @endforeach
                            @else
                                <a href="/" class="breadcrumb-item">{{trans('cms.home')}}</a>
                                <span class="breadcrumb-item active">{{trans('cms.error')}}</span>
                            @endif
                        </div>

                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                    @if(isset($data['trash']))
                        <div class="header-elements d-none">
                            <div class="breadcrumb justify-content-center">
                                <a href="{{$data['trash-route']}}" class="breadcrumb-elements-item">
                                    <i class="icon-trash mr-1"></i>
                                    {{trans('cms.trash')}}<span class="badge bg-danger-400 align-self-center ml-2">{{$data['trash']}}</span>
                                </a>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- /page header -->

            <!-- Alerts -->
            @if(Session::has('flash_message'))
                <div class="container">
                    <div class="alert alert-success alert-styled-left alert-arrow-left alert-dismissible mt-3">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                        {!! session('flash_message') !!}
                    </div>                    
                </div>
            @endif
            @if(Session::has('error_message'))
                <div class="container">
                    <div class="alert alert-warning alert-styled-left alert-arrow-left alert-dismissible mt-3">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                        {!! session('error_message') !!}
                    </div>
                </div>
            @endif
            <!-- /alerts -->

            <!-- Content area -->
            <div class="content">
                @section('content')
                @show
            </div>
            <!-- /content area -->

@include('layouts.limitless-footer')