<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }}</title>

    <!-- Global stylesheets -->
    <!--link rel="icon" type="image/png" href="/assets/img/favicon.png" /-->
    <link rel="shortcut icon" href="/assets/img/favicon-uk3.ico" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/limitless_assets/css/colors.min.css" rel="stylesheet" type="text/css">
    @section('styles')
    @show
    <link href="/limitless_assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/limitless_assets/js/main/jquery.min.js"></script>
    <script src="/limitless_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/limitless_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/limitless_assets/js/plugins/notifications/bootbox.min.js"></script>
    <script src="/limitless_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script src="/limitless_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/limitless_assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
    <script src="/limitless_assets/js/plugins/ui/fullcalendar/lang/locale-all.js"></script>

    <script src="/limitless_assets/js/plugins/prism.min.js"></script>
    <script src="/limitless_assets/js/plugins/sticky.min.js"></script>
    
    <script src="/limitless_assets/js/app.js"></script>
    <script src="/limitless_assets/js/pages/components_scrollspy.js"></script>

    <!--script src="/limitless_assets/js/demo_pages/components_modals.js"></script-->
    <!-- /theme JS files -->


</head>

<body>
    @if(auth()->user()->can('admin'))
    <!-- Preloader -->
    <div class="loaderArea"></div>
    @endif
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="navbar-brand">
            <a href="{{route('home')}}" class="d-inline-block" style="font-size: 14px;color:#fff;">
                <img src="/limitless_assets/images/LexMart_Logo_sm.png" alt="" class="d-inline-block" \> ActiveLexCMS
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-component-toggle d-none d-md-block">
                        <i class="icon-transmission"></i>
                    </a>
                </li>
            </ul>

            <span class="navbar-text ml-md-3 mr-md-auto">
                <!--span class="badge bg-success">Online</span-->
            </span>

            <ul class="navbar-nav">
                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="/limitless_assets/images/placeholders/placeholder.jpg" class="rounded-circle" alt="">
                        <span>@if(isset(Auth::user()->name)){{Auth::user()->name}}@endif</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route('account') }}" class="dropdown-item"><i class="icon-cog5"></i> {{ trans('cms.account') }}</a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item" id="logout"><i class="icon-switch2"></i> {{ trans('cms.exit') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                Navigation
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- Main navigation -->
                <div class="card card-sidebar-mobile" style="display: none;">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <li class="nav-item"><a href="{{route('home')}}" class="nav-link {!! Auth::user()->uriSegment(1) === '' ? 'active' : '' !!}"><i class="icon-home"></i> <span>Главная панель</span></a></li>

                        <!-- Main -->
                        <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Документы</div> <i class="icon-menu" title="Документы"></i></li>
                        @if(auth()->user()->can('admin') || auth()->user()->can('acts'))
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-law"></i> <span>Законодательные акты</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Законодательные акты">
                                    <li class="nav-item"><a href="{{ route('acts.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'acts' ? 'active' : '' !!}">Документы</a></li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Справочники</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="{{ route('publishers.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'publishers' ? 'active' : '' !!}">Авторы законодательных актов</a></li>
                                            <li class="nav-item"><a href="{{ route('types.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'types' ? 'active' : '' !!}">Типы актов</a></li>
                                            <li class="nav-item"><a href="{{ route('sources.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'sources' ? 'active' : '' !!}">Источники публикаций актов</a></li>
                                            <li class="nav-item"><a href="{{ route('asubacts.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'asubacts' ? 'active' : '' !!}">Подтипы актов</a></li>
                                            <li class="nav-item"><a href="{{ route('akeywords.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'akeywords' ? 'active' : '' !!}">Ключевые слова</a></li>
                                        </ul>
                                    </li>
                                    @if(auth()->user()->can('admin') || auth()->user()->can('comments'))
                                        <li class="nav-item nav-item-submenu">
                                            <a href="#" class="nav-link">{{trans('cms.vocabularies')}} {{trans('cms.doc-com')}}</a>
                                            <ul class="nav nav-group-sub">
                                                <li class="nav-item"><a href="{{ route('cauthors.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'cauthors' ? 'active' : '' !!}">{{trans('cms.voc-cauthor')}}</a></li>
                                                <li class="nav-item"><a href="{{ route('cauthorroles.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'cauthorroles' ? 'active' : '' !!}">{{trans('cms.voc-cauthorrole')}}</a></li>
                                                <li class="nav-item"><a href="{{ route('csources.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'csources' ? 'active' : '' !!}">{{trans('cms.voc-csource')}}</a></li>
                                                <li class="nav-item"><a href="{{ route('cdocsubtypes.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'cdocsubtypes' ? 'active' : '' !!}">{{trans('cms.voc-cdocsubtype')}}</a></li>
                                            </ul>
                                        </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcan
                        {{--@if(auth()->user()->can('admin') || auth()->user()->can('comments'))
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-comment"></i> <span>{{trans('cms.doc-com')}}</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Законодательные акты">
                                    <li class="nav-item"><a href="{{ route('comments.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'comments' ? 'active' : '' !!}">{{trans('cms.documents')}}</a></li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">{{trans('cms.vocabularies')}}</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="{{ route('cauthors.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'cauthors' ? 'active' : '' !!}">{{trans('cms.voc-cauthor')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('cauthorroles.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'cauthorroles' ? 'active' : '' !!}">{{trans('cms.voc-cauthorrole')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('csources.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'csources' ? 'active' : '' !!}">{{trans('cms.voc-csource')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('cdocsubtypes.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'cdocsubtypes' ? 'active' : '' !!}">{{trans('cms.voc-cdocsubtype')}}</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endcan--}}
                        @if(auth()->user()->can('admin') || auth()->user()->can('news'))
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-newspaper"></i> <span>Новости</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Новости">
                                    <li class="nav-item"><a href="{{ route('news.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'news' ? 'active' : '' !!}">Документы</a></li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Справочники</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="{{ route('ncats.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'ncats' ? 'active' : '' !!}">{{trans('cms.voc-ncat')}}</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endcan
                        @if(auth()->user()->can('admin') || auth()->user()->can('jurs'))
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-hammer2"></i> <span>Судебные решения</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Судебные решения">
                                    <li class="nav-item"><a href="../seed/sidebar_main.html" class="nav-link">Документы</a></li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Справочники</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="../seed/sidebar_secondary.html" class="nav-link">Secondary sidebar</a></li>
                                            <li class="nav-item"><a href="../seed/sidebar_right.html" class="nav-link">Right sidebar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endcan
                        @if(auth()->user()->can('admin') || auth()->user()->can('mons'))
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-book3"></i> <span>Монографии</span></a>

                                <ul class="nav nav-group-sub" data-submenu-title="Монографии">
                                    <li class="nav-item"><a href="../seed/sidebar_main.html" class="nav-link">Документы</a></li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Справочники</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="../seed/sidebar_secondary.html" class="nav-link">Secondary sidebar</a></li>
                                            <li class="nav-item"><a href="../seed/sidebar_right.html" class="nav-link">Right sidebar</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endcan
                        @if(auth()->user()->can('admin') || auth()->user()->can('pubs'))
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-magazine"></i> <span>Публикации</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Публикации">
                                    <li class="nav-item"><a href="{{ route('publications.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'publications' ? 'active' : '' !!}">Документы</a></li>
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link">Справочники</a>
                                        <ul class="nav nav-group-sub">
                                            <li class="nav-item"><a href="{{ route('pauthors.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'pauthors' ? 'active' : '' !!}">{{trans('cms.voc-pauthor')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('pauthorroles.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'pauthorroles' ? 'active' : '' !!}">{{trans('cms.voc-pauthorrole')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('pglosstypes.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'pglosstypes' ? 'active' : '' !!}">{{trans('cms.voc-pglosstype')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('psources.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'psources' ? 'active' : '' !!}">{{trans('cms.voc-psource')}}</a></li>
                                            <li class="nav-item"><a href="{{ route('pdocsubtypes.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'pdocsubtypes' ? 'active' : '' !!}">{{trans('cms.voc-pdocsubtype')}}</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endcan
                        <!-- /main -->

                        @if(auth()->user()->can('admin'))
                            <!-- Users -->
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Пользователи</div> <i class="icon-menu" title="Пользователи"></i></li>
                            <li class="nav-item"><a href="{{ route('users.index') }}" class="nav-link {!! Auth::user()->uriSegment(1) === 'users' ? 'active' : '' !!}"><i class="icon-users"></i> <span>Пользователи</span></a></li>
                            <li class="nav-item"><a href="{{ route('roles.index') }}" class="nav-link {!! Auth::user()->uriSegment(1) === 'roles' ? 'active' : '' !!}"><i class="icon-user-check"></i> <span>Роли</span></a></li>
                            <li class="nav-item"><a href="{{ route('permissions.index') }}" class="nav-link {!! Auth::user()->uriSegment(1) === 'permissions' ? 'active' : '' !!}"><i class="icon-user-lock"></i> <span>Доступы</span></a></li>
                            <!-- /users -->
                        @endcan

                        @if(auth()->user()->can('admin'))
                            <!-- Perseus -->
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Perseus</div> <i class="icon-menu" title="Perseus"></i></li>
                            <!-- /perseus -->
                            <li class="nav-item"><a class="nav-link {!! Auth::user()->uriSegment(2) === 'exportdatas' ? 'active' : '' !!}" href="{{ route('exportdatas.index') }}"><i class="icon-database-export"></i> <span>{{trans('cms.export-data')}}</span></a></li>
                            <li class="nav-item"><a class="nav-link {!! Auth::user()->uriSegment(2) === 'exportvocs' ? 'active' : '' !!}" href="{{ route('exportvocs.index') }}"><i class="icon-database-export"></i> <span>{{trans('cms.export-voc')}}</span></a></li>
                            <li class="nav-item"><a class="nav-link {!! Auth::user()->uriSegment(2) === 'exportvocmodels' ? 'active' : '' !!}" href="{{ route('exportvocmodels.index') }}"><i class="icon-database-export"></i> <span>{{trans('cms.export-vocmodel')}}</span></a></li>
                            <li class="nav-item"><a class="nav-link {!! Auth::user()->uriSegment(2) === 'blocks' ? 'active' : '' !!}" href="{{ route('blocks.index') }}"><i class="icon-database-export"></i> <span>{{trans('cms.blocks')}}</span></a></li>
                        @endcan

                        @if(auth()->user()->can('admin'))
                            <!-- Tools -->
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Служебные</div> <i class="icon-menu" title="Perseus"></i></li>

                            <li class="nav-item nav-item-submenu @if(in_array(Auth::user()->uriSegment(2),['logactions', 'logs'])) nav-item-open @endif">
                                <a href="#" class="nav-link"><i class="icon-eye4"></i> <span>Логирование</span></a>
                                <ul class="nav nav-group-sub" data-submenu-title="Логирование">
                                    <li class="nav-item"><a href="{{ route('logs.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'logs' ? 'active' : '' !!}">Логи</a></li>
                                    <li class="nav-item"><a href="{{ route('logactions.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'logactions' ? 'active' : '' !!}">Справочник действий</a></li>
                                </ul>
                            </li>
                            <li class="nav-item"><a class="nav-link {!! Auth::user()->uriSegment(2) === 'tools' ? 'active' : '' !!}" href="{{ route('tools') }}"><i class="icon-hammer-wrench"></i> <span>{{trans('cms.tools')}}</span></a></li>
                            <!-- /tools -->
                        @endcan
                        @include('Menu.radabot')
                        @include('Menu.develop')

                    </ul>
                </div>
                <!-- /main navigation -->
                <script type="text/javascript">
                    $('.nav-link.active').parents('.nav-item-submenu').addClass('nav-item-open');
                    $('.nav-link.active').parents('.nav-group-sub').show();
                    $('.card-sidebar-mobile').show();
                </script>

            </div>
            <!-- /sidebar content -->
            
        </div>
        <!-- /main sidebar -->

        <!-- Main content -->
        <div class="content-wrapper">