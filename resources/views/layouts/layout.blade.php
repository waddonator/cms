@include('layouts.header')

    @if(Session::has('flash_message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Well done!</strong> {!! session('flash_message') !!}
        </div>
    @endif
    @if(Session::has('error_message'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Oh snap!</strong> {!! session('error_message') !!}
        </div>
    @endif
    @if(!empty($successMsg))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>Well done!</strong> {!! $successMsg !!}
        </div>
    @endif
    
    <div class="content-area">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{!!$title or ''!!}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @section('content')
                @show
            </div>
        </div>

    </div>

@include('layouts.footer')