    <div class="card">
        <div class="card-header header-elements-inline">
            <h6 class="card-title">{{trans('cms.doc-act')}}</h6>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row text-center">
                <div class="col-4">
                    <p><i class="icon-stack-text icon-2x d-inline-block text-info"></i></p>
                    <h5 class="font-weight-semibold mb-0">{{$data['act']->all}}</h5>
                    <span class="text-muted font-size-sm">всего</span>
                </div>
                <div class="col-4">
                    <p><i class="icon-trash icon-2x d-inline-block text-warning"></i></p>
                    <h5 class="font-weight-semibold mb-0">{{$data['act']->trash}}</h5>
                    <span class="text-muted font-size-sm">в корзине</span>
                </div>
                <div class="col-4">
                    <p><i class="icon-database-export icon-2x d-inline-block text-success"></i></p>
                    <h5 class="font-weight-semibold mb-0">{{$data['act']->export}}</h5>
                    <span class="text-muted font-size-sm">нужно экспортировать</span>
                </div>
            </div>
        </div>
        <div class="card-body">
            <ul class="media-list">
                <li class="media">
                    <div class="mr-3">
                        <span class="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon">
                            <i class="icon-new"></i>
                        </span>
                    </div>
                    <div class="media-body">
                        Последний созданный: <a href="{{route('acts.index',['sid'=>$data['act']->lastnew->id])}}">{{$data['act']->lastnew->name}}</a>
                    </div>
                </li>
                <li class="media">
                    <div class="mr-3">
                        <span class="btn bg-transparent border-danger text-danger rounded-round border-2 btn-icon">
                            <i class="icon-pencil3"></i>
                        </span>
                    </div>
                    <div class="media-body">
                        Последний измененный: <a href="{{route('acts.index',['sid'=>$data['act']->lastedit->id])}}">{{$data['act']->lastedit->name}}</a>
                    </div>
                </li>
            @if($data['act']->lasttrash)
                <li class="media">
                    <div class="mr-3">
                        <a href="#" class="btn bg-transparent border-slate text-slate rounded-round border-2 btn-icon">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                    <div class="media-body">
                        Последний удаленный: <a href="{{route('act-trash',['sid'=>$data['act']->lasttrash->id])}}">{{$data['act']->lasttrash->name}}</a>
                    </div>
                </li>
            @endif
            </ul>
        </div>
    </div>        
