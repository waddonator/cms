<div class="card">
    <div class="card-body text-center">
        <i class="icon-toggle icon-2x text-orange-400 border-orange-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
        <h5 class="card-title">Relations</h5>
        <p class="mb-3">Создание массива связей между актами и локальными комментариями</p>
        <a href="{{route('relations')}}" class="btn bg-orange-400">Вперед</a>
    </div>
</div>