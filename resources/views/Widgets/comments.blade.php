<div class="card bg-blue-400 text-white text-center p-3">
    <div>
        <a href="{{route('acts.index')}}" class="btn btn-lg btn-icon mb-3 mt-1 btn-outline text-white border-white bg-white rounded-round border-2"><i class="icon-comment-discussion"></i></a>
    </div>
    <blockquote class="blockquote mb-0">
        <p>Комментарии к законодательным актам</p>
        <footer class="text-white">
            @if($data)
                @foreach($data as $key => $value)
                    {{$key}}: {{$value}}<br>
                @endforeach
            @endif
        </footer>
    </blockquote>
</div>