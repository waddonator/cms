<div class="card">
    <div class="card-body text-center">
        <i class="icon-box-add icon-2x text-primary-400 border-primary-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
        <h5 class="card-title">Lexinform</h5>
        <p class="mb-3">Синхронизация новостей и справочников с сайтом lexinform</p>
        <a href="{{route('lexinform')}}" class="btn bg-primary-400">Вперед</a>
    </div>
</div>