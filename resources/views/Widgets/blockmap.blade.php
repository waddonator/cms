<div class="card">
    <div class="card-body text-center">
        <i class="icon-cube3 icon-2x text-violet-400 border-violet-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
        <h5 class="card-title">Карта блоков</h5>
        <p class="mb-3">Экспорт блоков и создание карты блоков</p>
        <a href="{{route('blocks-map')}}" class="btn bg-violet-400">Вперед</a>
    </div>
</div>