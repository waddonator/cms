<div class="card">
    <div class="card-body text-center">
        <i class="icon-reset icon-2x text-success-400 border-success-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
        <h5 class="card-title">Счетчики Autoincrement</h5>
        <p class="mb-3">Установка всех счетчиков id в положение +1 к максимальному значению</p>
        <a href="{{route('reincrement')}}" class="btn bg-success-400">Вперед</a>
    </div>
</div>