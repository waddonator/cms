<div class="card bg-pink-400 text-white text-center p-3">
    <div>
        <a href="#" class="btn btn-lg btn-icon mb-3 mt-1 btn-outline text-white border-white bg-white rounded-round border-2"><i class="icon-android"></i></a>
    </div>
    <blockquote class="blockquote mb-0">
        <p>Робот получения данных законодательных актов с официального сайта Верховной Рады Украины</p>
        <footer class="text-white">
            @if( isset($data->acts) )
                @foreach($data->acts as $key => $value)
                    {{$key}}: {{$value}}<br>
                @endforeach
            @endif
            <hr>
            @if( isset($data->actversions) )
                @foreach($data->actversions as $key => $value)
                    {{$key}}: {{$value}}<br>
                @endforeach
            @endif
            <hr>
            @if( isset($data->links) )
                @foreach($data->links as $key => $value)
                    {{$key}}: {{$value}}<br>
                @endforeach
            @endif
            <hr>
            @if( isset($data->warnings) )
                @foreach($data->warnings as $key => $value)
                    {{$key}}: {{$value}}<br>
                @endforeach
            @endif
        </footer>
    </blockquote>
</div>