@extends('layouts.layout')

@section('content')
    <h1>Export Monographs Page</h1>
    <h3>{{$method or ''}}</h3>

    <form method="POST" action="" style="margin-bottom: 30px;">
        @csrf
        <button class="btn btn-info btn-sm">Create documents</button>
    </form>

    <table class="table table-striped table-hover small">
        <thead class="bg-secondary text-white table-bordered">
            <tr>
                <th>Наша база</th>
                <th>Польская база</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">{{$doc1 or ''}}</td>
                <td class="text-center">{{$doc2 or ''}}</td>
            </tr>
        </tbody>
    </table>

    <pre>@php(print_r($document))</pre>
    <pre>@php(print_r($content))</pre>

@endsection
