@extends('layouts.limitless-layout')

@section('content')
    <h1>Экспорт справочников</h1>
    {{$method}}

    <div class="card mb-3">

        <div class="card-header header-elements-inline">
            <h5 class="card-title">{{trans('cms.doc-news')}}</h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Название</th>
                <th>Записей</th>
                <th>Польский справочник</th>
                <th>Существование</th>
                <th>Записей</th>
                <th>Действие</th>
            </tr>
        </thead>
        <tbody>
            @foreach($object as $key=>$row)
                <tr>
                    <td>{{$row->name or ''}}</td>
                    <td class="text-center">{{$row->count or ''}}</td>
                    <td>{{$row->pname or ''}}</td>
                    <td class="text-center">{{$row->pexist or ''}}</td>
                    <td class="text-center">{{$row->pcount or ''}}</td>
                    <td class="text-center">
                        <form method="POST" action="{{ route('exportvocs') }}">
                            @csrf
                            <input type="hidden" name="voc" value="{{$row->pname or ''}}">
                            <div class="btn-group">
                                <button class="btn btn-info btn-sm">Export</button>
                            </div>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

            </div>
        </div>
    </div>

@endsection
