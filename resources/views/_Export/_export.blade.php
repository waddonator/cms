@extends('layouts.layout')

@section('content')
    <h2>{{$method OR ''}}</h2>
    <div class="card mb-3">
        <div class="card-header">Документы@include('layouts.minimize')</div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Тип</th>
                        <th>Записей</th>
                        <th>Perseus</th>
                        <th>Последний экспорт</th>
                        <th>Область</th>
                        <th>Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @php(dump($input))                  
                    @foreach($documents as $document)
                    <tr>
                        {{ Form::open() }}
                            {{ Form::hidden('key', $document->key) }}
                            <td>{{$document->name}}</td>
                            <td class="text-center">{{$document->reccount}}</td>
                            <td class="text-center">{{$document->percount}}</td>
                            <td class="text-center">2018-08-13</td>
                            <td><select class="form-control form-control-sm" name="area">
                                <option value="0">Выберите</option>
                                <option value="1">Новые записи</option>
                                <option value="2">Все записи</option>
                            </select></td>
                            <td class="text-center"><button class="btn btn-light btn-sm"><i class="fa fa-fw fa-upload"></i></button></td>
                        {{ Form::close() }}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
