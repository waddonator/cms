<div id="modal_export" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title">Экспорт документов</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="export-modal-body">
                Вы уверены, что хотите экспортировать отмеченные элементы выбранной категории?
            </div>
            <form id="export-form" action="{{ route('export') }}" method="POST" style="display: none;">@csrf
                <input name="_method" type="hidden" value="POST">
                <input name="_action" type="hidden" value="EXPORT">
                <input name="id" type="hidden" value="0">
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn bg-success" onclick="document.getElementById('export-form').submit()">Экспортировать</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.btn-export').click(function(e){
        e.preventDefault();
        $("#export-form").children("[name='id']").val($(this).attr('data-id'));
    });    
});
</script>