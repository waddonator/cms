                <div id="modal_trash" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <h6 class="modal-title">Удаление элемента в корзину</h6>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body" id="remove-modal-body">
                                Вы уверены, что хотите отправить элемент в корзину? У Вас остается возможность восстановления указанного элемента до момента очистки корзины.
                            </div>
                            <form id="trash-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
                            <input name="_method" type="hidden" value="DELETE"></form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Отмена</button>
                                <button type="button" class="btn bg-danger" onclick="document.getElementById('trash-form').submit()">Удалить</button>
                            </div>
                        </div>
                    </div>
                </div>
