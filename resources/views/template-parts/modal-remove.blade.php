                <div id="modal_theme_danger" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <h6 class="modal-title">Удаление элемента</h6>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body" id="remove-modal-body">
                                Вы уверены, что хотите удалить элемент без возможности восстановления?
                            </div>
                            <form id="remove-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
                            <input name="_method" type="hidden" value="DELETE"><input name="_action" type="hidden" value="DELETE"></form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Отмена</button>
                                <button type="button" class="btn bg-danger" onclick="document.getElementById('remove-form').submit()">Удалить</button>
                            </div>
                        </div>
                    </div>
                </div>
