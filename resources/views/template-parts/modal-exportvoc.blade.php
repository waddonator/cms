<div id="modal_exportvoc" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title">Экспорт справочников</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="exportvoc-modal-body">
                Вы уверены, что хотите экспортировать выбранный справочник в базу Perseus?
            </div>
            <form id="exportvoc-form" action="{{ route('exportvoc') }}" method="POST" style="display: none;">@csrf
                <input name="_method" type="hidden" value="POST">
                <input name="_action" type="hidden" value="EXPORT">
                <input name="id" type="hidden" value="0">
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn bg-success" onclick="document.getElementById('exportvoc-form').submit()">Экспортировать</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.btn-exportvoc').click(function(e){
        e.preventDefault();
        $("#exportvoc-form").children("[name='id']").val($(this).attr('data-id'));
    });    
});
</script>