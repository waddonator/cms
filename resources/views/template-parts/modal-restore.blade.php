                <div id="modal_restore" class="modal fade" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h6 class="modal-title">Восстановление элемента</h6>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>

                            <div class="modal-body" id="remove-modal-body">
                                Вы уверены, что хотите восстановить элемент из корзины?
                            </div>
                            <form id="restore-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
                            <input name="_method" type="hidden" value="DELETE"><input name="_action" type="hidden" value="RESTORE"></form>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Отмена</button>
                                <button type="button" class="btn bg-success" onclick="document.getElementById('restore-form').submit()">Восстановить</button>
                            </div>
                        </div>
                    </div>
                </div>
