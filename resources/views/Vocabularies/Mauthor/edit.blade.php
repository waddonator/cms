@extends('layouts.layout')

@section('content')

{{ Form::model($model, array('route' => array('mauthors.update', $model->id), 'method' => 'PUT')) }}

    <div class="row">

        <div class="col-lg-9">
            <div class="card mb-3">
                <div class="card-header">{{trans('main.parameters')}}
                    @include('layouts.minimize')
                </div>
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('table.name'), ['class'=>'small text-muted font-italic']) }}
                        {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}
                        @if($errors->has('name'))
                            @foreach ($errors->get('name') as $message)
                                <div class="invalid-feedback">{{ $message }}</div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="card mb-3">
                <div class="card-header">{{trans('table.actions')}}
                    @include('layouts.minimize')
                </div>
                <div class="card-body">
                        <div class="btn-group" style="width: 100%;">
                            <button type="submit" id="submit" class="btn btn-success" style="width:50%;"title="{{trans('buttons.save')}}"><i class="fa fa-save fa-fw"></i></button>
                            <a href="{{ route('mauthors.index') }}" class="btn btn-danger btn-block" style="width:50%;" title="{{trans('buttons.exit')}}"><i class="fa fa-sign-out fa-fw"></i></a>
                        </div>
                </div>
            </div>
        </div>

    </div>

{{ Form::close() }}

@endsection
