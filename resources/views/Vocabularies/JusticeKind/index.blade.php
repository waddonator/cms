@extends('layouts.layout')

@section('content')
    <div class="card mb-3">
        <div class="card-header bg-dark text-white">Форми судових рішень</div>
        <div class="card-body bg-light">
                    <table class="table table-striped table-hover small">
                        <thead class="bg-secondary text-white table-bordered">
                            <tr>
                                <th>{{trans('table.key')}}</th>
                                <th>{{trans('table.name')}}</th>
                                <th>{{trans('table.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($models as $model)
                            <tr>
                                <td class="text-center">{{ $model->id }}</td>
                                <td>{{ $model->name }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="{{ route('cause-category.edit', $model->id) }}" class="btn btn-info btn-sm" title="{{trans('buttons.edit')}}"><i class="fa fa-pencil fa-fw"></i></a>
                                        <a href="{{route('cause-category.destroy', $model->id)}}" class="btn btn-danger btn-sm btn-remove" data-toggle="modal" data-target="#removeModal" title="{{trans('buttons.delete')}}"><i class="fa fa-trash-o fa-fw"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $models->links() }}

                    <a href="{{ route('cause-category.create') }}" class="btn btn-primary">{{trans('buttons.append')}}</a>

        </div>
        <div class="card-footer small text-muted font-italic text-right bg-dark">{{trans('table.updated',['date' => date("d.m.Y"), 'time' => date("H:i")])}}</div>
    </div>

@endsection