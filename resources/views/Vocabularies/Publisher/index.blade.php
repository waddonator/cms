@extends('layouts.limitless-layout')

@section('content')
<div class="card mb-3">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">{{ $data['title'] OR ''}}</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>{{trans('cms.column-key')}}</th>
                        <th>{{trans('cms.column-name')}}</th>
                        <th>{{trans('cms.column-actions')}}</th>
                    </tr>
                    <tr class="filter-line">
                        {!! Form::open(['method' => 'GET', 'route' => ['publishers.index'], 'id'=>'filter-form' ]) !!}
                            <th>{{ Form::text('sid', isset($_GET['sid']) ? $_GET['sid'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                            <th>{{ Form::text('sname', isset($_GET['sname']) ? $_GET['sname'] : null, array('class' => 'form-control form-control-sm')) }}</th>
                            <th>
                                <div class="list-icons">
                                    <a href="#" class="list-icons-item text-primary-600 btn-filter" title="{{ trans('cms.btn-filter') }}"><i class="icon-filter3"></i></a>
                                </div>
                            </th>
                        {!! Form::close() !!}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($models as $model)
                    <tr>
                        <td class="text-center">{{ $model->id }}</td>
                        <td>{{ $model->name }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <a href="{{ route('publishers.edit', $model->id) }}" class="list-icons-item" title="{{ trans('cms.btn-edit') }}"><i class="icon-pencil7"></i></a>
                                @can('admin')
                                    <a href="{{route('publishers.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-remove" title="{{ trans('cms.btn-remove') }}" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-trash"></i></a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex mt-3">
            <a href="{{ route('publishers.create') }}" class="btn btn-success" title="{{ trans('cms.btn-create') }}"><i class="icon-file-plus"></i></a>
            {{ $models->appends($pagination_add)->links() }}
        </div>
    </div>
</div>

@endsection