@extends('layouts.limitless-layout')

@section('content')
<div class="card mb-3">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">{{ $data['title'] OR ''}}</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>{{trans('table.key')}}</th>
                        <th>{{trans('table.name')}}</th>
                        <th>{{trans('table.actions')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($models as $model)
                    <tr>
                        <td class="text-center">{{ $model->id }}</td>
                        <td>{{ $model->name }}</td>
                        <td class="text-center">
                            <div class="list-icons">
                                <a href="{{ route('akeywords.edit', $model->id) }}" class="list-icons-item" title="{{ trans('cms.btn-edit') }}"><i class="icon-pencil7"></i></a>
                                @can('admin')
                                    <a href="{{route('akeywords.destroy', $model->id)}}" class="list-icons-item text-danger-600 btn-remove" title="{{ trans('cms.btn-remove') }}" data-toggle="modal" data-target="#modal_theme_danger"><i class="icon-trash"></i></a>
                                @endcan
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="d-flex mt-3">
            <a href="{{ route('akeywords.create') }}" class="btn btn-success" title="{{ trans('cms.btn-create') }}"><i class="icon-file-plus"></i></a>
            {{ $models->appends($pagination_add)->links() }}
        </div>
    </div>
</div>

@endsection