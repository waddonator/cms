<?php

return [

    'key' => 'Key',
    'name' => 'Name',
    'actions' => 'Actions',
    'region' => 'Region',
    'judge' => 'Judge',
    'id' => 'Id',
    'num' => 'Number',
    'adjudication_date' => 'Date of adjudication',
    'justice_kind' => 'Kind of justice',

];
