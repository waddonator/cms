<?php

return [

    'append' => 'Append New',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'cancel' => 'Cancel',

];
