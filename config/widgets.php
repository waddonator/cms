<?php
/*
 * Ключ: краткое название виджета для обращения к нему из шаблона
 * Значение: название класса виджета с пространством имен
 */
return [
    'rbot' => 'App\Widgets\RbotWidget',
    'comments' => 'App\Widgets\CommentsWidget',
];
