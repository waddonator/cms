<?php

namespace App;

use App\Repllink;

use Illuminate\Database\Eloquent\Model;
use App\Hexa\Common;
use Carbon\Carbon;

class Ractversion extends Model
{
    protected $common;

    public function __construct() {
        $this->common = new Common;
    }    

    protected $casts = [
        'rmeta' => 'object',
        'rcontent' => 'object',
        'pmeta' => 'object',
    ];

    public function ract()
    {
        return $this->belongsTo('App\Ract','act_id');
    }

    public function rbotstatus()
    {
        return $this->belongsTo('App\Rbotstatus');
    }

    public function rwork()
    {
        return $this->hasOne('App\Rwork','id','id');
    }



    #
    # Получение редакции документа с сайта ВР (метаданные и контент)
    #
    public function getRadaDocument()
    {
        # получаем данные карточки
            $opts = [
                'http' => [
                    'method'=>"GET",
                    'header'=>"Accept-language: en\r\n" .
                        "Cookie: OpenData=OpenData\r\n"
                ]
            ];
            $context = stream_context_create($opts);

            $url_meta = 'http://data.rada.gov.ua/laws/card/' . $this->ract->nreg . '/ed' . $this->ed . '.json'; #временно
            $html_meta = file_get_contents($url_meta, false, $context); #временно
        # получаем контент
            $url_content = 'http://data.rada.gov.ua/laws/show/' . $this->ract->nreg . '/ed' . $this->ed . '.json';
            $html_content = file_get_contents($url_content, false, $context);
        # сохраняем полученные данные
            $this->rmeta = $html_meta; #временно
            $this->rcontent = $html_content;
            $this->rbotstatus_id = 2; #метаданные и контент загружены
            $this->save();
        return true;
    }


    #
    # Преобразование json-контента ВР к тексту
    #
    public function buildContent()
    {
        $result = false;
        if ($this->rbotstatus_id==2){
            $this->rbotstatus_id = 11;
            $this->save();
            ini_set('max_execution_time', 900);
            # создаем запись, если ее нет
                $model = Rwork::find($this->id);
                if (!$model) {
                    $model = new Rwork;
                    $model->id = $this->id;
                }
            # собираем контент
                $text = '';
                if ($this->rcontent!='' && isset(json_decode($this->rcontent)->stru->nums)) {
                        foreach(json_decode($this->rcontent)->stru->nums as $krey => $row) {
                            $text .= json_decode($this->rcontent)->stru->{$row}->text;
                        }
                    # сохраняем контент
                        $model->rcontent = $text;
                        $model->save();
                    # ставим отметку, что контент собран
                        $this->rbotstatus_id = 13;
                        $this->save();
                } else {
                    # ошибка сборки
                        $this->rbotstatus_id = 12;
                        $this->save();
                }
            $result = true; 
        }
        return $result;
    }


    #
    # Замена ссылок в тексте
    #
    public function changeLinks()
    {

        $result = false;
        if ( in_array( $this->rbotstatus_id,[13, 22, 23, 24] ) ){
            // $this->rbotstatus_id = 21;
            // $this->save();
            # находим все ссылки
                $content = $this->rwork->rcontent;
                preg_match_all('|javascript\:OpenDoc\(\'(.*)\'\);|U',$content,$matches1); # простые ссылки 
                preg_match_all('|javascript\:OpenDocFrom\(\'(.*)\',\'(.*)\'\);|U',$content,$matches2); # toc ссылки
                $matches1[3] = [];
                $matches2[3] = [];
            # находим nro для каждой ссылки
                $count1 = 0;
                foreach ($matches1[1] as $key => $match) {
                    $model = Ract::where('nreg','=', $match)->first();
                    if ($model) {
                        $matches1[3][$key] = '#/document/' . $model->nro;
                    } else {
                        # проверяем наличие ненайденного документа для альтернативной загрузки

                                $opts = [
                                    'http' => [
                                        'method'=>"GET",
                                        'header'=>"Accept-language: en\r\n" .
                                            "Cookie: OpenData=OpenData\r\n"
                                    ]
                                ];
                                $context = stream_context_create($opts);

                                //$url_meta = 'http://data.rada.gov.ua/rada/card/' . trim($match) . '.json';
                                $url_meta = 'http://data.rada.gov.ua/rada/card/v0158282-05.json';
                                dump($url_meta);
                                //echo '<pre>'.$match.'</pre>';
                                //dd($match);
                                // $c = curl_init($url_meta);
                                // curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                                // $content = curl_exec($c);
                                // dd($content);
                                $html_meta = @file_get_contents($url_meta, false, $context);
                                dd($html_meta);
                            # проверяем ответ, и если всё в порядке - сохраняем полученные данные
                                if ($http_response_header){
                                    dump($http_response_header);
                                    if ($http_response_header[0]=='HTTP/1.1 200 OK'){
                                        //dd('HTTP/1.1 200 OK');
                                        $object = json_decode($html_meta);
                                        // $model = new Ract;
                                        // $this->rmeta = $html_meta;
                                        // $this->rbotstatus_id = 2;
                                        // $this->save();
                                        dd($object);
                                    } else {
                                        $matches1[3][$key] = $matches1[0][$key];
                                        $count1++;
                                    }
                                }

                        # ---
                    }
                }
                $count2 = 0;
                foreach ($matches2[1] as $key => $match) {
                    $model = Repllink::where([ ['nreg','=', $matches2[1][$key]], ['anchor','=', $matches2[2][$key]] ])->first();
                    if (!$model){
                        $element = Ract::where('nreg','=',$matches2[1][$key])->first();
                        $model = new Repllink;
                        $model->act_id = ($element) ? $element->id : 0;
                        $model->nreg = $matches2[1][$key];
                        $model->anchor = $matches2[2][$key];
                        $model->tocunit = '';
                        if ($model->act_id) {$model->save();}
                    }
                    if ($model->tocunit!=''){
                        $matches2[3][$key] = '#/document/' . $model->ract->nro . '?unitId=' . $model->tocunit;
                    } else {
                        $matches2[3][$key] = $matches2[0][$key];
                        $count2++;
                    }
                }
            # заменяем ссылки на нужные
                foreach ($matches1[0] as $key => $match) {
                    $content = str_replace($match, $matches1[3][$key], $content);
                }
                foreach ($matches2[0] as $key => $match) {
                    $content = str_replace($match, $matches2[3][$key], $content);
                }
                $this->rwork->rcontent = $content;
                $this->rwork->save();
            # проверяем, все ли ссылки заменили и устанавливаем статус
                if ( $count1==0 && $count2==0 ){
                    $this->rbotstatus_id = 25;
                } elseif ($count1!=0 && $count2!=0) {
                    $this->rbotstatus_id = 24;
                } elseif ($count2!=0) {
                    $this->rbotstatus_id = 23;
                } else {
                    $this->rbotstatus_id = 22;
                }
                $this->save();
            # ---
            $result = true;
        }

        return $result;
    }

}
