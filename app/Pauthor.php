<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pauthor extends Model
{
    public $timestamps = FALSE;

    public function pauthorroles()
    {
        return $this->belongsToMany('App\Pauthorrole');
    }

    public function publications()
    {
        return $this->belongsToMany('App\Publication');
    }
}
