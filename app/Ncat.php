<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ncat extends Model
{
    public $timestamps = FALSE;

    public function news()
    {
        return $this->belongsToMany('App\News');
    }

}
