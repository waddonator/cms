<?php

namespace App\Http\Controllers\Documents;

use App\Jur;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JurController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $where = [];
        $where[] = ['court_code_id', '=', '9911', 'or'];
        $where[] = ['court_code_id', '=', '9921', 'or'];
        $where[] = ['court_code_id', '=', '9931', 'or'];
        $where[] = ['court_code_id', '=', '9941', 'or'];
        $where[] = ['court_code_id', '=', '9951', 'or'];
        $models = Jur::select(['jurs.*', 'justice_kinds.name AS justice_kind_name', 'judgment_codes.name AS judgment_code_name'])
            ->leftJoin('justice_kinds', 'jurs.justice_kind_id', '=', 'justice_kinds.id')
            ->leftJoin('judgment_codes', 'jurs.judgment_code_id', '=', 'judgment_codes.id')
            ->where($where)
            ->paginate(10);
        //$models = Jur::paginate(10);
        return view('Documents.Jur.index', [
                'models' => $models,
                'pagination_add' => [],
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jur  $jur
     * @return \Illuminate\Http\Response
     */
    public function show(Jur $jur)
    {
        //$model = Jur::findOrFail($jur);
        return view('Documents.Jur.show', [
                'model' => $jur,
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jur  $jur
     * @return \Illuminate\Http\Response
     */
    public function edit(Jur $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jur  $jur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jur $jur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jur  $jur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jur $jur)
    {
        //
    }
}
