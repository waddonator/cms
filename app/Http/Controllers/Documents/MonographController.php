<?php

namespace App\Http\Controllers\Documents;

use App\Monograph;
use App\Mauthor;
use App\Http\Requests\StoreMonographRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MonographController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Monograph::orderby('id')->paginate(20);
        return view('Documents.Monograph.index', [
                'title' => 'Documents',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        $mauthors = Mauthor::pluck('name','id')->prepend('',0);
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Documents.Monograph.create', ['title'=>$title, 'mauthors'=>$mauthors, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreMonographRequest $request)
    {
        $model = new Monograph;
        $model->name = $request->name;
        $model->mauthor_id = $request->mauthor_id;
        $model->novelty_date = $request->novelty_date;
        $model->save();
        
        return redirect()->route('monographs.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $title = 'Содержание монографии';
        $model = Monograph::findOrFail($id);
        $object = $model->create_contents();

        return view('Documents.Monograph.show', [
                'title'=>$title,
                'model'=>$model,
                'object'=>$object,
            ]);
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        $mauthors = Mauthor::pluck('name','id')->prepend('',0);
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = Monograph::findOrFail($id);
        return view('Documents.Monograph.edit', [
                'title'=>$title,
                'model'=>$model,
                'mauthors'=>$mauthors,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreMonographRequest $request, $id)
    {
        $model = Monograph::findOrFail($id);
        $model->name = $request->name;
        $model->mauthor_id = $request->mauthor_id;
        $model->novelty_date = $request->novelty_date;
        $model->save();
        return redirect()->route('monographs.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Monograph::findOrFail($id);
        $model->delete();
        return redirect(route('monographs.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
