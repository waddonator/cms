<?php

namespace App\Http\Controllers\Documents;

use App\Ncat;
use App\News;
use App\Http\Requests\StoreNewsRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-newspaper';
        $this->data['title'] = trans('cms.doc-news');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('news.index')]);
        $this->data['trash'] = News::where('trash','=',true)->count();
        $this->data['trash-route'] = route('news-trash');
    }


    /*
     * Список всех элементов
     */
    public function index(Request $request)
    {
        $where = [['trash','=',false]];
            if ($request->snro)  { $where[]=['nro','=', $request->snro]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = News::where($where)->orderby('id','desc')->paginate(20);
        return view('Documents.News.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }


    /*
     * Форма создания
     */
    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        $ncats = Ncat::orderby('name')->get();
        return view('Documents.News.create', [
                'ncats' => $ncats,
                'data' => $this->data,
            ]);
    }


    /*
     * Создание
     */
    public function store(StoreNewsRequest $request)
    {
        $model = new News;
        $model->name = $request->name;
        $model->date_effective = $request->date_effective;
        $model->date_event = $request->date_event;
        $model->content = $request->content ? $request->content : '';
        $model->export = $request->export;
        $model->save();
        $model->nro = $model->id + 16777216 * 39;
        $model->save();
        $model->ncats()->sync($request->ncats);
        Auth::user()->log($request->ip(), 2001, $model->id);
        return redirect()->route('news.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }


    /*
     * Просмотр
     */
    public function show($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-view') ,"url" => "#"]);
        $model = News::findOrFail($id);
        return view('Documents.News.show', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }


    /*
     * Форма изменения
     */
    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = News::findOrFail($id);
        $ncats = Ncat::orderby('name')->get();
        return view('Documents.News.edit', [
                'model'=>$model,
                'ncats' => $ncats,
                'data' => $this->data,
            ]);
    }


    /*
     * Изменение
     */
    public function update(StoreNewsRequest $request, $id)
    {
        $model = News::findOrFail($id);
        $model->nro = $model->id + 16777216 * 39;
        $model->name = $request->name;
        $model->date_effective = $request->date_effective;
        $model->date_event = $request->date_event;
        $model->content = $request->content ? $request->content : '';
        $model->export = $request->export;
        $model->save();
        $model->ncats()->sync($request->ncats);
        Auth::user()->log($request->ip(), 2002, $model->id);
        return redirect()->route('news.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }


    /*
     * Удаление, восстановление и помещение в корзину
     */
    public function destroy(Request $request, $id)
    {
        if (!isset($request->_action)){
            $model = News::findOrFail($id);
            $model->trash = true;
            $model->save();
            Auth::user()->log($request->ip(), 2003, $model->id);
            return redirect(route('news.index'))->with('error_message', trans('cms.alert-trash', ['name' => $model->name]));
        } else {
            if ($request->_action == 'RESTORE'){
                $model = News::findOrFail($id);
                $model->trash = false;
                $model->save();
                Auth::user()->log($request->ip(), 2004, $model->id);
                return redirect(route('news-trash'))->with('flash_message', trans('cms.alert-restore', ['name' => $model->name]));
            }
            if ($request->_action == 'DELETE'){
                $model = News::findOrFail($id);
                $model->delete();
                Auth::user()->log($request->ip(), 2005, $model->id);
                return redirect(route('news-trash'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
            }
        }
    }


    /*
     * Корзина
     */
    public function trash(Request $request)
    {
        $this->data['icon'] = 'icon-trash';
        $this->data['subtitle'] = trans('cms.trash');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);

        $where = [['trash','=',true]];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = News::where($where)->orderby('id','desc')->paginate(20);

        return view('Documents.News.trash', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }
}
