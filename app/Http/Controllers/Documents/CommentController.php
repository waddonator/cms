<?php

namespace App\Http\Controllers\Documents;

use App\Comment;
//use App\Commentlocal;
use App\Cauthor;
use App\Csource;
use App\Cdocsubtype;
use App\Act;

use App\Http\Requests\StoreCommentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-comment';
        $this->data['title'] = trans('cms.doc-act');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
        $this->data['trash'] = Act::where('trash','=',1)->count();
        $this->data['trash-route'] = route('act-trash');
    }

    public function index(Request $request)
    {
        // $where = [['trash','=',0]];
        //     if ($request->sid)  { $where[]=['id','=', $request->sid]; }
        //     if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        // $models = Comment::where($where)->orderby('id', 'desc')->paginate(20);
        // return view('Documents.Comment.index', [
        //         'models' => $models,
        //         'pagination_add' => $request->input(),
        //         'data' => $this->data,
        //     ]);
    }

    public function create(Request $request)
    {
        $act = Act::find($request->act_id);
        array_push($this->data['breadcrumbs'] , ["title" => $act->atype->name . ' ' . $act->number ,"url" => route('acts.show',$act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create-comment') ,"url" => "#"]);
        $cauthors = Cauthor::get();
        $csources = Csource::pluck('name','id')->prepend('',0);
        $cdocsubtypes = Cdocsubtype::pluck('name','id')->prepend('',0);
        return view('Documents.Comment.create', [
                'cauthors' => $cauthors,
                'csources' => $csources,
                'cdocsubtypes' => $cdocsubtypes,
                'act' => $act,
                'data' => $this->data,
            ]);
    }

    public function store(StoreCommentRequest $request)
    {
        $model = new Comment;
        $model->name = $request->name;
        $model->csource_id = $request->csource_id;
        $model->cdocsubtype_id = $request->cdocsubtype_id;
        $model->act_id = $request->act_id;
        $model->year = $request->year ? $request->year : 0;
        $model->issuefrom = $request->issuefrom ? $request->issuefrom : 0;
        $model->issueto = $request->issueto ? $request->issueto : 0;
        $model->position = $request->position ? $request->position : 0;
        $model->string = $request->string ? $request->string : '';
        $model->export = $request->export;
        $model->save();
        $model->nro = $model->id + 12 * 16777216;
        $model->save();
        Auth::user()->log($request->ip(), 6001, $model->id);
        $model->cauthors()->sync($request->cauthors);
        return redirect()->route('acts.show',$model->act_id)
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
        $model = Comment::findOrFail($id);
        array_push($this->data['breadcrumbs'] , ["title" => $model->act->atype->name . ' ' . $model->act->number ,"url" => route('acts.show',$model->act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => $model->name ,"url" => "#"]);
        return view('Documents.Comment.show', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function edit($id)
    {
        $model = Comment::findOrFail($id);
        array_push($this->data['breadcrumbs'] , ["title" => $model->act->atype->name . ' ' . $model->act->number ,"url" => route('acts.show',$model->act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit-comment') ,"url" => "#"]);
        $cauthors = Cauthor::get();
        $csources = Csource::pluck('name','id')->prepend('',0);
        $cdocsubtypes = Cdocsubtype::pluck('name','id')->prepend('',0);

        return view('Documents.Comment.edit', [
                'model'=>$model,
                'cauthors' => $cauthors,
                'csources' => $csources,
                'cdocsubtypes' => $cdocsubtypes,
                'data' => $this->data,
            ]);
    }

    public function update(StoreCommentRequest $request, $id)
    {
        $model = Comment::findOrFail($id);
        $model->name = $request->name;
        $model->csource_id = $request->csource_id;
        $model->cdocsubtype_id = $request->cdocsubtype_id;
        $model->year = $request->year ? $request->year : 0;
        $model->issuefrom = $request->issuefrom ? $request->issuefrom : 0;
        $model->issueto = $request->issueto ? $request->issueto : 0;
        $model->position = $request->position ? $request->position : 0;
        $model->string = $request->string ? $request->string : '';
        $model->export = $request->export;
        $model->save();
        Auth::user()->log($request->ip(), 6002, $model->id);
        $model->cauthors()->sync($request->cauthors);
        return redirect()->route('acts.show',$model->act_id)
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        if (!isset($request->_action)){
            $model = Comment::findOrFail($id);
            $model->trash = 1;
            $model->save();
            Auth::user()->log($request->ip(), 6003, $model->id);
            return redirect(route('acts.show',$model->act_id))->with('error_message', trans('cms.alert-trash', ['name' => $model->name]));
        } else {
            if ($request->_action == 'RESTORE'){
                $model = Comment::findOrFail($id);
                $model->trash = 0;
                $model->save();
                Auth::user()->log($request->ip(), 6004, $model->id);
                return redirect(route('acts.show',$model->act_id))->with('flash_message', trans('cms.alert-restore', ['name' => $model->name]));
            }
            if ($request->_action == 'DELETE'){
                $model = Comment::findOrFail($id);
                $model->delete();
                Auth::user()->log($request->ip(), 6005, $model->id);
                return redirect(route('acts.show',$model->act_id))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
            }
        }
    }


    /*
     * Корзина
     */

    public function trash(Request $request)
    {
        $this->data['icon'] = 'icon-trash';
        $this->data['subtitle'] = trans('cms.trash');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);

        $where = [['trash','=',true]];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Comment::where($where)->orderby('id', 'desc')->paginate(20);

        return view('Documents.Comment.trash', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

}
