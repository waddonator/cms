<?php

namespace App\Http\Controllers\Documents;

use App\Act;
use App\Actversion;
use App\Type;
use App\Asubact;
use App\Publisher;
use App\Akeyword;
use App\Source;

use App\Http\Requests\StoreActRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ActController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-law';
        $this->data['title'] = trans('cms.doc-act');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
        $this->data['trash'] = Act::where('trash','=',1)->count();
        $this->data['trash-route'] = route('act-trash');
    }

    public function index(Request $request)
    {
        $types = Type::pluck('name','id')->prepend('',0);

        $where = [['trash','=',0]];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
            if ($request->snumber)  { $where[]=['number','like', '%'.$request->snumber.'%']; }
            if ($request->stype)  { $where[]=['atype_id','=', $request->stype]; }
            if ($request->sexport==1)  { $where[]=['export','=', false]; }
            if ($request->sexport==2)  { $where[]=['export','=', true]; }
        $models = Act::where($where)->orderby('id', 'desc')->paginate(20);

        return view('Documents.Act.index', [
                'types' => $types,
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        $this->data['subtitle'] = 'Создание';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $types = Type::pluck('name','id')->prepend('',0);
        $asubacts = Asubact::pluck('name','id')->prepend('',0);
        $publishers = Publisher::get();
        $sources = Source::pluck('name','id');
        $akeywords = Akeyword::orderby('name')->pluck('name','id');
        return view('Documents.Act.create', [
                'types' => $types,
                'asubacts' => $asubacts,
                'publishers' => $publishers,
                'sources' => $sources,
                'akeywords' => $akeywords,
                'data' => $this->data,
            ]);
    }

    public function store(StoreActRequest $request)
    {
        $model = new Act;
        $model->name = $request->name;
        $model->atype_id = $request->atype_id;
        $model->number = $request->number;
        $model->asubact_id = $request->asubact_id;
        $model->date_acc = $request->date_acc;
        $model->date_force = $request->date_force;
        $model->reestr_code = $request->reestr_code;
        $model->reestr_date = $request->reestr_date;
        $model->reg_date = $request->reg_date;
        $model->reg_number = $request->reg_number;
        $model->finisher_id = $request->finisher_id ? $request->finisher_id : 0;
        $model->finish_date = $request->finish_date;
        $model->export = $request->export ? $request->export : false;
        $model->save();
        $model->nro = $model->id + 16777216 * $model->asubact_id;
        $model->save();
        Auth::user()->log($request->ip(), 1001, $model->id);

        $submodel = new Actversion;
        $submodel->act_id = $model->id;
        $submodel->date_acc = $model->date_acc;
        $submodel->modifier_id = 0;
        $submodel->format = 1;
        $submodel->content = $request->content ? $request->content : '';
        $submodel->save();
        Auth::user()->log($request->ip(), 1011, $submodel->id);

        $model->publishers()->sync($request->publishers);
        $model->sync_sources($request->sources);
        $model->sync_keywords($request->keywords);

        return redirect()->route('acts.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
        $model = Act::findOrFail($id);
        $this->data['title'] = $model->atype->name . ' ' . $model->number;
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => "#"]);
        return view('Documents.Act.show', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function edit($id)
    {
        $model = Act::findOrFail($id);
        $this->data['subtitle'] = $model->atype->name . ' ' . $model->number . ' (редактирование)';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $types = Type::pluck('name','id')->prepend('',0);
        $asubacts = Asubact::pluck('name','id')->prepend('',0);
        $publishers = Publisher::get();
        $sources = Source::pluck('name','id');
        $akeywords = Akeyword::orderby('name')->pluck('name','id');

        return view('Documents.Act.edit', [
                'model'=>$model,
                'types' => $types,
                'asubacts' => $asubacts,
                'publishers' => $publishers,
                'sources' => $sources,
                'akeywords' => $akeywords,
                'data' => $this->data,
            ]);
    }

    public function update(StoreActRequest $request, $id)
    {
        $model = Act::findOrFail($id);
        $model->name = $request->name;
        $model->atype_id = $request->atype_id;
        $model->number = $request->number;
        $model->asubact_id = $request->asubact_id;
        $model->date_acc = $request->date_acc;
        $model->date_force = $request->date_force;
        $model->reestr_code = $request->reestr_code;
        $model->reestr_date = $request->reestr_date;
        $model->reg_date = $request->reg_date;
        $model->reg_number = $request->reg_number;
        $model->finisher_id = $request->finisher_id ? $request->finisher_id : 0;
        $model->finish_date = $request->finish_date;
        $model->export = $request->export ? $request->export : false;
        $model->nro = $model->id + 16777216 * $model->asubact_id;
        $model->save();
        Auth::user()->log($request->ip(), 1002, $model->id);

        $model->publishers()->sync($request->publishers);
        $model->sync_sources($request->sources);
        $model->sync_keywords($request->keywords);

        return redirect()->route('acts.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        if (!isset($request->_action)){
            $model = Act::findOrFail($id);
            $model->trash = 1;
            $model->save();
            Auth::user()->log($request->ip(), 1003, $model->id);
            return redirect(route('acts.index'))->with('error_message', trans('cms.alert-trash', ['name' => $model->name]));
        } else {
            if ($request->_action == 'RESTORE'){
                $model = Act::findOrFail($id);
                $model->trash = 0;
                $model->save();
                Auth::user()->log($request->ip(), 1004, $model->id);
                return redirect(route('act-trash'))->with('flash_message', trans('cms.alert-restore', ['name' => $model->name]));
            }
            if ($request->_action == 'DELETE'){
                $model = Act::findOrFail($id);
                $model->delete();
                Auth::user()->log($request->ip(), 1005, $model->id);
                return redirect(route('act-trash'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
            }
        }
    }


    /*
     * Корзина
     */

    public function trash(Request $request)
    {
        $this->data['icon'] = 'icon-trash';
        $this->data['subtitle'] = trans('cms.trash');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);

        $types = Type::pluck('name','id')->prepend('',0);

        $where = [['trash','=',1]];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
            if ($request->snumber)  { $where[]=['number','like', '%'.$request->snumber.'%']; }
            if ($request->stype)  { $where[]=['atype_id','=', $request->stype]; }
        $models = Act::where($where)->orderby('id', 'desc')->paginate(20);

        return view('Documents.Act.trash', [
                'types' => $types,
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

}
