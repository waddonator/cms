<?php

namespace App\Http\Controllers\Documents;

use App\Publication;
use App\Pdocsubtype;
use App\Pglosstype;
use App\Psource;
use App\Pauthor;
use App\Http\Requests\StorePublicationRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PublicationController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-magazine';
        $this->data['title'] = trans('cms.doc-pub');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('publications.index')]);
        $this->data['trash'] = Publication::where('trash','=',true)->count();
        $this->data['trash-route'] = route('publication-trash');
    }


    /*
     * Список всех элементов
     */
    public function index(Request $request)
    {
        $where = [['trash','=',false]];
            if ($request->snro)  { $where[]=['nro','=', $request->snro]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Publication::where($where)->orderby('id','desc')->paginate(20);
        return view('Documents.Publication.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }


    /*
     * Форма создания
     */
    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        $pdocsubtypes = Pdocsubtype::pluck('name','id')->prepend('',0);
        $pglosstypes = Pglosstype::pluck('name','id')->prepend('',0);
        $psources = Psource::pluck('name','id')->prepend('',0);
        $pauthors = Pauthor::orderby('name')->get();
        return view('Documents.Publication.create', [
                'pdocsubtypes' => $pdocsubtypes,
                'pglosstypes' => $pglosstypes,
                'psources' => $psources,
                'pauthors' => $pauthors,
                'data' => $this->data,
            ]);
    }


    /*
     * Создание
     */
    public function store(StorePublicationRequest $request)
    {
        $model = new Publication;
        $model->name = $request->name;
        $model->novelty_date = $request->novelty_date;
        $model->pdocsubtype_id = $request->pdocsubtype_id;
        $model->pglosstype_id = $request->pglosstype_id;
        $model->subtitle = $request->subtitle ? $request->subtitle : '';
        $model->abstracttext = $request->abstracttext ? $request->abstracttext : '';
        $model->psource_id = $request->psource_id ? $request->psource_id : 0;
        $model->year = $request->year ? $request->year : 0;
        $model->issuefrom = $request->issuefrom ? $request->issuefrom : 0;
        $model->issueto = $request->issueto ? $request->issueto : 0;
        $model->position = $request->position ? $request->position : 0;
        $model->content = $request->content ? $request->content : '';
        $model->export = $request->export;
        $model->save();
        $model->nro = $model->id + 16777216 * 9;
        $model->save();
        $model->pauthors()->sync($request->pauthors);
        Auth::user()->log($request->ip(), 5001, $model->id);
        return redirect()->route('publications.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }


    /*
     * Просмотр
     */
    public function show($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-view') ,"url" => "#"]);
        $model = Publication::findOrFail($id);
        return view('Documents.Publication.show', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }


    /*
     * Форма изменения
     */
    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Publication::findOrFail($id);
        $pdocsubtypes = Pdocsubtype::pluck('name','id')->prepend('',0);
        $pglosstypes = Pglosstype::pluck('name','id')->prepend('',0);
        $psources = Psource::pluck('name','id')->prepend('',0);
        $pauthors = Pauthor::orderby('name')->get();
        return view('Documents.Publication.edit', [
                'model'=>$model,
                'pdocsubtypes' => $pdocsubtypes,
                'pglosstypes' => $pglosstypes,
                'psources' => $psources,
                'pauthors' => $pauthors,
                'data' => $this->data,
            ]);
    }


    /*
     * Изменение
     */
    public function update(StorePublicationRequest $request, $id)
    {
        $model = Publication::findOrFail($id);
        $model->nro = $model->id + 16777216 * 9;
        $model->name = $request->name;
        $model->novelty_date = $request->novelty_date;
        $model->pdocsubtype_id = $request->pdocsubtype_id;
        $model->pglosstype_id = $request->pglosstype_id;
        $model->subtitle = $request->subtitle ? $request->subtitle : '';
        $model->abstracttext = $request->abstracttext ? $request->abstracttext : '';
        $model->psource_id = $request->psource_id ? $request->psource_id : 0;
        $model->year = $request->year ? $request->year : 0;
        $model->issuefrom = $request->issuefrom ? $request->issuefrom : 0;
        $model->issueto = $request->issueto ? $request->issueto : 0;
        $model->position = $request->position ? $request->position : 0;
        $model->content = $request->content ? $request->content : '';
        $model->export = $request->export;
        $model->save();
        $model->pauthors()->sync($request->pauthors);
        Auth::user()->log($request->ip(), 5002, $model->id);
        return redirect()->route('publications.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }


    /*
     * Удаление, восстановление и помещение в корзину
     */
    public function destroy(Request $request, $id)
    {
        if (!isset($request->_action)){
            $model = Publication::findOrFail($id);
            $model->trash = true;
            $model->save();
            Auth::user()->log($request->ip(), 5003, $model->id);
            return redirect(route('publications.index'))->with('error_message', trans('cms.alert-trash', ['name' => $model->name]));
        } else {
            if ($request->_action == 'RESTORE'){
                $model = Publication::findOrFail($id);
                $model->trash = false;
                $model->save();
                Auth::user()->log($request->ip(), 5004, $model->id);
                return redirect(route('publication-trash'))->with('flash_message', trans('cms.alert-restore', ['name' => $model->name]));
            }
            if ($request->_action == 'DELETE'){
                $model = Publication::findOrFail($id);
                $model->delete();
                Auth::user()->log($request->ip(), 5005, $model->id);
                return redirect(route('publication-trash'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
            }
        }
    }


    /*
     * Корзина
     */
    public function trash(Request $request)
    {
        $this->data['icon'] = 'icon-trash';
        $this->data['subtitle'] = trans('cms.trash');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);

        $where = [['trash','=',true]];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Publication::where($where)->orderby('id','desc')->paginate(20);

        return view('Documents.Publication.trash', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }
}
