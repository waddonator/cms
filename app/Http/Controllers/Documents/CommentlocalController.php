<?php

namespace App\Http\Controllers\Documents;

use App\Comment;
use App\Commentlocal;
use App\Commentnro;

use App\Http\Requests\StoreCommentlocalRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CommentlocalController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-comment';
        $this->data['title'] = trans('cms.doc-act');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
    }

    public function index(Request $request)
    {
        return redirect()->route('home');
    }

    public function create(Request $request)
    {
        $comment_id = $request->comment_id ? $request->comment_id : 0;
        if (!$comment_id){
            return redirect()->route('comments.index')->with('error_message', 'Комментария не существует');
        }
        $comment = Comment::find($comment_id);
        array_push($this->data['breadcrumbs'] , ["title" => $comment->act->atype->name . ' ' . $comment->act->number ,"url" => route('acts.show',$comment->act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => $comment->name ,"url" => route('acts.show',$comment->act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create-comment-local') ,"url" => "#"]);
        $tocunits = $comment->act->get_tocunits();
        $actversions = $comment->act->actversions;
        
        return view('Documents.Commentlocal.create', [
                'comment' => $comment,
                'tocunits' => $tocunits,
                'actversions' => $actversions,
                'data' => $this->data,
            ]);
    }

    public function store(StoreCommentlocalRequest $request)
    {
        $model = new Commentlocal;
        $model->comment_id = $request->comment_id;
        $model->name = $request->name;
        $model->tocunit = $request->tocunit ? $request->tocunit : '';
        $model->content = $request->content ? $request->content : '';
        $model->export = $request->export;
        $model->nro = Commentnro::get_nro($model->comment_id, $model->tocunit);
        $model->save();
        $model->actversions()->sync($request->actversions);
        Auth::user()->log($request->ip(), 6011, $model->id);
        return redirect()->route('comments.show', $model->comment_id)
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
        $model = Commentlocal::findOrFail($id);
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-comloc'),"url" => route('comments.show', $model->comment_id)]);
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-view'),"url" => '#']);
        return view('Documents.Commentlocal.show', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function edit($id)
    {
        $model = Commentlocal::findOrFail($id);
        array_push($this->data['breadcrumbs'] , ["title" => $model->comment->act->atype->name . ' ' . $model->comment->act->number ,"url" => route('acts.show',$model->comment->act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => $model->comment->name ,"url" => route('acts.show',$model->comment->act->id)]);
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit-comment-local') ,"url" => "#"]);
        $tocunits = $model->comment->act->get_tocunits();
        $actversions = $model->comment->act->actversions;
        return view('Documents.Commentlocal.edit', [
                'model'=>$model,
                'tocunits' => $tocunits,
                'actversions' => $actversions,
                'data' => $this->data,
            ]);
    }

    public function update(StoreCommentlocalRequest $request, $id)
    {
        $model = Commentlocal::findOrFail($id);
        $model->name = $request->name;
        $model->tocunit = $request->tocunit ? $request->tocunit : '';
        $model->content = $request->content ? $request->content : '';
        $model->export = $request->export;
        $model->nro = Commentnro::get_nro($model->comment_id, $model->tocunit);
        $model->save();
        $model->actversions()->sync($request->actversions);
        Auth::user()->log($request->ip(), 6012, $model->id);
        return redirect()->route('comments.show', $model->comment_id)
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        if (!isset($request->_action)){
            $model = Commentlocal::findOrFail($id);
            $model->trash = 1;
            $model->save();
            Auth::user()->log($request->ip(), 6013, $model->id);
            return redirect(route('comments.show', $model->comment_id))->with('error_message', trans('cms.alert-trash', ['name' => $model->name]));
        } else {
            if ($request->_action == 'RESTORE'){
                $model = Commentlocal::findOrFail($id);
                $model->trash = 0;
                $model->save();
                Auth::user()->log($request->ip(), 6014, $model->id);
                return redirect(route('comments.show', $model->comment_id))->with('error_message', trans('cms.alert-restore', ['name' => $model->name]));
            }
            if ($request->_action == 'DELETE'){
                $model = Commentlocal::findOrFail($id);
                $model->delete();
                Auth::user()->log($request->ip(), 6015, $model->id);
                return redirect(route('comments.show', $model->comment_id))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
            }
        }

    }

}
