<?php

namespace App\Http\Controllers\Documents;

use App\Monograph;
use App\Mfragment;
use App\Mnodetype;
use App\Http\Requests\StoreMfragmentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MfragmentController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Mfragment::orderby('id')->paginate(20);
        return view('Documents.Mfragment.index', [
                'title' => 'Documents',
                'models' => $models,
            ]);
    }

    public function create(Request $request)
    {
        $title = trans('main.create-variant');
        $monograph_id = $request->monograph_id ? $request->monograph_id : 0;
        $mnodetypes = Mnodetype::pluck('name','id')->prepend('',0);
        $parents = Monograph::find($monograph_id)->mfragments->where('mnodetype_id','=',1)->pluck('name','id')->prepend('',0);
        return view('Documents.Mfragment.create', [
                'title'=>$title,
                'monograph_id'=>$monograph_id,
                'mnodetypes'=>$mnodetypes,
                'parents'=>$parents,
            ]);
    }

    public function store(StoreMfragmentRequest $request)
    {
        $model = new Mfragment;
        $model->name = $request->name;
        $model->monograph_id = $request->monograph_id;
        $model->mnodetype_id = $request->mnodetype_id;
        $model->parent_id = $request->parent_id;
        $model->type = $request->type ? $request->type : '';
        $model->nr = $request->nr ? $request->nr : '';
        $model->ordinal = $request->ordinal ? $request->ordinal : 0;
        $temp = Mfragment::find($request->parent_id);
        if ($temp) {
            $model->level = $temp->level + 1;
        }
        $model->content = '';
        $model->save();
        
        return redirect()->route('monographs.show', $request->monograph_id)
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $title = trans('main.edit-variant');
        $model = Mfragment::findOrFail($id);
        return view('Documents.Mfragment.show', [
                'title'=>$title,
                'model'=>$model,
            ]);
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        $model = Mfragment::findOrFail($id);
        $mnodetypes = Mnodetype::pluck('name','id')->prepend('',0);
        $parents = Monograph::find($model->monograph_id)->mfragments->where('mnodetype_id','=',1)->pluck('name','id')->prepend('',0);
        return view('Documents.Mfragment.edit', [
                'title'=>$title,
                'model'=>$model,
                'mnodetypes'=>$mnodetypes,
                'parents'=>$parents,
            ]);
    }

    public function update(StoreMfragmentRequest $request, $id)
    {
        $model = Mfragment::findOrFail($id);
        $model->name = $request->name;
        $model->monograph_id = $request->monograph_id;
        $model->mnodetype_id = $request->mnodetype_id;
        $model->parent_id = $request->parent_id;
        $model->type = $request->type ? $request->type : '';
        $model->nr = $request->nr ? $request->nr : '';
        $model->ordinal = $request->ordinal ? $request->ordinal : 0;
        $temp = Mfragment::find($request->parent_id);
        if ($temp) {
            $model->level = $temp->level + 1;
        }
        $model->content = $request->content ? $request->content : '';
        $model->save();
        return redirect()->route('monographs.show', $model->monograph_id)
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Mfragment::findOrFail($id);
        $model->delete();
        return redirect(route('mfragments.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
