<?php

namespace App\Http\Controllers\Documents;

use App\Ractversion;
use App\Ract;
use App\Http\Requests\StoreRactversionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RactversionController extends Controller
{
    public function __construct() {
        parent::__construct();
        // $this->data['icon'] = 'icon-law';
        // $this->data['title'] = trans('cms.doc-act');
        // array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
    }

    public function index(Request $request)
    {
        return redirect()->route('home');
    }

    public function create(Request $request)
    {
        // $act_id = $request->act_id ? $request->act_id : 0;
        // $act = Act::find($act_id);
        // $formats = ["Text", "HTML", "TOC"];
        // if (!$act){
        //     return redirect()->route('acts.index')->with('error_message', 'Законодательного акта не существует');
        // } else {
        //     $this->data['title'] = $act->atype->name . ' ' . $act->number;
        //     array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.show', $act_id)]);
        //     $this->data['subtitle'] = 'Создание редакции';
        //     array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => '#']);
        // }
        
        // return view('Documents.Actversion.create', [
        //         'act_id' => $act_id,
        //         'formats' => $formats,
        //         'data' => $this->data,
        //     ]);
    }

    public function store(StoreActversionRequest $request)
    {
        // $model = new Actversion;
        // $model->act_id = $request->act_id;
        // $model->date_acc = $request->date_acc;
        // $model->modifier_id = $request->modifier_id ? $request->modifier_id : 0;
        // $model->format = isset($request->format) ? $request->format : 1;
        // $model->versionid = isset($request->versionid) ? $request->versionid : 1;
        // $model->export = $request->export ? $request->export : false;
        // $model->content = $request->content ? $request->content : '';
        // // if(isset($request->date_start)){
        // //     $model->date_start = $request->date_start;
        // // }
        // // if(isset($request->date_interval_create_from_date_string())){
        // //     $model->date_end = $request->date_end;
        // // }
        // $model->save();
        // if ($model->export){
        //     $model->act->export = $model->export;
        //     $model->act->save();
        // }
        // Auth::user()->log($request->ip(), 1011, $model->id);
        // if ($model->updated_at > $model->act->updated_at){
        //     $model->act->updated_at = $model->updated_at;
        //     $model->act->save();
        // }

        // return redirect()->route('acts.show', $request->act_id)
        //     ->with('flash_message', trans('cms.alert-create', ['name' => $model->date_acc->format('Y-m-d')]));
    }

    public function show($id)
    {
        $model = Ractversion::findOrFail($id);
        $this->data['title'] = '111';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('racts.show', $model->act_id)]);
        $this->data['subtitle'] = 'Просмотр редакции от ' . $model->ed;
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => '#']);
        return view('Documents.Ractversion.show', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function edit($id)
    {
        // $model = Actversion::findOrFail($id);
        // $formats = ["Text", "HTML", "TOC"];
        // $this->data['title'] = $model->act->atype->name . ' ' . $model->act->number;
        // array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.show', $model->act_id)]);
        // $this->data['subtitle'] = 'Редактирование редакции';
        // array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => '#']);
        // return view('Documents.Actversion.edit', [
        //         'model'=>$model,
        //         'formats' => $formats,
        //         'data' => $this->data,
        //     ]);
    }

    public function update(StoreActversionRequest $request, $id)
    {
        // $model = Actversion::findOrFail($id);
        // $model->date_acc = $request->date_acc;
        // $model->modifier_id = $request->modifier_id ? $request->modifier_id : 0;
        // $model->format = isset($request->format) ? $request->format : $model->format;
        // $model->versionid = isset($request->versionid) ? $request->versionid : 1;
        // $model->date_start = $request->date_start ? $request->date_start : NULL;
        // $model->date_end = $request->date_end ? $request->date_end : NULL;
        // $model->export = $request->export ? $request->export : false;
        // $content = $request->content ? $request->content : '';
        // $content = str_replace('<br>','<br />',$content);
        // $content = str_replace('<hr>','',$content);
        // $model->content = $content;
        // $model->save();
        // if ($model->export){
        //     $model->act->export = $model->export;
        //     $model->act->save();
        // }
        // Auth::user()->log($request->ip(), 1012, $model->id);
        // if ($model->updated_at > $model->act->updated_at){
        //     $model->act->updated_at = $model->updated_at;
        //     $model->act->save();
        // }
        // return redirect()->route('acts.show', $model->act_id)
        //     ->with('flash_message', trans('cms.alert-update', ['name' => $model->date_acc->format('Y-m-d')]));
    }

    // public function destroy(Request $request, $id)
    // {
    //     if (!isset($request->_action)){
    //         $model = Actversion::findOrFail($id);
    //         $model->trash = 1;
    //         $model->save();
    //         Auth::user()->log($request->ip(), 1013, $model->id);
    //         return redirect(route('acts.show', $model->act_id))->with('error_message', trans('cms.alert-trash', ['name' => $model->date_acc->format('Y-m-d')]));
    //     } else {
    //         if ($request->_action == 'RESTORE'){
    //             $model = Actversion::findOrFail($id);
    //             $model->trash = 0;
    //             $model->save();
    //             Auth::user()->log($request->ip(), 1014, $model->id);
    //             return redirect(route('acts.show', $model->act_id))->with('error_message', trans('cms.alert-restore', ['name' => $model->date_acc->format('Y-m-d')]));
    //         }
    //         if ($request->_action == 'DELETE'){
    //             $model = Actversion::findOrFail($id);
    //             $model->delete();
    //             Auth::user()->log($request->ip(), 1015, $model->id);
    //             return redirect(route('acts.show', $model->act_id))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    //         }
    //     }

    // }

    public function destroy(Request $request, $id)
    {
        $model = Ractversion::findOrFail($id);
        $model->delete();
        //Auth::user()->log($request->ip(), 1143, $model->id);
        return redirect(route('racts.show',$model->act_id))->with('error_message', 'Редакция акта ' . $model->name . ' удалена');
    }

    public function rmeta($id)
    {
        $model = Ractversion::findOrFail($id);
        $this->data['title'] = 'Мета данные';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => "#"]);
        return view('Documents.Ractversion.rmeta', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function jcontent($id)
    {
        $model = Ractversion::findOrFail($id);
        $this->data['title'] = 'Контент в виде JSON';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => "#"]);
        return view('Documents.Ractversion.jcontent', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function rwork($id)
    {
        $model = Ractversion::findOrFail($id);
        $model->buildContent();
        //$model->changeLinks();
        $this->data['title'] = 'Преобразованный контент';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => "#"]);
        return view('Documents.Ractversion.rwork', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }
}
