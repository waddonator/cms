<?php

namespace App\Http\Controllers;

use App\Log;
//use App\Http\Requests\StoreLogactionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-eye4';
        $this->data['title'] = trans('cms.voc-logs');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('logs.index')]);
    }


    public function index(Request $request)
    {
        $where = [];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Log::where($where)->orderby('id', 'desc')->paginate(20);
        return view('Tools.Log.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }


    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Tools.Log.create', ['data'=>$this->data]);
    }


    public function store(Request $request)
    {
        $model = new Log;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('logs.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Log::findOrFail($id);
        return view('Tools.Log.edit', [
                'model'=>$model,
                'data'=>$this->data,
            ]);
    }

    public function update(Request $request, $id)
    {
        $model = Log::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        return redirect()->route('logs.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Log::findOrFail($id);
        $model->delete();
        return redirect(route('logs.index'))
            ->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
