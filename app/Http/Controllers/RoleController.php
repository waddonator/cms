<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreRoleRequest;

use Auth;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class RoleController extends Controller 
{

    public function __construct() {
        parent::__construct();
        array_push($this->data['breadcrumbs'] , ["title" => 'Роли',"url" => route('roles.index')]);
        $this->data['icon'] = 'icon-user-check';
        $this->data['title'] = 'Роли';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = Permission::pluck('name','id')->prepend('',0);
        $where = [];
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
            if ($request->spermission)  { $where[]=['permission_id','=', $request->spermission]; }
        $title = trans('main.roles');
        $roles = Role::distinct()->select('roles.*')->join('role_has_permissions', 'roles.id', '=', 'role_has_permissions.role_id')->where($where)->paginate(20);
        return view('users.roles.index', [
                'roles'=>$roles,
                'permissions'=>$permissions,
                'pagination_add'=>$request->input(),
                'data' => $this->data,
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['subtitle'] = 'Создание';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $permissions = Permission::all();//Get all permissions
        return view('users.roles.create', [
                'permissions' => $permissions,
                'data' => $this->data,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        $name = $request['name'];
        $role = new Role;
        $role->name = $name;

        $permissions = $request['permissions'];

        $role->save();
        //Looping thru selected permissions
        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); 
            //Fetch the newly created role and assign permission
            $role = Role::where('name', '=', $name)->first(); 
            $role->givePermissionTo($p);
        }

        return redirect()->route('roles.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.role'), 'name' => $role->name])); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['subtitle'] = 'Редактирование';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $role = Role::findOrFail($id);
        $permissions = Permission::all();
        return view('users.roles.edit', [
                'role'=>$role,
                'permissions'=>$permissions,
                'data' => $this->data,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);//Get role with the given id
        //Validate name and permission fields
        $this->validate($request, [
            'name' => 'required|max:20|unique:roles,name,' . $id,
            'permissions' => 'required',
        ]);

        $input = $request->except(['permissions']);
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }

        foreach ($permissions as $permission) {
            $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
            $role->givePermissionTo($p);  //Assign permission to role
        }

        return redirect()->route('roles.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.role'), 'name' => $role->name]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return redirect(route('roles.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.role'), 'name' => $role->name]));
    }
}