<?php

namespace App\Http\Controllers;

use App\Log;

use App\Jur;
use App\Type;
use App\CourtCode;
use App\Judge;
use App\Document;
use App\Document3;
use App\AdditionalData;

use App\Act;

use App\Monograph;
use App\Publication;

use App\News;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{

    public function __construct() {
        parent::__construct();
        set_time_limit(0);
    }

    public function index(Request $request)
    {
        if (Auth::check()) {
            $this->data['act'] = Act::stat();
            $this->data['pub'] = Publication::stat();
            $this->data['news'] = News::stat();

            return view('index',[
                    'act_count'=>Act::count(),
                    'jur_count'=>Jur::count(),
                    'mon_count'=>Monograph::count(),
                    'pub_count'=>Publication::count(),
                    'data' => $this->data,
                ]);
        } else {
            return redirect()->route('login');
        }
    }

    // public function opendata(Request $request)
    // {
    //     $object = (object)[];        
    //     if ($request->isMethod('post')){            
    //         $method = 'POST';

    //         for ($i = 0; $i < 10; $i++) { 

    //             $d_start = now();
    //             $d_end = now();
    //             $d_start->modify("-".($i+1)." day");
    //             $d_end->modify("-".$i." day");
    //             $object->{$i} = $d_start->format("Y-m-d") . ' - ' . $d_end->format("Y-m-d");

    //             //$object->{$i} = 'http://court.opendatabot.com/api/v1/court?adjudication_date_start=2019-01-01&adjudication_date_end=2019-01-01&limit=1000&start='.($i*1000).'&apiKey=Y4h47HWteJhG';

    //             $object->{$i} = 'http://court.opendatabot.com/api/v1/court?adjudication_date_start='.($d_start->format("Y-m-d")).'&adjudication_date_end='.($d_end->format("Y-m-d")).'&limit=1000&apiKey=Y4h47HWteJhG';

    //             // if ($stream = fopen('http://court.opendatabot.com/api/v1/court?adjudication_date_start='.($d_start->format("Y-m-d")).'&adjudication_date_end='.($d_end->format("Y-m-d")).'&limit=1000&apiKey=Y4h47HWteJhG', 'r')) {
    //             //     $object = json_decode( stream_get_contents($stream) );
    //             //     fclose($stream);
    //             // }

    //             // foreach ($object->items as $key => $item) {

    //             //     $model = Jur::find($item->doc_id);
    //             //     if (!$model){
    //             //         $model = new Jur;
    //             //         $model->id = $item->doc_id;
    //             //         $model->cause_num = $item->cause_num;
    //             //         $model->judge = $item->judge;
    //             //         $model->adjudication_date = $item->adjudication_date;
    //             //         $model->receipt_date = $item->receipt_date;

    //             //         $model->court_code_id = $item->court_code;
    //             //         $model->justice_kind_id = $item->justice_kind;
    //             //         $model->judgment_code_id = $item->judgment_code;
    //             //         $model->category_code_id = $item->category_code;

    //             //         if ($stream = fopen('http://court.opendatabot.com/api/v1/court/'.($item->doc_id).'?apiKey=Y4h47HWteJhG', 'r')) {
    //             //             $item_detail = json_decode( stream_get_contents($stream) );
    //             //             fclose($stream);
    //             //             $model->text = $item_detail->text;
    //             //         }

    //             //         $model->save();
    //             //     }
    //             // }
    //         }

    //     } else {
    //         $method = 'GET';
    //     }

    //     return view('Tools.OpenData.index',[
    //             'method' => $method,
    //             'object' => $object,
    //         ]);
    // }

    // public function opendata_scan(Request $request)
    // {
    //     $object = (object)[];

    //     $stamp_start = now();

    //     $object->stamp_start = now()->format("Y-m-d H:i:s");
    //     $object->stamp_end = now()->format("Y-m-d H:i:s");
    //     $object->count = $request->input('count');
    //     $object->iteration = $request->input('iteration');
    //     $object->next_iteration = $request->input('iteration');
    //     $d_start = new \DateTime($request->input('date_start'));
    //     $d_end   = new \DateTime($request->input('date_start'));
    //     $d_end->modify("+1 day");
    //     $object->date_start = $d_start->format("Y-m-d");
    //     $object->next_date = $d_start->format("Y-m-d");
    //     //$object->date_end = $d_end->format("Y-m-d");
    //     //$object->type = $request->input('type');

    //     if ($stream = fopen('http://court.opendatabot.com/api/v1/court?adjudication_date_start='.($d_start->format("Y-m-d")).'&adjudication_date_end='.($d_end->format("Y-m-d")).'&limit='.($request->input('count')).'&start='.($request->input('iteration') * $request->input('count')).'&apiKey=Y4h47HWteJhG', 'r')) {
    //         $data = json_decode( stream_get_contents($stream) );
    //         fclose($stream);
    //     }

    //     if(isset($data->count)){$object->itemsInBase=$data->count;}
    //     if(!isset($data->items) || count($data->items) == 0 || $data->count < ( ($request->input('iteration')+1) * $request->input('count'))){
    //         $object->next_iteration=0;
    //         $d_start->modify("-1 day");
    //         $object->next_date = $d_start->format("Y-m-d");
    //     } else {
    //         $object->next_iteration++;
    //     }

    //     // if(isset($data->count)){$object->itemsInBase=$data->count;}
    //     // if(isset($data->items) ){
    //     //     if (count($data->items) == 0 || $data->count < ( ($request->input('iteration')+1) * $request->input('count'))){
    //     //         $object->next_iteration=0;
    //     //         $d_start->modify("-1 day");
    //     //         $object->next_date = $d_start->format("Y-m-d");
    //     //     } else {
    //     //         $object->next_iteration++;
    //     //     }
    //     // }

    //     $object->stamp_end = now()->format("Y-m-d H:i:s");
    //     $stamp_end = now();
    //     return json_encode($object);
    //     //return $object;
    // }


    // public function support(Request $request)
    // {
    //     return view('support');
    // }

    public function create_judges(Request $request)
    {
        $elements = Jur::select('id', 'judge')->get();
        foreach ($elements as $key => $element) {
            //$full_element = Jur::find($element->id);
            $judge = $element->judge;
            $model = Judge::where('name','=',$judge)->first();
            if (!$model){
                $model = new Judge;
                $model->name = $judge;
                $model->save();
            }
        }
        return redirect()->route('judges.index')
            ->with('flash_message', 'Справочник судей создан');
    }


    public function create_blocks_map(Request $request)
    {
        AdditionalData::create_blocks();
        AdditionalData::create_blocks_map();
        return redirect()->route('tools')
            ->with('flash_message', 'Карта блоков создана');
    }


    public function account(Request $request)
    {
        $this->data['title'] = trans('cms.account');
        $this->data['icon'] = 'icon-cog5';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => "#"]);
        return view('blank',[
                'data' => $this->data,
            ]);
    }


    /**
     * Вход в систему
     */
    public function singin(Request $request)
    {
        Auth::user()->log($request->ip(), 1);
        return redirect()->route('home');
    }

    /**
     * Выход из системы
     */
    public function logout(Request $request)
    {
        Auth::user()->log($request->ip(), 2);
        Auth::logout();
        return redirect()->route('home');
    }

    /**
     * 
     */
    public function tools(Request $request)
    {
        $this->data['title'] = trans('cms.tools');
        $this->data['icon'] = 'icon-hammer-wrench';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => "#"]);
        return view('Tools.index',[
                'data' => $this->data,
            ]);
    }

}
