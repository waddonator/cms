<?php

/***********************************************
 *
 * Rada
 *
 ***********************************************/

namespace App\Http\Controllers\Api;

use App\Ract;
use App\Ractversion;
use App\Raction;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\Auth;

class RadaController extends Controller
{


    #
    # Получение полного списка документов с сайта Верховной Рады
    #
    public function docs()
    {
        # находим модель raction и слитываем Last-Modified
            $model = Raction::findOrFail(1);
            $LastModified = '';
            if ($model->response_header){
                foreach ($model->response_header as $key => $row) {
                    if (mb_substr($row,0,13) == 'Last-Modified') {$LastModified = 'If-Modified-Since: ' . mb_substr($row,15);}
                }
            }
        # запрашиваем список с сайта ВР
            set_time_limit(0);
            $opts = [
                'http' => [
                    'method'=>"GET",
                    'header'=>"Accept-language: en\r\n" .
                        "Cookie: OpenData=OpenData\r\n" . $LastModified
                ]
            ];
            $context = stream_context_create($opts);

            $url = 'http://data.rada.gov.ua/ogd/zak/laws/data/csv/doc.txt';
            $html_request = file_get_contents($url, false, $context);
        # сохраняем заголовок ответа
            if ($http_response_header){
                foreach ($http_response_header as $key => $row) {
                    if (mb_substr($row,0,13) == 'Last-Modified') {
                        $model->response_header = $http_response_header;
                        $model->save();
                    }
                }
            }
            // dump($http_response_header);
            // dd($model->response_header);
        # если ответ 200 - начинаем сканировать ответ
            $elements = explode("\n", $html_request);
            $object = (object)[];
            $result = 0;
            # сканируем весь полученный массив
                foreach ($elements as $key => $row) {
                    $array = explode("\t", mb_convert_encoding($row, "utf-8", "windows-1251"));
                    if (isset($array[0]) && (int)$array[0]){
                        $element = Ract::find($array[0]);
                        if (!$element){
                            $element = new Ract;
                            $element->id = $array[0];
                            $element->nreg = $array[1];
                            $element->name = $array[2];
                            $element->rstatus_id = $array[3];
                            $element->typ = $array[4];
                            $element->organs = $array[5];
                            $element->rmeta = $object;
                            $element->save();
                            $element->chooseSubact();
                            $result++;
                        }
                    }
                }
        # готовим ответ
            $data['result'] = '(Новых актов - ' . $result . ')';

        return response()->json($data);
    }



    #
    # Получение метаданных и контента одного акта с сайта Верховной Рады
    # и создание списка редакций
    #
    public function lastmeta()
    {
        set_time_limit(0);

        # !!! Добавить работу в транзакции

        $element = Ract::where('rbotstatus_id','=',0)->orderby('id', 'asc')->first();
        if ($element){
            $element->rbotstatus_id = 1;
            $element->save();
            $element->downloadLastMeta();
            if ($element->sync_ractversions($element->rmeta)){
                $element->rbotstatus_id = 2;
                $element->save();
            }
            // $data = [
            //     'http_response_header' => $http_response_header, #---понадобится для анализа ответа сервера
            // ];
            $data = ['result'=>'('.$element->id.')'];
        } else {
            $data = ['result'=>'-'];
        }

        return response()->json($data);
    }



    #----------------------------------------------------------
    # Получение метаданных и контента одной редакции акта с сайта Верховной Рады
    #----------------------------------------------------------
    public function getRadaDocument()
    {
        set_time_limit(0);

        # с помощью транзакции отбираем элемент для загрузки редакции
            $element = DB::transaction(function () {
                $element = Ractversion::where('rbotstatus_id','=',0)->orderby('id', 'asc')->first(); #все
                //$element = Ractversion::where('rbotstatus_id','=',1)->orderby('id')->first(); #произошли ошибки и отметка осталаст
                //$element = Ractversion::where([['act_id','=',82054],['rbotstatus_id','=',0]])->orderby('id')->first(); #какие-то произвольные
                if ($element){
                    $element->rbotstatus_id = 1;
                    $element->save();
                }
                return $element;
            });
        # если элемент существует - загружаем с ВР
            if ($element){
                $element->getRadaDocument();
                $data = ['result'=>'{'.$element->id.'}'];
            } else {
                $data = ['result'=>'-'];
            }

        return response()->json($data);
    }



    #
    # Получение списка измененных документов с сайта Верховной Рады
    #
    public function updatedDocs()
    {
        # находим модель raction и слитываем Last-Modified
            $model = Raction::findOrFail(4);
            $LastModified = '';
            if ($model->response_header){
                foreach ($model->response_header as $key => $row) {
                    if (mb_substr($row,0,13) == 'Last-Modified') {$LastModified = 'If-Modified-Since: ' . mb_substr($row,15);}
                }
            }
        # запрашиваем список с сайта ВР
            set_time_limit(0);
            $opts = [
                'http' => [
                    'method'=>"GET",
                    'header'=>"Accept-language: en\r\n" .
                        "Cookie: OpenData=OpenData\r\n" . $LastModified
                ]
            ];
            $context = stream_context_create($opts);

            $url = 'http://data.rada.gov.ua/laws/main/r.json';
            $html_request = file_get_contents($url, false, $context);
        # сохраняем заголовок ответа
            if ($http_response_header){
                foreach ($http_response_header as $key => $row) {
                    if (mb_substr($row,0,13) == 'Last-Modified') {
                        $model->response_header = $http_response_header;
                        $model->save();
                    }
                }
            }
            // dump($http_response_header);
            // dd(json_decode($html_request));
        # если ответ 200 - ставим отметки о необходимости получения данных
            $result = 0;
            $elements = json_decode($html_request);
            if ($elements){
                DB::table('racts')->whereIn('id',$elements)->update(['rbotstatus_id' => 0]);
                $result = count($elements);
            }
        # готовим ответ
            $data['result'] = '(Измененных актов - ' . $result . ')';

        return response()->json($data);
    }


    #----------------------------------------------------------
    # Сборка контента одной записи
    #----------------------------------------------------------
    public function buildContent()
    {
        set_time_limit(0);

        $elements = Ractversion::select('id')->where([/*['act_id','=',82054],*/['rbotstatus_id','=',2]])->limit(10)->orderby('id','DESC')->get();
        //$elements = Ractversion::select('id')->where([/*['act_id','=',82054],*/['rbotstatus_id','=',13]])->limit(10)->get();

        foreach ($elements as $key => $row) {
            $element = Ractversion::find($row->id);
            if ($element) {
                $element->buildContent();
                $element->changeLinks();
            }
        }
        $data['result'] = '(+' . $elements->count() . ')';
        return response()->json($data);
    }


    #----------------------------------------------------------
    # Временная, пока не сравняются с контентами
    #----------------------------------------------------------
    public function changeLinks($id=0)
    {
        # устанавливаем неограниченный временной лимит
            set_time_limit(0);
        # если есть параметр id, то берем запись с таким номером,
        # если нет - то любою со статутом 13 
            if ($id){
                $where = [['act_id','=',$id]];
            } else {
                $where = [['rbotstatus_id','=',13]];
            }
        # отбираем элементы
            $elements = Ractversion::select('id')
                ->where($where)
                ->limit(1)
                ->get();
        # пробегаем по всем элементам и меняем ссылки
            foreach ($elements as $key => $row) {
                $element = Ractversion::find($row->id);
                if ($element) {
                    $element->changeLinks();
                }
            }
        # результат - количество отобранных элементов
            $data['result'] = '(+' . $elements->count() . ')';
        return response()->json($data);
    }

}
