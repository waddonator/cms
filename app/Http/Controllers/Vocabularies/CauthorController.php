<?php

namespace App\Http\Controllers\Vocabularies;

use App\Cauthor;
use App\Cauthorrole;
use App\Http\Requests\StoreCauthorRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CauthorController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-cauthor');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-act'),"url" => route('acts.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('cauthors.index')]);
    }

    public function index(Request $request)
    {
        $models = Cauthor::orderby('id')->paginate(20);
        return view('Vocabularies.Cauthor.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        $cauthorroles = Cauthorrole::get();
        return view('Vocabularies.Cauthor.create', [
                'cauthorroles' => $cauthorroles,
                'data' => $this->data,
            ]);
    }

    public function store(StoreCauthorRequest $request)
    {
        $model = new Cauthor;
        $model->name = $request->name;
        $model->save();
        $model->cauthorroles()->sync($request->cauthorroles);
        Auth::user()->log($request->ip(), 6101, $model->id);
        return redirect()->route('cauthors.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Cauthor::findOrFail($id);
        $cauthorroles = Cauthorrole::get();
        return view('Vocabularies.Cauthor.edit', [
                'model'=>$model,
                'cauthorroles' => $cauthorroles,
                'data' => $this->data,
            ]);
    }

    public function update(StoreCauthorRequest $request, $id)
    {
        $model = Cauthor::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        $model->cauthorroles()->sync($request->cauthorroles);
        Auth::user()->log($request->ip(), 6102, $model->id);
        return redirect()->route('cauthors.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Cauthor::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 6103, $model->id);
        return redirect(route('cauthors.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
