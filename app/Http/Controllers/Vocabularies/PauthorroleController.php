<?php

namespace App\Http\Controllers\Vocabularies;

use App\Pauthorrole;
use App\Http\Requests\StorePauthorroleRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PauthorroleController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-pauthorrole');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-pub'),"url" => route('publications.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('pauthorroles.index')]);
    }

    public function index(Request $request)
    {
        $models = Pauthorrole::orderby('id')->paginate(20);
        return view('Vocabularies.Pauthorrole.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Pauthorrole.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StorePauthorroleRequest $request)
    {
        $model = new Pauthorrole;
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 5111, $model->id);
        return redirect()->route('pauthorroles.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Pauthorrole::findOrFail($id);
        return view('Vocabularies.Pauthorrole.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StorePauthorroleRequest $request, $id)
    {
        $model = Pauthorrole::findOrFail($id);
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 5112, $model->id);
        return redirect()->route('pauthorroles.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Pauthorrole::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 5113, $model->id);
        return redirect(route('pauthorroles.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
