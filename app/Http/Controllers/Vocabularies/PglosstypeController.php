<?php

namespace App\Http\Controllers\Vocabularies;

use App\Pglosstype;
use App\Http\Requests\StorePglosstypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PglosstypeController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-pglosstype');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-pub'),"url" => route('publications.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('pglosstypes.index')]);
    }

    public function index(Request $request)
    {
        $models = Pglosstype::orderby('id')->paginate(20);
        return view('Vocabularies.Pglosstype.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Pglosstype.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StorePglosstypeRequest $request)
    {
        $model = new Pglosstype;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 5121, $model->id);
        return redirect()->route('pglosstypes.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Pglosstype::findOrFail($id);
        return view('Vocabularies.Pglosstype.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StorePglosstypeRequest $request, $id)
    {
        $model = Pglosstype::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 5122, $model->id);
        return redirect()->route('pglosstypes.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Pglosstype::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 5123, $model->id);
        return redirect(route('pglosstypes.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
