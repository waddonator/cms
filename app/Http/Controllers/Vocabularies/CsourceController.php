<?php

namespace App\Http\Controllers\Vocabularies;

use App\Csource;
use App\Http\Requests\StoreCsourceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CsourceController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-psource');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-act'),"url" => route('acts.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('csources.index')]);
    }

    public function index(Request $request)
    {
        $models = Csource::orderby('id')->paginate(20);
        return view('Vocabularies.Csource.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Csource.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreCsourceRequest $request)
    {
        $model = new Csource;
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 6121, $model->id);
        return redirect()->route('csources.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Csource::findOrFail($id);
        return view('Vocabularies.Csource.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreCsourceRequest $request, $id)
    {
        $model = Csource::findOrFail($id);
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 6122, $model->id);
        return redirect()->route('csources.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Csource::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 6123, $model->id);
        return redirect(route('csources.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
