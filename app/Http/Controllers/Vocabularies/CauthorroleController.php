<?php

namespace App\Http\Controllers\Vocabularies;

use App\Cauthorrole;
use App\Http\Requests\StoreCauthorroleRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CauthorroleController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-pauthorrole');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-act'),"url" => route('acts.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('cauthorroles.index')]);
    }

    public function index(Request $request)
    {
        $models = Cauthorrole::orderby('id')->paginate(20);
        return view('Vocabularies.Cauthorrole.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Cauthorrole.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreCauthorroleRequest $request)
    {
        $model = new Cauthorrole;
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 6111, $model->id);
        return redirect()->route('cauthorroles.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Cauthorrole::findOrFail($id);
        return view('Vocabularies.Cauthorrole.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreCauthorroleRequest $request, $id)
    {
        $model = Cauthorrole::findOrFail($id);
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 6112, $model->id);
        return redirect()->route('cauthorroles.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Cauthorrole::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 6113, $model->id);
        return redirect(route('cauthorroles.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
