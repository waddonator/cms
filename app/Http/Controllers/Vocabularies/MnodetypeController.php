<?php

namespace App\Http\Controllers\Vocabularies;

use App\Mnodetype;
use App\Http\Requests\StoreMnodetypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MnodetypeController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Mnodetype::orderby('id')->paginate(20);
        return view('Vocabularies.Mnodetype.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.Mnodetype.create', ['title'=>$title, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreMnodetypeRequest $request)
    {
        $model = new Mnodetype;
        // $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('mnodetypes.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = Mnodetype::findOrFail($id);
        return view('Vocabularies.Mnodetype.edit', [
                'title'=>$title,
                'model'=>$model,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreMnodetypeRequest $request, $id)
    {
        $model = Mnodetype::findOrFail($id);
        // $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('mnodetypes.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Mnodetype::findOrFail($id);
        $model->delete();
        return redirect(route('mnodetypes.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
