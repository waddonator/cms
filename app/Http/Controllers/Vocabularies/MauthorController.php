<?php

namespace App\Http\Controllers\Vocabularies;

use App\Mauthor;
use App\Http\Requests\StoreMauthorRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MauthorController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Mauthor::orderby('id')->paginate(20);
        return view('Vocabularies.Mauthor.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.Mauthor.create', ['title'=>$title, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreMauthorRequest $request)
    {
        $model = new Mauthor;
        // $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('mauthors.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = Mauthor::findOrFail($id);
        return view('Vocabularies.Mauthor.edit', [
                'title'=>$title,
                'model'=>$model,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreMauthorRequest $request, $id)
    {
        $model = Mauthor::findOrFail($id);
        // $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('mauthors.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Mauthor::findOrFail($id);
        $model->delete();
        return redirect(route('mauthors.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
