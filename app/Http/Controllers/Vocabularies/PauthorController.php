<?php

namespace App\Http\Controllers\Vocabularies;

use App\Pauthor;
use App\Pauthorrole;
use App\Http\Requests\StorePauthorRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PauthorController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-pauthor');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-pub'),"url" => route('publications.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('pauthors.index')]);
    }

    public function index(Request $request)
    {
        $models = Pauthor::orderby('id')->paginate(20);
        return view('Vocabularies.Pauthor.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        $pauthorroles = Pauthorrole::get();
        return view('Vocabularies.Pauthor.create', [
                'pauthorroles' => $pauthorroles,
                'data' => $this->data,
            ]);
    }

    public function store(StorePauthorRequest $request)
    {
        $model = new Pauthor;
        $model->name = $request->name;
        $model->save();
        $model->pauthorroles()->sync($request->pauthorroles);
        Auth::user()->log($request->ip(), 5101, $model->id);
        return redirect()->route('pauthors.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Pauthor::findOrFail($id);
        $pauthorroles = Pauthorrole::get();
        return view('Vocabularies.Pauthor.edit', [
                'model'=>$model,
                'pauthorroles' => $pauthorroles,
                'data' => $this->data,
            ]);
    }

    public function update(StorePauthorRequest $request, $id)
    {
        $model = Pauthor::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        $model->pauthorroles()->sync($request->pauthorroles);
        Auth::user()->log($request->ip(), 5102, $model->id);
        return redirect()->route('pauthors.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Pauthor::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 5103, $model->id);
        return redirect(route('pauthors.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
