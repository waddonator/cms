<?php

namespace App\Http\Controllers\Vocabularies;

use App\Judge;
use App\Http\Requests\StoreJudgeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JudgeController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Judge::orderby('id')->paginate(20);
        return view('Vocabularies.Judge.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.Judge.create', ['title'=>$title, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreJudgeRequest $request)
    {
        $model = new Judge;
        // $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('judges.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = Judge::findOrFail($id);
        return view('Vocabularies.Judge.edit', [
                'title'=>$title,
                'model'=>$model,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreJudgeRequest $request, $id)
    {
        $model = Judge::findOrFail($id);
        // $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('judges.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Judge::findOrFail($id);
        $model->delete();
        return redirect(route('judges.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
