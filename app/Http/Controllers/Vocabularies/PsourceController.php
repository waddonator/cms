<?php

namespace App\Http\Controllers\Vocabularies;

use App\Psource;
use App\Http\Requests\StorePsourceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PsourceController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-psource');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-pub'),"url" => route('publications.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('psources.index')]);
    }

    public function index(Request $request)
    {
        $models = Psource::orderby('id')->paginate(20);
        return view('Vocabularies.Psource.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Psource.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StorePsourceRequest $request)
    {
        $model = new Psource;
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 5131, $model->id);
        return redirect()->route('psources.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Psource::findOrFail($id);
        return view('Vocabularies.Psource.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StorePsourceRequest $request, $id)
    {
        $model = Psource::findOrFail($id);
        $model->name = $request->name;
        $model->shortname = $request->shortname;
        $model->save();
        Auth::user()->log($request->ip(), 5132, $model->id);
        return redirect()->route('psources.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Psource::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 5133, $model->id);
        return redirect(route('psources.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
