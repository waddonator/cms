<?php

namespace App\Http\Controllers\Vocabularies;

use App\Akeyword;
use App\Http\Requests\StoreAkeywordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AkeywordController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-akeyword');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('akeywords.index')]);
    }

    public function index(Request $request)
    {
        $models = Akeyword::orderby('id')->paginate(20);
        return view('Vocabularies.Akeyword.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Vocabularies.Akeyword.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreAkeywordRequest $request)
    {
        $model = new Akeyword;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1141, $model->id);
        return redirect()->route('akeywords.index')
            ->with('flash_message', 'Ключевое слово ' . $model->name . ' создано');
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Akeyword::findOrFail($id);
        return view('Vocabularies.Akeyword.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreAkeywordRequest $request, $id)
    {
        $model = Akeyword::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1142, $model->id);
        return redirect()->route('akeywords.index')
            ->with('flash_message', 'Ключевое слово ' . $model->name . ' изменено');
    }

    public function destroy(Request $request, $id)
    {
        $model = Akeyword::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 1143, $model->id);
        return redirect(route('akeywords.index'))->with('error_message', 'Ключевое слово ' . $model->name . ' удалено');
    }

}
