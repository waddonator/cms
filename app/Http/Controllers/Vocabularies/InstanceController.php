<?php

namespace App\Http\Controllers\Vocabularies;

use App\Instance;
use App\Http\Requests\StoreInstanceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstanceController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Instance::orderby('id')->paginate(20);
        return view('Vocabularies.Instance.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.Instance.create', ['title'=>$title, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreInstanceRequest $request)
    {
        $model = new Instance;
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('instances.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = Instance::findOrFail($id);
        return view('Vocabularies.Instance.edit', [
                'title'=>$title,
                'model'=>$model,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreInstanceRequest $request, $id)
    {
        $model = Instance::findOrFail($id);
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('instances.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Instance::findOrFail($id);
        $model->delete();
        return redirect(route('instances.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
