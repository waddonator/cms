<?php

namespace App\Http\Controllers\Vocabularies;

use App\Cdocsubtype;
use App\Http\Requests\StoreCdocsubtypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CdocsubtypeController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-cdocsubtype');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-act'),"url" => route('acts.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('cdocsubtypes.index')]);
    }

    public function index(Request $request)
    {
        $models = Cdocsubtype::orderby('id')->paginate(20);
        return view('Vocabularies.Cdocsubtype.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Cdocsubtype.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreCdocsubtypeRequest $request)
    {
        $model = new Cdocsubtype;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 6131, $model->id);
        return redirect()->route('cdocsubtypes.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Cdocsubtype::findOrFail($id);
        return view('Vocabularies.Cdocsubtype.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreCdocsubtypeRequest $request, $id)
    {
        $model = Cdocsubtype::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 6132, $model->id);
        return redirect()->route('cdocsubtypes.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Cdocsubtype::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 6133, $model->id);
        return redirect(route('cdocsubtypes.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
