<?php

namespace App\Http\Controllers\Vocabularies;

use App\CourtCode;
use App\Region;
use App\Http\Requests\StoreCourtCodeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CourtCodeController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = CourtCode::orderby('id')->paginate(20);
        return view('Vocabularies.CourtCode.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        $regions = Region::pluck('name','id')->prepend('',0);
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.CourtCode.create', [
                'title'=>$title,
                'regions'=>$regions,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function store(StoreCourtCodeRequest $request)
    {
        $model = new CourtCode;
        $model->id = $request->id;
        $model->name = $request->name;
        $model->region_id = $request->region_id;
        $model->save();
        
        return redirect()->route('court-code.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = CourtCode::findOrFail($id);
        $regions = Region::pluck('name','id')->prepend('',0);
        return view('Vocabularies.CourtCode.edit', [
                'title'=>$title,
                'model'=>$model,
                'regions'=>$regions,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreCourtCodeRequest $request, $id)
    {
        $model = CourtCode::findOrFail($id);
        $model->id = $request->id;
        $model->name = $request->name;
        $model->region_id = $request->region_id;
        $model->save();
        return redirect()->route('court-code.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = CourtCode::findOrFail($id);
        $model->delete();
        return redirect(route('court-code.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
