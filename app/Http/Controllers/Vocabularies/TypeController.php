<?php

namespace App\Http\Controllers\Vocabularies;

use App\Type;
use App\Http\Requests\StoreTypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TypeController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-type');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('types.index')]);
    }


    public function index(Request $request)
    {
        $where = [];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Type::where($where)->orderby('id')->paginate(20);
        return view('Vocabularies.Type.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }


    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Vocabularies.Type.create', ['data'=>$this->data]);
    }


    public function store(StoreTypeRequest $request)
    {
        $model = new Type;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1111, $model->id);
        return redirect()->route('types.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Type::findOrFail($id);
        return view('Vocabularies.Type.edit', [
                'model'=>$model,
                'data'=>$this->data,
            ]);
    }

    public function update(StoreTypeRequest $request, $id)
    {
        $model = Type::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1112, $model->id);
        return redirect()->route('types.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Type::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 1113, $model->id);
        return redirect(route('types.index'))
            ->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
