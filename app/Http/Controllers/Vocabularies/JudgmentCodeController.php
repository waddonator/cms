<?php

namespace App\Http\Controllers\Vocabularies;

use App\JudgmentCode;
use App\Http\Requests\StoreVariantRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class JudgmentCodeController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = JudgmentCode::paginate(10);
        return view('Vocabularies.JudgmentCode.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.JudgmentCode.create', ['title'=>$title, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreVariantRequest $request)
    {
        $variant = new Variant;
        $variant->name = $request->name;
        $variant->save();
        $variant->sync_answers(json_decode($request['answers']));
        
        return redirect()->route('JudgmentCode.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $variant->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $variant = Variant::findOrFail($id);
        return view('Vocabularies.JudgmentCode.edit', [
                'title'=>$title,
                'variant'=>$variant,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreVariantRequest $request, $id)
    {
        $variant = Variant::findOrFail($id);
        $variant->name = $request['name'];
        $variant->save();
        $variant->sync_answers(json_decode($request['answers']));
        return redirect()->route('JudgmentCode.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $variant->name]));
    }

    public function destroy(Request $request, $id)
    {
        $variant = Variant::findOrFail($id);
        $variant->delete();
        return redirect(route('JudgmentCode.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $variant->name]));
    }

}
