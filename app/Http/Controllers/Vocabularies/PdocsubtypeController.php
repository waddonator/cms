<?php

namespace App\Http\Controllers\Vocabularies;

use App\Pdocsubtype;
use App\Http\Requests\StorePdocsubtypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PdocsubtypeController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-pdocsubtype');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-pub'),"url" => route('publications.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('pdocsubtypes.index')]);
    }

    public function index(Request $request)
    {
        $models = Pdocsubtype::orderby('id')->paginate(20);
        return view('Vocabularies.Pdocsubtype.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Vocabularies.Pdocsubtype.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StorePdocsubtypeRequest $request)
    {
        $model = new Pdocsubtype;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 5141, $model->id);
        return redirect()->route('pdocsubtypes.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Pdocsubtype::findOrFail($id);
        return view('Vocabularies.Pdocsubtype.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StorePdocsubtypeRequest $request, $id)
    {
        $model = Pdocsubtype::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 5142, $model->id);
        return redirect()->route('pdocsubtypes.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Pdocsubtype::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 5143, $model->id);
        return redirect(route('pdocsubtypes.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
