<?php

namespace App\Http\Controllers\Vocabularies;

use App\Publisher;
use App\Http\Requests\StorePublisherRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PublisherController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-publisher');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('publishers.index')]);
    }


    public function index(Request $request)
    {
        $where = [];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Publisher::where($where)->orderby('id')->paginate(20);
        return view('Vocabularies.Publisher.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }


    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Vocabularies.Publisher.create', ['data'=>$this->data]);
    }


    public function store(StorePublisherRequest $request)
    {
        $model = new Publisher;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1101, $model->id);
        return redirect()->route('publishers.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }


    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Publisher::findOrFail($id);
        return view('Vocabularies.Publisher.edit', [
                'model'=>$model,
                'data'=>$this->data,
            ]);
    }


    public function update(StorePublisherRequest $request, $id)
    {
        $model = Publisher::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1102, $model->id);
        return redirect()->route('publishers.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }


    public function destroy(Request $request, $id)
    {
        $model = Publisher::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 1103, $model->id);
        return redirect(route('publishers.index'))
            ->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
