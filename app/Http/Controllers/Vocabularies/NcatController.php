<?php

namespace App\Http\Controllers\Vocabularies;

use App\Ncat;
use App\Http\Requests\StoreNcatRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NcatController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-ncat');
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.doc-news'),"url" => route('news.index')]);
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('ncats.index')]);
    }


    public function index(Request $request)
    {
        $models = Ncat::orderby('id')->paginate(20);
        return view('Vocabularies.Ncat.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Vocabularies.Ncat.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreNcatRequest $request)
    {
        $model = new Ncat;
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 2101, $model->id);
        return redirect()->route('ncats.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Ncat::findOrFail($id);
        return view('Vocabularies.Ncat.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreNcatRequest $request, $id)
    {
        $model = Ncat::findOrFail($id);
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 2102, $model->id);
        return redirect()->route('ncats.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Ncat::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 2103, $model->id);
        return redirect(route('ncats.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
