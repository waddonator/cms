<?php

namespace App\Http\Controllers\Vocabularies;

use App\Asubact;
use App\Http\Requests\StoreAsubactRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AsubactController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-asubact');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('asubacts.index')]);
    }


    public function index(Request $request)
    {
        $models = Asubact::orderby('id')->paginate(20);
        return view('Vocabularies.Asubact.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Vocabularies.Asubact.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreAsubactRequest $request)
    {
        $model = new Asubact;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1131, $model->id);
        return redirect()->route('asubacts.index')
            ->with('flash_message', 'Подтип актов ' . $model->name . ' создан');
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Asubact::findOrFail($id);
        return view('Vocabularies.Asubact.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreAsubactRequest $request, $id)
    {
        $model = Asubact::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1132, $model->id);
        return redirect()->route('asubacts.index')
            ->with('flash_message', 'Подтип актов ' . $model->name . ' изменен');
    }

    public function destroy(Request $request, $id)
    {
        $model = Asubact::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 1133, $model->id);
        return redirect(route('asubacts.index'))->with('error_message', 'Подтип актов ' . $model->name . ' удален');
    }

}
