<?php

namespace App\Http\Controllers\Vocabularies;

use App\Region;
use App\Http\Requests\StoreRegionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $models = Region::orderby('id')->paginate(20);
        return view('Vocabularies.Region.index', [
                'title' => 'Vocabularies',
                'models' => $models,
            ]);
    }

    public function create()
    {
        $title = trans('main.create-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        return view('Vocabularies.Region.create', ['title'=>$title, 'breadcrumbs' => $this->breadcrumbs]);
    }

    public function store(StoreRegionRequest $request)
    {
        $model = new Region;
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('regions.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function edit($id)
    {
        $title = trans('main.edit-variant');
        array_push($this->breadcrumbs , ["title" => $title,"url" => "#"]);
        $model = Region::findOrFail($id);
        return view('Vocabularies.Region.edit', [
                'title'=>$title,
                'model'=>$model,
                'breadcrumbs' => $this->breadcrumbs
            ]);
    }

    public function update(StoreRegionRequest $request, $id)
    {
        $model = Region::findOrFail($id);
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('regions.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Region::findOrFail($id);
        $model->delete();
        return redirect(route('regions.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.variant'), 'name' => $model->name]));
    }

}
