<?php

namespace App\Http\Controllers\Vocabularies;

use App\Source;
use App\Http\Requests\StoreSourceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SourceController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-source');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('sources.index')]);
    }

    public function index(Request $request)
    {
        $models = Source::orderby('id')->paginate(20);
        return view('Vocabularies.Source.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Vocabularies.Source.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreSourceRequest $request)
    {
        $model = new Source;
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1121, $model->id);
        return redirect()->route('sources.index')
            ->with('flash_message', 'Источник публикации актов ' . $model->name . ' создан');
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Source::findOrFail($id);
        return view('Vocabularies.Source.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreSourceRequest $request, $id)
    {
        $model = Source::findOrFail($id);
        $model->name = $request->name;
        $model->save();
        Auth::user()->log($request->ip(), 1122, $model->id);
        return redirect()->route('sources.index')
            ->with('flash_message', 'Источник публикации актов ' . $model->name . ' изменен');
    }

    public function destroy(Request $request, $id)
    {
        $model = Source::findOrFail($id);
        $model->delete();
        Auth::user()->log($request->ip(), 1123, $model->id);
        return redirect(route('sources.index'))->with('error_message', 'Источник публикации актов ' . $model->name . ' удален');
    }

}
