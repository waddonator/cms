<?php

/***********************************************
 *
 * Инструменты
 *
 ***********************************************/

namespace App\Http\Controllers;

use App\Ncat;
use App\News;
use App\Commentnro;
use App\DocumentsRelation;

use App\Ract;
use App\Ractversion;
use App\Raction;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ToolController extends Controller
{

    public function __construct() {
        parent::__construct();
    }

    public function index(Request $request)
    {
    }


    public function reincrement(Request $request)
    {
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('acts', 'id'), coalesce(max(id)+1,1), false) FROM acts;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('actversions', 'id'), coalesce(max(id)+1,1), false) FROM actversions;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('publishers', 'id'), coalesce(max(id)+1,1), false) FROM publishers;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('types', 'id'), coalesce(max(id)+1,1), false) FROM types;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('asubacts', 'id'), coalesce(max(id)+1,1), false) FROM asubacts;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('akeywords', 'id'), coalesce(max(id)+1,1), false) FROM akeywords;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('sources', 'id'), coalesce(max(id)+1,1), false) FROM sources;");

        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('news', 'id'), coalesce(max(id)+1,1), false) FROM news;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('ncats', 'id'), coalesce(max(id)+1,1), false) FROM ncats;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('ncat_news', 'id'), coalesce(max(id)+1,1), false) FROM ncat_news;");

        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('publications', 'id'), coalesce(max(id)+1,1), false) FROM publications;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('pauthors', 'id'), coalesce(max(id)+1,1), false) FROM pauthors;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('pauthorroles', 'id'), coalesce(max(id)+1,1), false) FROM pauthorroles;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('pglosstypes', 'id'), coalesce(max(id)+1,1), false) FROM pglosstypes;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('psources', 'id'), coalesce(max(id)+1,1), false) FROM psources;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('pdocsubtypes', 'id'), coalesce(max(id)+1,1), false) FROM pdocsubtypes;");

        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('comments', 'id'), coalesce(max(id)+1,1), false) FROM comments;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('cauthor_comment', 'id'), coalesce(max(id)+1,1), false) FROM cauthor_comment;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('commentlocals', 'id'), coalesce(max(id)+1,1), false) FROM commentlocals;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('cauthors', 'id'), coalesce(max(id)+1,1), false) FROM cauthors;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('cauthorroles', 'id'), coalesce(max(id)+1,1), false) FROM cauthorroles;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('csources', 'id'), coalesce(max(id)+1,1), false) FROM csources;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('cdocsubtypes', 'id'), coalesce(max(id)+1,1), false) FROM cdocsubtypes;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('commentnros', 'id'), coalesce(max(id)+1,1), false) FROM commentnros;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('actversion_commentlocal', 'id'), coalesce(max(id)+1,1), false) FROM actversion_commentlocal;");

        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('exportvocs', 'id'), coalesce(max(id)+1,1), false) FROM exportvocs;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('exportvocmodels', 'id'), coalesce(max(id)+1,1), false) FROM exportvocmodels;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('blocks', 'id'), coalesce(max(id)+1,1), false) FROM blocks;");

        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('logs', 'id'), coalesce(max(id)+1,1), false) FROM logs;");

        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('racts', 'id'), coalesce(max(id)+1,1), false) FROM racts;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('ractversions', 'id'), coalesce(max(id)+1,1), false) FROM ractversions;");
        DB::connection('pgsql')->select("SELECT setval(pg_get_serial_sequence('repllinks', 'id'), coalesce(max(id)+1,1), false) FROM repllinks;");

        return redirect()->route('tools')
            ->with('flash_message', 'Все счетчики таблиц id установлены в положение +1 к максимальному значению');
    }


    /*
     *
     * Импорт записей с сайтом lexinform
     *
     */
    public function lexinform(Request $request)
    {
        /* Получаем список категорий */
            $categories = DB::connection('mysql')->table('li_terms')->join('li_term_taxonomy','li_terms.term_id','=','li_term_taxonomy.term_id')->where('taxonomy','category')->get();
            foreach ($categories as $key => $category) {
                $model = Ncat::find($category->term_id);
                if (!$model){
                    $model = new Ncat;
                    $model->id = $category->term_id;
                }
                $model->name = $category->name;
                $model->save();
            }
        /* Получаем список опубликованных постов  */
            $where = [];
            $where[] = ['post_type', '=', 'post'];
            $where[] = ['post_status', '=', 'publish'];
            $news = DB::connection('mysql')->table('li_posts')->where($where)->get();
            foreach ($news as $key => $row) {
                $model = News::find($row->ID);
                if (!$model){
                    $model = new News;
                    $model->id = $row->ID;
                    $model->name = $row->post_title ? str_replace('"', "'", $row->post_title) : 'б/н';
                    $model->date_effective = $row->post_date;
                    $model->date_event = $row->post_date;
                    $model->important = true;
                    $model->export = true;
                    $model->nro = $row->ID + 16777216 * 39;
                        $post_content = preg_replace('/<img(?:\\s[^<>]*)?>/i', '', preg_replace("/\[[^\]]*\]/", '', $row->post_content));
                        $post_content = str_replace('target=_blank','target="_blank"',$post_content);
                    $model->content = $post_content;
                    $model->save();
                /* Получаем список категорий поста и синхронизируем */
                    $for_sync = [];
                    $categories = DB::connection('mysql')
                        ->table('li_terms')
                        ->join('li_term_relationships','li_terms.term_id','=','li_term_relationships.term_taxonomy_id')
                        ->join('li_term_taxonomy','li_terms.term_id','=','li_term_taxonomy.term_id')
                        ->where('object_id',$row->ID)
                        ->where('taxonomy','category')
                        ->get();
                    foreach ($categories as $key => $category) {
                        $for_sync[] = $category->term_id;
                    }
                    $model->ncats()->sync($for_sync);
                }
            }

        return redirect()->route('tools')
            ->with('flash_message', 'Данные из таблиц Lexinform успешно импортированы');
    }


    /*
     *
     * Создание массива связей между актами и локальными комментариями
     *
     */
    public function relations(Request $request)
    {
        DocumentsRelation::act_commentary();
        return redirect()->route('tools')
            ->with('flash_message', 'Массив связей успешно создан');
    }


    /*
     *
     * Страница для тестов
     *
     */
    public function dev(Request $request)
    {
        $element = Ractversion::where('rbotstatus_id','=',5)->orderby('id')->first();
        return view('Tools.dev',[
                'element'=>$element,
            ]);
    }


    /*
     *
     * Страница для link
     *
     */
    public function link(Request $request)
    {
        return view('Tools.link');
    }


    /*
     *
     * Страница для RADA BOT
     *
     */
    public function radabot(Request $request)
    {
        $models = Raction::orderby('id')->get();
        return view('Tools.radabot',[
                'models' => $models,
            ]);
    }


    /*
     *
     * Страница для тестов TOC
     *
     */
    public function toc(Request $request)
    {
        $element = Ractversion::where('rbotstatus_id','=',5)->orderby('id')->first();
        return view('Tools.toc',[
                'element'=>$element,
            ]);
    }
}
