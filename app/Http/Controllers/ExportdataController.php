<?php

namespace App\Http\Controllers;

use App\Exportdata;
use App\Http\Requests\StoreExportdataRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportdataController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-database-export';
        $this->data['title'] = trans('cms.export-data');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
    }

    public function index(Request $request)
    {
        $models = Exportdata::orderby('id')->paginate(20);
        return view('Tools.Exportdata.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create(Request $request)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Tools.Exportdata.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreExportdataRequest $request)
    {
        $model = new Exportdata;
        $model->name = $request->name;
        $model->model = $request->model;
        $model->document_production_main_type = $request->document_production_main_type;
        $model->save();
        return redirect()->route('exportdatas.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Exportdata::findOrFail($id);
        return view('Tools.Exportdata.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreExportdataRequest $request, $id)
    {
        $model = Exportdata::findOrFail($id);
        $model->name = $request->name;
        $model->model = $request->model;
        $model->document_production_main_type = $request->document_production_main_type;
        $model->save();
        return redirect()->route('exportdatas.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Exportdata::findOrFail($id);
        $model->delete();
        return redirect(route('exportdatas.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
