<?php

namespace App\Http\Controllers;

use App\Logaction;
use App\Http\Requests\StoreLogactionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogactionController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-books';
        $this->data['title'] = trans('cms.voc-logactions');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('logactions.index')]);
    }


    public function index(Request $request)
    {
        $where = [];
            if ($request->sid)  { $where[]=['id','=', $request->sid]; }
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $models = Logaction::where($where)->orderby('id')->paginate(20);
        return view('Tools.Logaction.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }


    public function create()
    {
        $this->data['subtitle'] = trans('cms.title-create');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        return view('Tools.Logaction.create', ['data'=>$this->data]);
    }


    public function store(StoreLogactionRequest $request)
    {
        $model = new Logaction;
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        
        return redirect()->route('logactions.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        $this->data['subtitle'] = trans('cms.title-edit');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $model = Logaction::findOrFail($id);
        return view('Tools.Logaction.edit', [
                'model'=>$model,
                'data'=>$this->data,
            ]);
    }

    public function update(StoreLogactionRequest $request, $id)
    {
        $model = Logaction::findOrFail($id);
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('logactions.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Logaction::findOrFail($id);
        $model->delete();
        return redirect(route('logactions.index'))
            ->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
