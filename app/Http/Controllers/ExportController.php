<?php

namespace App\Http\Controllers;

use App\Publisher;
use App\Type;
use App\Source;
use App\Jur;
use App\Region;
use App\CourtCode;
use App\JudgmentCode;
use App\JusticeKind;
use App\Instance;
use App\Judge;
use App\Vocabulary;
use App\AdditionalData;
use App\Document;

use App\Monograph;
use App\Mfragment;
use App\Mauthor;

use App\Publication;
use App\Pauthorrole;
use App\Pauthor;
use App\Pglosstype;
use App\Pdocsubtype;
use App\Psource;

use App\Act;
use App\Exportdata;
use App\Exportvoc;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class ExportController extends Controller
{

    public function __construct() {
        parent::__construct();
        set_time_limit(0);
    }


    public function index(Request $request)
    {
        $messageType = 'flash_message';
        $model = Exportdata::find($request->id);

        // $area = [1=>"Последние", 2=>"Все"];
        // $model = Exportdata::find($request->id);
        // if ($request->area < 1){
        //     $messageType = 'error_message';
        //     $message = 'Не выбрана область экспорта для документов <strong>' . $model->name . '</strong>';
        // } else {
        //     /* Временная отметка о дате последнего экспорта */
        //     $last_update = $model->exported_at;
        //     // $model->exported_at = now();
        //     // $model->save();
        //     $message = 'Экспортированы ' . $area[$request->area] . ' (' .
        //     (method_exists(($model->model), 'exportPerseus') ? ($model->model)::exportPerseus($request->area, $last_update) : 'undefined') . ') '
        //     . '<strong>' . $model->name . '</strong>';
        // }

        if ( method_exists(($model->model), 'exportPerseus') ){
            $message = 'Метод найден. Экспортировано - ' . ($model->model)::exportPerseus();
            Auth::user()->log($request->ip(), 100 + $request->id);
        } else {
            $message = 'Метод отсутсвует';
        }

        return redirect()->route('exportdatas.index')
            ->with( $messageType, $message );
    }


    /*
     *
     * Экспорт справочников
     *
     */
    public function exportvoc(Request $request)
    {
            $data = [];
        /* Создаем справочник в виде массива из всех привязанных моделей */
            $model = Exportvoc::find($request->id);
            foreach ($model->exportvocmodels as $key => $exportmodel) {
                foreach (($exportmodel->model)::get() as $key => $row) {
                    $data[] = (object)[
                            "@type" => $model->type,
                            "id" => $row->id + 16777216 * $model->factor + $exportmodel->factor,
                            "name" => $row->name,
                        ];
                }
            }
        /* Сохраняем справочник в базу Perseus */
            $vocmodel = Vocabulary::find($model->vocabulary_production_type);
            if (!$vocmodel){
                $vocmodel = new Vocabulary;
                $vocmodel->vocabulary_production_type = $model->vocabulary_production_type;
            }
            $vocmodel->data = pg_escape_bytea(json_encode( $data , JSON_UNESCAPED_UNICODE));
            $vocmodel->save();
        /* Отправляем пользователя на страницу всех справочников */
            $messageType = 'flash_message';
            $message = 'Справочник <strong>' . $model->name . '</strong> успешно экспортирован. Сознано элементов - <strong>' . count($data) . '</strong>.';
            return redirect()->route('exportvocs.index')
                ->with( $messageType, $message );
    }




    // public function get_vocabularies()
    // {
    //     //$result = DB::connection('perseus')->select("SELECT * FROM vocabularies");
    //     // foreach ($result as $key => $vocabulary) {
    //     //     $vocabulary->data = json_decode(stream_get_contents($vocabulary->data));
    //     // }
    //     return $result;
    // }

    // public function get_term_id($attr=[])
    // {
    //     //$vocabulary = $attr['vocabulary'] ? $attr['vocabulary'] : false;
    //     $value = $attr['value'] ? trim($attr['value']) : false;
    //     $result = 0;
    //     if ($value) {
    //         $data = Judge::where('name','=', $value)->first();
    //         if ($data){
    //             $result = $data->id;
    //         }
    //     }
    //     return $result;
    // }

    public function jurs(Request $request)
    {
        $document = (object)[];
        $content = '';
        if ($request->isMethod('post')){
            $method = 'POST';

            $elements = Jur::select('id')->get();
            foreach ($elements as $key => $row) {
                $element = Jur::find($row->id);
                $model = Document::find($element->pid + 16777216 * 31);
                if (!$model) {
                    $model = new Document;
                    $model->nro = $element->pid + 16777216 * 31;
                    $model->version_id = 1;
                    $model->fragment_id = 1;
                    $model->document_production_main_type = 'JURISPRUDENCE';
                }



                    $document->{"@type"} = 'jur';
                    $document->id = (object)["nro"=>($element->pid + 16777216 * 31), "versionId"=>1, "fragmentId"=>1];
                    $document->title = JusticeKind::find($element->justice_kind_id)->name . ', № '.$element->cause_num . ', ' . substr( $element->adjudication_date, 0, 10 ) . ', ' . CourtCode::find($element->court_code_id)->name . ' (' . JudgmentCode::find($element->judgment_code_id)->name . ')';
                    $document->language = 'UK';
                    $document->documentType = 'COURT_JURISPRUDENCE';
                    $document->noveltyDate = substr( $element->adjudication_date, 0, 10 );
                    $document->documentTypesForBoost = [];
                    $document->issueDate = substr( $element->adjudication_date, 0, 10 );
                    $document->documentSubTypeId = $element->justice_kind_id + 16777216 * 16 + 100000;
                    $document->caseNumbers = [(object)[
                                            "string"=> $element->cause_num ? $element->cause_num : 'б/н',
                                            "romanNumeral"=>"",
                                            "mark"=>"",
                                            "number"=>(int)$element->cause_num,
                                            "year"=>substr($element->adjudication_date, 0,4),
                                            "main"=>true,
                                            "procedureId"=>""
                                        ]];
                    $document->author = (object)[
                            "authorId" => $element->court_code_id + 16777216 * 13 + 100000
                        ];
                    if ($element->judge !='') {
                        $document->author->courtCompositionId = $this->get_term_id(["value"=>$element->judge]) + 16777216 * 17;
                    }
                    $document->author->courtClassId = CourtCode::find($element->court_code_id)->instance->id + 16777216 * 14;

                    $document->author->seatId = CourtCode::find($element->court_code_id)->region->id + 16777216 * 28;

                    $content = '<?xml version="1.0" encoding="utf-8"?><html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemalocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd" xmlns="http://www.w3.org/1999/xhtml"><body><div class="a_body">' . $element->text . '</div></body></html>';

                    $model->metadata = pg_escape_bytea(json_encode($document, JSON_UNESCAPED_UNICODE));
                    $model->content = pg_escape_bytea($content);
                    $model->save();
            }

        } else {
            $method = 'GET';
            // проставили номера для экспорта
            // $elements = Jur::select('id', 'pid')->get();
            // $i=1;
            // foreach ($elements as $key => $element) {
            //     $element->pid = $i;
            //     $element->save();
            //     $i++;
            // }
        }        

        $doc1 = Jur::count();
        $doc2 = Document::where('document_production_main_type','=','JURISPRUDENCE')->count();
        return view('Export.Jurs.export',[
                'doc1' => $doc1,
                'doc2' => $doc2,
                'method' => $method,
                'document' => $document,
                'content' => $content,
            ]);
    }



    public function vocabularies(Request $request)
    {
        if ($request->isMethod('post')){
            $method = 'POST';
                if($request->input('voc')=='source_publication'){
                    Vocabulary::create_source_publication();
                }
                if($request->input('voc')=='seat'){
                    Vocabulary::create_seat();
                }
                if($request->input('voc')=='author'){
                    Vocabulary::create_author();
                }
                if($request->input('voc')=='author_class'){
                    Vocabulary::create_author_class();
                }
                if($request->input('voc')=='author_role'){
                    Vocabulary::create_author_role();
                }
                if($request->input('voc')=='document_subtype'){
                    Vocabulary::create_document_subtype();
                }
                if($request->input('voc')=='court_composition'){
                    Vocabulary::create_court_composition();
                }
                if($request->input('voc')=='gloss_type'){
                    Vocabulary::create_gloss_type();
                }
                if($request->input('voc')=='newsletter_type'){
                    Vocabulary::create_newsletter_type();
                }
                // if($request->input('voc')=='blocks'){
                //     AdditionalData::create_blocks();
                // }
        } else {
            $method = 'GET';
        }

        $object = (object)[
                'source_publication'=>(object)[],
                'regions'=>(object)[],
                'author'=>(object)[],
                'author_class'=>(object)[],
                'author_role'=>(object)[],
                'document_subtype'=>(object)[],
                'court_composition'=>(object)[],
                'gloss_type'=>(object)[],
                'newsletter_type'=>(object)[],
            ];

                $object->source_publication->name = 'Источники (1)';
                $object->source_publication->count = Source::count() + Psource::count();
                $object->source_publication->pname = 'source_publication';
                $voc = Vocabulary::find('source_publication');
                if ($voc){
                    $object->source_publication->pexist = 'True';
                    $object->source_publication->pcount = count( json_decode(stream_get_contents($voc->data)) );
                } else {
                    $object->source_publication->pexist = 'False';
                    $object->source_publication->pcount = '--';
                }

        $object->regions->name = 'Регионы (28)';
        $object->regions->count = Region::count();
        $object->regions->pname = 'seat';
        $voc = Vocabulary::where('vocabulary_production_type','=','seat')->first();
        if ($voc){
            $object->regions->pexist = 'True';
            $object->regions->pcount = count( json_decode(stream_get_contents($voc->data)) );
        } else {
            $object->regions->pexist = 'False';
            $object->regions->pcount = '--';
        }


        $object->author->name = 'Авторы (13)';
        $object->author->count = Publisher::count() + CourtCode::count() + Mauthor::count() + Pauthor::count();
        $object->author->pname = 'author';
        $voc = Vocabulary::where('vocabulary_production_type','=','author')->first();
        if ($voc){
            $object->author->pexist = 'True';
            $object->author->pcount = count( json_decode(stream_get_contents($voc->data)) );
        } else {
            $object->author->pexist = 'False';
            $object->author->pcount = '--';
        }


        $object->author_class->name = 'Классы авторов (14)';
        $object->author_class->count = Instance::count();
        $object->author_class->pname = 'author_class';
        $voc = Vocabulary::find('author_class');
        if ($voc){
            $object->author_class->pexist = 'True';
            $object->author_class->pcount = count( json_decode(stream_get_contents($voc->data)) );
        } else {
            $object->author_class->pexist = 'False';
            $object->author_class->pcount = '--';
        }

                $object->author_role->name = 'Роли авторов (11)';
                $object->author_role->count = Pauthorrole::count();
                $object->author_role->pname = 'author_role';
                $voc = Vocabulary::find('author_role');
                if ($voc){
                    $object->author_role->pexist = 'True';
                    $object->author_role->pcount = count( json_decode(stream_get_contents($voc->data)) );
                } else {
                    $object->author_role->pexist = 'False';
                    $object->author_role->pcount = '--';
                }


        $object->document_subtype->name = 'Подтипы документов(16)';
        $object->document_subtype->count = Type::count() + JusticeKind::count() + Pdocsubtype::count();
        $object->document_subtype->pname = 'document_subtype';
        $voc = Vocabulary::find('document_subtype');
        if ($voc){
            $object->document_subtype->pexist = 'True';
            $object->document_subtype->pcount = count( json_decode(stream_get_contents($voc->data)) );
        } else {
            $object->document_subtype->pexist = 'False';
            $object->document_subtype->pcount = '--';
        }


        $object->court_composition->name = 'Состав судейской коллегии(18)';
        //$object->court_composition->count = Instance::count();
        $object->court_composition->count = 0;
        $object->court_composition->pname = 'court_composition';
        $voc = Vocabulary::find('court_composition');
        if ($voc){
            $object->court_composition->pexist = 'True';
            $object->court_composition->pcount = count( json_decode(stream_get_contents($voc->data)) );
        } else {
            $object->court_composition->pexist = 'False';
            $object->court_composition->pcount = '--';
        }

                $object->gloss_type->name = 'Gloss type (12)';
                $object->gloss_type->count = Pglosstype::count();
                $object->gloss_type->pname = 'gloss_type';
                $voc = Vocabulary::find('gloss_type');
                if ($voc){
                    $object->gloss_type->pexist = 'True';
                    $object->gloss_type->pcount = count( json_decode(stream_get_contents($voc->data)) );
                } else {
                    $object->gloss_type->pexist = 'False';
                    $object->gloss_type->pcount = '--';
                }

        return view('Export.Vocabularies.export',[
                'method'=>$method,
                'object'=>$object,
            ]);
    }


    // public function monographs(Request $request)
    // {
    //     $document = (object)[];
    //     $content = '';        
    //     if ($request->isMethod('post')){
    //         $method = 'POST';

    //         $elements = Monograph::select('id')->orderby('id')->get();
    //         foreach ($elements as $key => $row) {
    //             $element = Monograph::find($row->id);
    //             foreach ($element->mfragments->where('ordinal','>',0) as $key2 => $mfragment) {
    //                 $where = [];
    //                 $where[] = ['nro','=', $element->id + 16777216 * 22 ];
    //                 $where[] = ['fragment_id','=', $mfragment->ordinal];
    //                 $model = Document::where($where)->first();
    //                 if (!$model) {
    //                     $model = new Document;
    //                     $model->nro = $element->id + 16777216 * 22;
    //                     $model->version_id = 1;
    //                     $model->fragment_id = $mfragment->ordinal;
    //                     $model->document_production_main_type = 'MONOGRAPH';
    //                 }
    //                 $document->{"@type"} = 'mon';
    //                 $document->id = (object)["nro"=>($element->id + 16777216 * 22), "versionId"=>1, "fragmentId"=>$mfragment->ordinal];
    //                 $document->title = $element->name;
    //                 $document->language = 'UK';
    //                 $document->documentType = 'MONOGRAPH';
    //                 $document->noveltyDate = substr( $element->novelty_date, 0, 10 );

    //                 $document->subtitle = $element->name;
    //                 $document->authors = [(object)['authorId'=>$element->mauthor->id + 16777216 * 13 + 200000]];
    //                 $document->fragments = $element->create_contents();

    //                 $content = $mfragment->create_content();

    //                 $model->metadata = pg_escape_bytea(json_encode($document, JSON_UNESCAPED_UNICODE));
    //                 $model->content = pg_escape_bytea($content);
    //                 $model->save();
    //             }
    //         }

    //     } else {
    //         $method = 'GET';
    //     }        

    //     $doc1 = Monograph::count() . '(' . Mfragment::where('mnodetype_id','=',2)->count() . ')';
    //     $doc2 = Document::where('document_production_main_type','=','MONOGRAPH')->count();
    //     return view('Export.Monographs.export',[
    //             'doc1' => $doc1,
    //             'doc2' => $doc2,
    //             'method' => $method,
    //             'document' => $document,
    //             'content' => $content,
    //         ]);
    // }


    // public function publications(Request $request)
    // {
    //     if ($request->isMethod('post')){
    //         $method = 'POST';
    //         $elements = Publication::select('id')->get();
    //         foreach ($elements as $key => $row) {
    //             $element = Publication::find($row->id);
    //             $model = Document::find($element->id + 16777216 * 9);
    //             if (!$model) {
    //                 $model = new Document;
    //                 $model->nro = $element->id + 16777216 * 9;
    //                 $model->version_id = 1;
    //                 $model->fragment_id = 1;
    //                 $model->document_production_main_type = 'PUBLICATION';
    //             }
    //             $model->metadata = pg_escape_bytea(json_encode( $element->get_metadata() , JSON_UNESCAPED_UNICODE));
    //             $model->content = pg_escape_bytea( $element->get_content() );
    //             $model->save();
    //         }
    //     } else {
    //         $method = 'GET';
    //     }        

    //     $doc1 = Publication::count();
    //     $doc2 = Document::where('document_production_main_type','=','PUBLICATION')->count();
    //     return view('Export.Publications.export',[
    //             'doc1' => $doc1,
    //             'doc2' => $doc2,
    //             'method' => $method,
    //         ]);
    // }


}
