<?php

namespace App\Http\Controllers;

use App\Exportvoc;
use App\Exportvocmodel;
use App\Http\Requests\StoreExportvocRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportvocController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-database-export';
        $this->data['title'] = trans('cms.export-voc');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('exportvocs.index')]);
    }

    public function index(Request $request)
    {
        $models = Exportvoc::orderby('id')->paginate(20);
        return view('Tools.Exportvoc.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create(Request $request)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Tools.Exportvoc.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreExportvocRequest $request)
    {
        $model = new Exportvoc;
        $model->name = $request->name;
        $model->vocabulary_production_type = $request->vocabulary_production_type;
        $model->factor = $request->factor;
        $model->type = $request->type;
        $model->save();
        return redirect()->route('exportvocs.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Exportvoc::findOrFail($id);
        return view('Tools.Exportvoc.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreExportvocRequest $request, $id)
    {
        $model = Exportvoc::findOrFail($id);
        $model->name = $request->name;
        $model->vocabulary_production_type = $request->vocabulary_production_type;
        $model->factor = $request->factor;
        $model->type = $request->type;
        $model->save();
        return redirect()->route('exportvocs.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Exportvoc::findOrFail($id);
        $model->delete();
        return redirect(route('exportvocs.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
