<?php

namespace App\Http\Controllers;

use App\Exportvoc;
use App\Exportvocmodel;
use App\Http\Requests\StoreExportvocmodelRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportvocmodelController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-database-export';
        $this->data['title'] = trans('cms.export-vocmodel');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('exportvocmodels.index')]);
    }

    public function index(Request $request)
    {
        $models = Exportvocmodel::orderby('id')->paginate(20);
        return view('Tools.Exportvocmodel.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create(Request $request)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        $exportvocs = Exportvoc::pluck('name','id')->prepend('--','0');
        return view('Tools.Exportvocmodel.create', [
                'exportvocs' => $exportvocs,
                'data' => $this->data,
            ]);
    }

    public function store(StoreExportvocmodelRequest $request)
    {
        $model = new Exportvocmodel;
        $model->name = $request->name;
        $model->model = $request->model;
        $model->exportvoc_id = $request->exportvoc_id;
        $model->factor = $request->factor;
        $model->save();
        return redirect()->route('exportvocmodels.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $exportvocs = Exportvoc::pluck('name','id')->prepend('--','0');
        $model = Exportvocmodel::findOrFail($id);
        return view('Tools.Exportvocmodel.edit', [
                'model'=>$model,
                'exportvocs' => $exportvocs,
                'data' => $this->data,
            ]);
    }

    public function update(StoreExportvocmodelRequest $request, $id)
    {
        $model = Exportvocmodel::findOrFail($id);
        $model->name = $request->name;
        $model->model = $request->model;
        $model->exportvoc_id = $request->exportvoc_id;
        $model->factor = $request->factor;
        $model->save();
        return redirect()->route('exportvocmodels.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Exportvocmodel::findOrFail($id);
        $model->delete();
        return redirect(route('exportvocmodels.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
