<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;

use App\User;
use Auth;
use App\Http\Controllers\Controller;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
// use App\Respondent;
// use App\Respondent_user;
// use App\School;

class UserController extends Controller {


    public function __construct() {
        parent::__construct();
        $this->middleware(['auth']); //isAdmin middleware lets only users with a specific permission permission to access these resources
        array_push($this->data['breadcrumbs'] , ["title" => 'Пользователи',"url" => route('users.index')]);
        $this->data['icon'] = 'icon-users';
        $this->data['title'] = 'Пользователи';
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {        
        $roles = Role::pluck('name','id')->prepend('',0);
        $where = [];
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
            if ($request->semail) { $where[]=['email','like', '%'.$request->semail.'%']; }
            if ($request->srole)  { $where[]=['role_id','=', $request->srole]; }
        if ($request->srole){
            $users = User::distinct()->select('users.*')->join('model_has_roles','users.id','=','model_has_roles.model_id')->where($where)->paginate(20);
        } else {
            $users = User::where($where)->paginate(20);
        }
        return view('users.users.index', [
                'users'=>$users,
                'roles'=>$roles,
                'pagination_add'=>$request->input(),
                'data' => $this->data,
            ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    //Get all roles and pass it to the view
        $this->data['subtitle'] = 'Создание';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $roles = Role::get();
        return view('users.users.create', [
                'roles'=>$roles,
                'data' => $this->data,
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreUserRequest $request) {
        $user = new User;
        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->password = $request->input('password');
        $user->save();

        $roles = $request['roles']; //Retrieving the roles field checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
            $role_r = Role::where('id', '=', $role)->firstOrFail();            
            $user->assignRole($role_r); //Assigning role to user
            }
        }        
    //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.user'), 'name' => $user->name]));
    }



    public function show(Request $request, $id)
    {
        $newuser = User::find($id);
        if ($newuser){
            Auth::user()->log($request->ip(), 2);
            Auth::login($newuser);
            Auth::user()->log($request->ip(), 1);
        }
        return redirect()->route('home')
            ->with('flash_message', 'Вы авторизовались под пользователем ' . auth()->user()->name);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $this->data['subtitle'] = 'Изменение';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        return view('users.users.edit', [
                'user'=>$user,
                'roles'=>$roles,
                'data' => $this->data
            ]); //pass user and roles data to view

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields  
        $this->validate($request, [
            'email'=>'required|email|unique:users,email,' . $id,
            'name' => 'required|string|max:255',
        ]);
        
        $input = $request->only(['email', 'name']); //Retreive the name, email and password fields
        $roles = $request['roles']; //Retreive all roles
        $user->fill($input)->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.user'), 'name' => $user->name]));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id) {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect(route('users.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.user'), 'name' => $user->name]));
    }

}