<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Controllers\Controller;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class PermissionController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->middleware(['auth']); //isAdmin middleware lets only users with a //specific permission permission to access these resources
        array_push($this->data['breadcrumbs'] , ["title" => 'Доступы',"url" => route('permissions.index')]);
        $this->data['icon'] = 'icon-user-lock';
        $this->data['title'] = 'Доступы';
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        $where = [];
            if ($request->sname)  { $where[]=['name','like', '%'.$request->sname.'%']; }
        $permissions = Permission::where($where)->paginate(20); //Get all permissions

        return view('users.permissions.index', [
                'permissions'=>$permissions,
                'pagination_add'=>$request->input(),
                'data' => $this->data,
            ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        $this->data['subtitle'] = 'Создание';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $roles = Role::get(); //Get all roles
        return view('users.permissions.create', [
            'roles'=>$roles,
            'data' => $this->data,
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();



        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('permissions.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.permission'), 'name' => $permission->name]));

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        return redirect('permissions');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $this->data['subtitle'] = 'Изменение';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['subtitle'],"url" => "#"]);
        $permission = Permission::findOrFail($id);
        return view('users.permissions.edit', [
                'permission'=>$permission,
                'data' => $this->data,
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('permissions.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.permission'), 'name' => $permission->name]));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id) {

        $permission = Permission::findOrFail($id);
        //Make it impossible to delete this specific permission 
        if ($permission->name == "admin") {
            return redirect()->route('permissions.index')
            ->with('flash_message', trans('messages.delete-admin'));
        }
        $permission->delete();
        return redirect(route('permissions.index'))->with('error_message', trans('messages.deleted', ['item'=>trans('main.permission'), 'name' => $permission->name]));
 
    }
}