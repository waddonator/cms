<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $breadcrumbs = [];
    public $pagination;
    public $data = ['breadcrumbs'=>[]];

    public function __construct() {
        array_push($this->data['breadcrumbs'] , ["title" => '<i class="icon-home2 mr-2"></i> Главная панель',"url" => route('home')]);
        $this->data['icon'] = 'icon-home';
        $this->data['title'] = 'Главная панель';
    }
    
}
