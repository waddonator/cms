<?php

namespace App\Http\Controllers;

use App\Rbotstatus;
use App\Http\Requests\StoreRbotstatusRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RbotstatusController extends Controller
{
    public function __construct() {
        parent::__construct();
        // $this->data['icon'] = 'icon-loop';
        // $this->data['title'] = 'Запросы радабота';
        // array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
    }

    public function index(Request $request)
    {
        //$models = Rbotstatus::orderby('id')->paginate(20);
        return view('Tools.Rbotstatus.index', [
                // 'models' => $models,
                // 'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create(Request $request)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Tools.Rbotstatus.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreRbotstatusRequest $request)
    {
        $model = new Rbotstatus;
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('rbotstatuses.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Rbotstatus::findOrFail($id);
        return view('Tools.Rbotstatus.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreRbotstatusRequest $request, $id)
    {
        $model = Rbotstatus::findOrFail($id);
        $model->id = $request->id;
        $model->name = $request->name;
        $model->save();
        return redirect()->route('rbotstatuses.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Rbotstatus::findOrFail($id);
        $model->delete();
        return redirect(route('rbotstatuses.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'actions'
        ];
        $totalData = Rbotstatus::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $models = Rbotstatus::offset($start)
                ->orderby($order,$dir)
                ->limit($limit)
                ->get();
                $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $models = Rbotstatus::where('id','=',(int)$search)
                ->orWhere('name','like',"%{$search}%")
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit)
                ->get();
            $totalFiltered = Rbotstatus::where('id','=',(int)$search)
                ->orWhere('name','like',"%{$search}%")
                ->count();
        }

        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = $model->name;
                $nestedData['actions'] = '
                <div class="list-icons">
                    <a href="'.route('rbotstatuses.edit',$model->id).'" class="list-icons-item" title="' . trans('cms.btn-edit') .'"><i class="icon-pencil7"></i></a>
                    <a href="'.route('rbotstatuses.destroy',$model->id).'" class="list-icons-item text-danger-600 btn-trash" title="' . trans('cms.btn-remove') .'" data-toggle="modal" data-target="#modal_trash"><i class="icon-trash"></i></a>
                </div>
                ';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
        
    }

}
