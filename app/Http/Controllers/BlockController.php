<?php

namespace App\Http\Controllers;

use App\Block;
use App\Http\Requests\StoreBlockRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlockController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-database-export';
        $this->data['title'] = trans('cms.blocks');
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('blocks.index')]);
    }

    public function index(Request $request)
    {
        $models = Block::orderby('id')->paginate(20);
        return view('Tools.Block.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create()
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Tools.Block.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreBlockRequest $request)
    {
        $model = new Block;
        $model->guid = $request->guid;
        $model->title = $request->title;
        $model->save();
        
        return redirect()->route('blocks.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Block::findOrFail($id);
        return view('Tools.Block.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreBlockRequest $request, $id)
    {
        $model = Block::findOrFail($id);
        $model->guid = $request->guid;
        $model->title = $request->title;
        $model->save();
        return redirect()->route('blocks.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Block::findOrFail($id);
        $model->delete();
        return redirect(route('blocks.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));;
    }

}
