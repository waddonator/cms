<?php

namespace App\Http\Controllers;

use App\Raction;
use App\Http\Requests\StoreRactionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RactionController extends Controller
{
    public function __construct() {
        parent::__construct();
        $this->data['icon'] = 'icon-loop';
        $this->data['title'] = 'Запросы радабота';
        array_push($this->data['breadcrumbs'] , ["title" => $this->data['title'],"url" => route('acts.index')]);
    }

    public function index(Request $request)
    {
        $models = Raction::orderby('id')->paginate(20);
        return view('Tools.Raction.index', [
                'models' => $models,
                'pagination_add' => $request->input(),
                'data' => $this->data,
            ]);
    }

    public function create(Request $request)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-create') ,"url" => "#"]);
        return view('Tools.Raction.create', [
                'data' => $this->data,
            ]);
    }

    public function store(StoreRactionRequest $request)
    {
        $model = new Raction;
        $model->name = $request->name;
        $model->url = $request->url;
        $model->sec = $request->sec;
        $model->response_header = (object)[];
        $model->save();
        return redirect()->route('ractions.index')
            ->with('flash_message', trans('cms.alert-create', ['name' => $model->name]));
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        array_push($this->data['breadcrumbs'] , ["title" => trans('cms.title-edit') ,"url" => "#"]);
        $model = Raction::findOrFail($id);
        return view('Tools.Raction.edit', [
                'model'=>$model,
                'data' => $this->data,
            ]);
    }

    public function update(StoreRactionRequest $request, $id)
    {
        $model = Raction::findOrFail($id);
        $model->name = $request->name;
        $model->url = $request->url;
        $model->sec = $request->sec;
        $model->save();
        return redirect()->route('ractions.index')
            ->with('flash_message', trans('cms.alert-update', ['name' => $model->name]));
    }

    public function destroy(Request $request, $id)
    {
        $model = Raction::findOrFail($id);
        $model->delete();
        return redirect(route('ractions.index'))->with('error_message', trans('cms.alert-remove', ['name' => $model->name]));
    }

}
