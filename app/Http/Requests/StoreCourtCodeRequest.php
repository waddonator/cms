<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCourtCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('court_code') ? $this->route()->parameter('court_code') : 'NULL';
        return [
            'id' => 'required|unique:court_codes,id,'.$id,
            'name' => 'required|string|max:255',
            'region_id' => 'integer|min:1',
        ];
    }
}
