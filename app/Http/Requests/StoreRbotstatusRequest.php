<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRbotstatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('rbotstatus') ? $this->route()->parameter('rbotstatus') : 'NULL';
        return [
            'id' => 'required|unique:rbotstatuses,id,'.$id,
            'name' => 'required|string|max:255',
        ];
    }
}
