<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('region') ? $this->route()->parameter('region') : 'NULL';
        return [
            'id' => 'required|unique:regions,id,'.$id,
            'name' => 'required|string|max:255',
        ];
    }
}
