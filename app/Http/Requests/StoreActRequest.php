<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreActRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:1000',
            'atype_id' => 'integer|min:1',
            'asubact_id' => 'integer|min:1',
            'number' => 'required|string|max:255',
            'date_acc' => 'required',
            'date_force' => 'required',
            'reestr_code' => 'max:255',
            //'reestr_code' => 'required|string|max:255',
            //'reestr_date' => 'required',
            'finisher_id' => 'max:9',
        ];
    }
}
