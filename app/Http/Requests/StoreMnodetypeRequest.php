<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMnodetypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $id = $this->route()->parameter('judge') ? $this->route()->parameter('judge') : 'NULL';
        return [
            // 'id' => 'required|unique:instances,id,'.$id,
            'name' => 'required|string|max:255',
        ];
    }
}
