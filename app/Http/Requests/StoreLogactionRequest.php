<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLogactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route()->parameter('logaction') ? $this->route()->parameter('logaction') : 'NULL';
        return [
            'id'=>'required|unique:logactions,id,' . $id,
            'name' => 'required|string|max:255',
        ];
    }
}
