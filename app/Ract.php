<?php

namespace App;

use App\Ractversion;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ract extends Model
{
    protected $casts = [
        'rmeta' => 'object',
    ];

    public function ractversions()
    {
        return $this->hasMany('App\Ractversion','act_id');
    }

    public function rbotstatus()
    {
        return $this->belongsTo('App\Rbotstatus');
    }

    public function repllinks()
    {
        return $this->hasMany('App\Repllink','act_id');
    }


    # загрузка последней версии метаданных
    public function downloadLastMeta()
    {
        $opts = [
            'http' => [
                'method'=>"GET",
                'header'=>"Accept-language: en\r\n" .
                    "Cookie: OpenData=OpenData\r\n"
            ]
        ];
        $context = stream_context_create($opts);
        $url = 'http://data.rada.gov.ua/laws/card/' . $this->nreg . '.json';
        $html_request = file_get_contents($url, false, $context);
        $this->rmeta = $html_request;
        $this->rbotstatus_id = 2;
        $this->save();
        return true;    
    }



    # создание пустых версий редакций (если их нет) и запись в последнюю меты и контента
    public function sync_ractversions($json = '{}')
    {
        $temp = json_decode($json);
        if (isset($temp->eds) and is_array($temp->eds)) {
            $eds = $temp->eds;
            usort($eds, function($a, $b){
                return ($a->datred - $b->datred);
            });            
            foreach ($eds as $key => $ed) {
                # Если основная редакция, то качаем контент
                    if ($key == 0){
                        $opts = [
                            'http' => [
                                'method'=>"GET",
                                'header'=>"Accept-language: en\r\n" .
                                    "Cookie: OpenData=OpenData\r\n"
                            ]
                        ];
                        $context = stream_context_create($opts);
                    # получаем контент
                        $url_content = 'http://data.rada.gov.ua/laws/show/' . $this->nreg . '.json';
                        // $url_content = 'http://data.rada.gov.ua/laws/show/' . $this->nreg . '/ed' . $this->ed . '.json';
                        $html_content = file_get_contents($url_content, false, $context);
                                }
                # Если нет такой редакции, то создаем
                # Если есть, то проверяем дату. Если не совпадает, то ставим отметку для загрузки
                    if ($this->ractversions()->where('versionid','=', $key+1 )->count()==0){
                        $ractversion = new Ractversion;
                        $ractversion->act_id = $this->id;
                        $ractversion->ed = $ed->datred;
                        $ractversion->versionid = $key+1;
                        $ractversion->rbotstatus_id = ($ed->datred == $temp->datred) ? 2 : 0;
                        $ractversion->rmeta = ($ed->datred == $temp->datred) ? $this->rmeta : (object)[];
                        $ractversion->rcontent = ($ed->datred == $temp->datred) ? $html_content : (object)[];
                        $ractversion->content = '';
                        $ractversion->pmeta = (object)[];
                        $ractversion->pcontent = '';
                        $ractversion->save();
                    } else {
                        $ractversion = $this->ractversions()->where('versionid','=', $key+1 )->first();
                        if ($ed->datred != $ractversion->datred){
                            $ractversion->rbotstatus_id = 0;
                            $ractversion->save();
                        }
                    }
                #---
            }
            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }

    // public function sync_ractversions($json = '{}')
    // {
    //     $temp = json_decode($json);
    //     if (isset($temp->eds) and is_array($temp->eds)) {
    //         $eds = $temp->eds;
    //         foreach ($eds as $key => $ed) {
    //             # Если основная редакция, то качаем контент
    //                 if ($key == 0){
    //                     $opts = [
    //                         'http' => [
    //                             'method'=>"GET",
    //                             'header'=>"Accept-language: en\r\n" .
    //                                 "Cookie: OpenData=OpenData\r\n"
    //                         ]
    //                     ];
    //                     $context = stream_context_create($opts);
    //                 # получаем контент
    //                     $url_content = 'http://data.rada.gov.ua/laws/show/' . $this->nreg . '/ed' . $this->ed . '.json';
    //                     $html_content = file_get_contents($url_content, false, $context);
    //                             }
    //             ###
    //             $versionid = ($key == 0) ? count($eds) : $key;
    //             if ($this->ractversions()->where('versionid','=',$versionid)->count()==0){
    //                 $ractversion = new Ractversion;
    //                 $ractversion->act_id = $this->id;
    //                 $ractversion->ed = $ed->datred;
    //                 $ractversion->versionid = $versionid;
    //                 $ractversion->rbotstatus_id = ($key == 0) ? 5 : 0;
    //                 $ractversion->rmeta = ($key == 0) ? $this->rmeta : (object)[];
    //                 $ractversion->rcontent = ($key == 0) ? $html_content : (object)[];
    //                 $ractversion->content = '';
    //                 $ractversion->pmeta = (object)[];
    //                 $ractversion->pcontent = '';
    //                 $ractversion->save();
    //             }
    //         }
    //         $result = true;
    //     } else {
    //         $result = false;
    //     }

    //     return $result;
    // }

    public function typs()
    {
        $typ = json_decode('{"1":{"name":"Закон"},"2":{"name":"Постанова"},"3":{"name":"Указ"},"4":{"name":"Декларація"},"5":{"name":"Концепція"},"6":{"name":"Розпорядження"},"7":{"name":"Заява"},"8":{"name":"Декрет"},"9":{"name":"Наказ"},"10":{"name":"Інструкція"},"11":{"name":"Положення"},"12":{"name":"Роз\'яснення"},"13":{"name":"Правила"},"14":{"name":"Статут"},"15":{"name":"Лист"},"16":{"name":"Договір"},"17":{"name":"Угода"},"18":{"name":"Протокол"},"19":{"name":"Меморандум"},"20":{"name":"Конвенція"},"21":{"name":"Кодекс України"},"22":{"name":"Рішення"},"23":{"name":"Конституційний договір"},"24":{"name":"Тариф"},"25":{"name":"Порядок"},"26":{"name":"Вказівки"},"27":{"name":"Хартія"},"28":{"name":"Комюніке"},"29":{"name":"Пакт"},"30":{"name":"Ухвала"},"31":{"name":"Норми"},"32":{"name":"Регламент"},"33":{"name":"Довідник"},"34":{"name":"Методика"},"35":{"name":"Перелік"},"36":{"name":"Інші"},"37":{"name":"Поправки"},"38":{"name":"Модель"},"39":{"name":"Ставки"},"44":{"name":"Рекомендації"},"52":{"name":"Резолюція"},"61":{"name":"Коментар"},"64":{"name":"Міжнародний документ"},"68":{"name":"Нота"},"69":{"name":"Висновок"},"74":{"name":"Принципи"},"75":{"name":"План"},"76":{"name":"Директива"},"78":{"name":"Акт"},"79":{"name":"Форма"},"80":{"name":"Розклад"},"84":{"name":"Домовленість"},"86":{"name":"Програма"},"87":{"name":"Витяг"},"88":{"name":"Коефіцієнти"},"89":{"name":"Бланк звітності"},"95":{"name":"Повідомлення"},"96":{"name":"Проект"},"97":{"name":"Послання"},"99":{"name":"Форма типового документа"},"100":{"name":"Конституція"},"101":{"name":"Звернення"},"102":{"name":"Заходи"},"103":{"name":"Претензія"},"104":{"name":"Умови"},"105":{"name":"Свідоцтво"},"107":{"name":"Записка"},"108":{"name":"Замовлення"},"109":{"name":"Картка"},"110":{"name":"Дозвіл"},"112":{"name":"Прейскурант"},"114":{"name":"Покажчик"},"115":{"name":"Класифікатор"},"116":{"name":"Питання-відповідь"},"117":{"name":"Універсал"},"118":{"name":"Звід"},"119":{"name":"Доповідь"},"120":{"name":"Доручення"},"121":{"name":"Кошторис"},"123":{"name":"Вирок"},"124":{"name":"Кодекс"},"125":{"name":"Справа"},"129":{"name":"Опис"},"130":{"name":"Статус"},"135":{"name":"Припис"},"138":{"name":"Стандарт"},"141":{"name":"Доктрина"},"142":{"name":"Зразок"},"143":{"name":"Сертифікат"},"144":{"name":"Реєстр"},"145":{"name":"Вимоги"},"148":{"name":"Телеграма"},"150":{"name":"Типовий закон"},"151":{"name":"Список"},"152":{"name":"Стратегія"},"153":{"name":"Окрема думка"},"154":{"name":"Звіт"},"156":{"name":"Пропозиції"},"158":{"name":"Історичний документ"},"162":{"name":"Узагальнення судової практики"},"163":{"name":"Схема"},"164":{"name":"Паспорт бюджетної програми"},"168":{"name":"Склад колегіального органу"},"170":{"name":"Класифікація"},"171":{"name":"Нормативи"},"172":{"name":"Конституційне подання"},"175":{"name":"Інформація"},"177":{"name":"Структура"},"179":{"name":"Титул"},"180":{"name":"Бланк"},"181":{"name":"Паспорт"},"184":{"name":"Оголошення"},"186":{"name":"Довідка"},"191":{"name":"Додаток"},"193":{"name":"Графік прийому громадян"},"197":{"name":"Бюджет"},"201":{"name":"Порядок денний"},"203":{"name":"Зміни до документа"},"204":{"name":"Запит"},"205":{"name":"Критерії"},"206":{"name":"Журнал"},"207":{"name":"Застереження"},"208":{"name":"Заперечення"},"209":{"name":"Розподіл"},"212":{"name":"Розбіжна думка"},"213":{"name":"Технічний регламент"}}');
        $result = '';
        $array = explode('|',$this->typ);
        foreach ($array as $key => $element) {
            if (isset($typ->{$element})) {$result .= ', '. $typ->{$element}->name;}
        }
        $result = mb_substr($result, 2);
        return $result;
    }


    public function status()
    {
        $result = '';
        if ($this->rbotstatus_id==0) {$result = 'Необходима загрузка с ВР';}
        if ($this->rbotstatus_id==1) {$result = 'Метаданные а процессе загрузки';}
        if ($this->rbotstatus_id==2) {$result = 'Метаданные загружены. Требуется синхронизация редакций';}
        if ($this->rbotstatus_id==3) {$result = 'Идет синхронизация редакций';}
        if ($this->rbotstatus_id==9) {$result = 'Все работы проведены';}
        return $result;
    }


    public function rsubact()
    {
        $result = '';
        $element = Asubact::find($this->rsubact_id);
        if ($element) {$result = $element->name;}
        return $result;
    }


    public function versionsCount()
    {
        //$result = Ractversion::select('id')->where('act_id','=',$this->id)->count();
        //dump($this->rmeta);
        $result = (!is_object($this->rmeta)) ? json_decode($this->rmeta)->edcnt : 0;
        return $result;
    }


    # определение подтипа - COMMON или DEPARTAMENTAL
    public function chooseSubact()
    {
        $array = explode('|',$this->typ);
        $result = 2;
        foreach ($array as $key => $element) {
            if (in_array($element, ['1', '124', '21', '20', '172', '23', '100', '19', '2', '13', '52', '22', '138', '130', '14', '17', '3'])) {
                $result = 1;
            }
        }
        $this->rsubact_id = $result;
        $this->nro = $this->id + $result * 16777216;
        $this->save();
        return true;
    }
    /**
     * 
     * 
     * 
     */
    // public function get_metadata()
    // {
    //     $document = (object)[];
    //     return $document;
    // }


    // public function get_content()
    // {
    //     $content = '';
    //     return $content;
    // }

    // public function sync_sources($obj = '{}')
    // {
    //     $object = json_decode($obj);
    //     $sources = [];
    //     foreach ($object as $key => $value) {
    //         $sources[] = $key;
    //     }
    //     $this->sources()->sync($sources);
    //     foreach ($this->sources as $key => $value) {
    //         $value->pivot->announced_date = $object->{$value->pivot->source_id}->f1 ? $object->{$value->pivot->source_id}->f1 : null;
    //         $value->pivot->issue_from = (int)$object->{$value->pivot->source_id}->f2;
    //         $value->pivot->position = (int)$object->{$value->pivot->source_id}->f3;
    //         $value->pivot->save();
    //     }
    //     return true;
    // }

    // public function get_sources()
    // {
    //     $object = (object)[];
    //     foreach ($this->sources as $key => $value) {
    //         $object->{$value->pivot->source_id} = (object)[
    //                 'f1' => Carbon::parse($value->pivot->announced_date)->format('Y-m-d'),
    //                 'f2' => $value->pivot->issue_from,
    //                 'f3' => $value->pivot->position,
    //             ];
    //     }
    //     return json_encode($object);
    // }

    // public function sync_keywords($arr = '[]')
    // {
    //     $arr = json_decode($arr);
    //     $keywords = [];
    //     foreach ($arr as $key => $element) {
    //         $word = Akeyword::where('name', '=', $element)->first();
    //         if (!$word){
    //             $word = new Akeyword;
    //             $word->name = $element;
    //             $word->save();
    //         }
    //         $keywords[] = $word->id;
    //     }
    //     $this->akeywords()->sync($keywords);
    //     return true;
    // }

    // /***
    //  *
    //  * Функция экспорта всех законодательных актов в базу Perseus
    //  *
    //  ***/
    // //static public function exportPerseus($area=0, $exported_at=NULL)
    // static public function exportPerseus()
    // {
    //     set_time_limit(0);
    //     $result = 0;
    //     $result2 = 0;
    //     $elements = Act::select('id')
    //         ->where('export', '=', true)
    //         ->get();

    //     foreach ($elements as $row) {
    //         $element = Act::find($row->id);
    //         //Document::where('nro','=', $element->nro)->delete();
    //         foreach ($element->actversions->where('export', '=', true) as $key => $actversion) {
    //             $where = [['nro','=', $element->nro]];
    //             $where[] = ['version_id', '=', $actversion->versionid];
    //             Document::where($where)->delete();
    //             $model = new Document;
    //             $model->nro = $element->nro;
    //             $model->version_id = $actversion->versionid;
    //             $model->fragment_id = 1;
    //             $model->document_production_main_type = 'ACT';
    //             $model->metadata = pg_escape_bytea(json_encode( $actversion->get_metadata() , JSON_UNESCAPED_UNICODE));
    //             $model->content = pg_escape_bytea( $actversion->get_content() );
    //             $model->save();
    //             $actversion->export = false;
    //             $actversion->save();
    //             $result2++;
    //         }
    //         $element->export = false;
    //         $element->save();
    //         $result++;
    //     }

    //     return trans('cms.doc-act') . ': ' . $result . ', ' . trans('cms.doc-actver') . ': ' . $result2;
    // }


    // /***
    //  *
    //  * Отображение количества документов
    //  *
    //  ***/
    // static public function doccount()
    // {
    //     return Actversion::count();
    // }


    // /***
    //  *
    //  * Отображение количества документов нуждающихся в экспорте
    //  *
    //  ***/
    // static public function expcount()
    // {
    //     return Act::where('export','=',true)->count() . ' (' . Actversion::where('export','=',true)->count() . ')';
    // }


    // /***
    //  *
    //  * Сводные данные для статистики виджета
    //  *
    //  ***/
    // static public function stat()
    // {
    //     $result = (object)[];
    //     $result->all = Act::count();
    //     $result->trash = Act::where('trash','=',1)->count();
    //     $model = Exportdata::find(1);
    //     //$result->export = Act::where('updated_at','>',$model->exported_at)->count();
    //     $result->export = Act::where('export','=',true)->count();
    //     $result->lastnew = Act::where('trash','=',0)->orderby('created_at','desc')->first();
    //     $result->lastedit = Act::where('trash','=',0)->orderby('updated_at','desc')->first();
    //     $result->lasttrash = Act::where('trash','=',1)->orderby('updated_at','desc')->first();
    //     return $result;
    // }


    // /**
    //  * 
    //  * Получаем все ТОС-элементы из контента последней редакции акта
    //  * 
    //  */
    // public function get_tocunits()
    // {
    //     $result = [""=>""];
    //     $actversion = $this->actversions->where('trash','=',false)->sortByDesc('versionid')->first();
    //     preg_match_all("|id=\"([^\"]*)\"[^>]*class=\"[^\"]*toc[^>]*\"|U", $actversion->content, $output, PREG_PATTERN_ORDER);
    //     foreach ($output[1] as $key => $row) {
    //         $result[$row] = $row;
    //     }
    //     return $result;
    // }
}
