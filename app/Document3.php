<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document3 extends Model
{

    protected $connection= 'perseus';
    protected $primaryKey = ['nro', 'version_id', 'fragment_id'];
    //protected $primaryKey = 'nro';
    protected $table = 'documents';

    const CREATED_AT = 'last_modified_date';
    const UPDATED_AT = 'last_modified_date';

    // protected function setKeysForSaveQuery(Builder $query)
    // {
    //     $query
    //         ->where('nro', '=', $this->getAttribute('nro'))
    //         ->where('version_id', '=', $this->getAttribute('version_id'))
    //         ->where('fragment_id', '=', $this->getAttribute('fragment_id'));
    //     return $query;
    // }



}
