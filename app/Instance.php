<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instance extends Model
{
    protected $fillable = ['name'];
    public $timestamps = FALSE;

}
