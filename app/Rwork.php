<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Rwork extends Model
{

    public $timestamps = FALSE;

    public function ractversion()
    {
        return $this->hasOne('App\Ractversion','id','id');
    }

}
