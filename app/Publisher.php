<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    protected $fillable = ['name'];
    public $timestamps = FALSE;

    public function acts()
    {
        return $this->belongsToMany('App\Act');
    }

}
