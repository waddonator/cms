<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JusticeKind extends Model
{
    protected $fillable = ['name'];

}
