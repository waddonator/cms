<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JudgmentCode extends Model
{
    protected $fillable = ['name'];

}
