<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Commentnro extends Model
{
    public $timestamps = FALSE;

    public function comment()
    {
        return $this->belongsTo('App\Comment');
    }

    public static function get_nro($comment_id=0, $tocunit='')
    {
        $model = Commentnro::where('comment_id','=',$comment_id)->where('tocunit','=',$tocunit)->first();
        if (!$model){
            $model = new Commentnro;
            $model->comment_id = $comment_id;
            $model->tocunit = $tocunit;
            $model->save();
            $model->nro = $model->id + 16777216 * 35;
            $model->save();
        }
        $result = $model->nro;
        return $result;
    }

}
