<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monograph extends Model
{
    protected $dates = ['novelty_date'];

    public function mauthor()
    {
        return $this->belongsTo('App\Mauthor');
    }

    public function mfragments()
    {
        return $this->hasMany('App\Mfragment');
    }

    public function create_contents()
    {
        $object = [
            (object)[
                "fragment_id"=>1,
                "ordinal"=>1,
                "label"=>"Перелік умовних позначень",
            ],
            (object)[
                "fragment_id"=>2,
                "ordinal"=>2,
                "label"=>"РОЗДІЛ 1 ТЕОРЕТИКО-МЕТОДОЛОГІЧНІ ЗАСАДИ ІНСТИТУТУ РЕЧОВИХ ПРАВ",
                "children"=>[
                    (object)[
                        "fragment_id"=>2,
                        "ordinal"=>1,
                        "label"=>"1.1 Ґенеза правових досліджень речових прав у цивілістиці",
                    ],
                    (object)[
                        "fragment_id"=>3,
                        "ordinal"=>2,
                        "label"=>"1.2 Правова природа, ознаки та поняття речових прав",
                    ],
                    (object)[
                        "fragment_id"=>4,
                        "ordinal"=>3,
                        "label"=>"1.3 Об’єкти речових прав",
                    ],
                    (object)[
                        "fragment_id"=>5,
                        "ordinal"=>4,
                        "label"=>"1.4 Принципи речових прав",
                        "children"=>[
                            (object)[
                                "fragment_id"=>5,
                                "ordinal"=>1,
                                "label"=>"1.4.1 Поняття та загальна характеристика принципів речових прав",
                            ],
                            (object)[
                                "fragment_id"=>6,
                                "ordinal"=>2,
                                "label"=>"1.4.2 Окремі види та зміст принципів у речовому праві",
                                "children"=>[
                                    (object)[
                                        "fragment_id"=>6,
                                        "ordinal"=>1,
                                        "label"=>"1.4.2.1 Принцип добросовісного набуття речових прав",
                                    ],
                                    (object)[
                                        "fragment_id"=>7,
                                        "ordinal"=>2,
                                        "label"=>"1.4.2.2 Принцип публічності речових прав",
                                    ],
                                    (object)[
                                        "fragment_id"=>8,
                                        "ordinal"=>3,
                                        "label"=>"1.4.2.3 Принципи роз’єднання та абстракції у речовому праві",
                                    ],
                                    (object)[
                                        "fragment_id"=>9,
                                        "ordinal"=>4,
                                        "label"=>"1.4.2.4 Принцип поєднання речових прав",
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            (object)[
                "fragment_id"=>10,
                "ordinal"=>3,
                "label"=>"РОЗДІЛ 2 СИСТЕМА РЕЧОВИХ ПРАВ",
                "children"=>[
                    (object)[
                        "fragment_id"=>10,
                        "ordinal"=>1,
                        "label"=>"2.1 Проблема класифікації речових прав: доктринальні підходи",
                    ],
                    (object)[
                        "fragment_id"=>11,
                        "ordinal"=>2,
                        "label"=>"2.2 Право власності та його місце в системі речових прав",
                    ],
                    (object)[
                        "fragment_id"=>12,
                        "ordinal"=>3,
                        "label"=>"2.3 Речові права на чуже майно",
                    ],
                    (object)[
                        "fragment_id"=>13,
                        "ordinal"=>4,
                        "label"=>"2.4 Титульне та безтитульне володіння в цивільному праві. Проблема легітимації",
                    ],
                ],
            ],
            (object)[
                "fragment_id"=>14,
                "ordinal"=>4,
                "label"=>"РОЗДІЛ 3 РОЗВИТОК ДОКТРИНИ РЕЧОВИХ ПРАВ",
                "children"=>[
                    (object)[
                        "fragment_id"=>14,
                        "ordinal"=>1,
                        "label"=>"3.1 Формування доктрини речових прав та вплив традицій",
                    ],
                    (object)[
                        "fragment_id"=>15,
                        "ordinal"=>2,
                        "label"=>"3.2 Співвідношення новітньої української доктрини речових прав з дорево-люційною, радянською та західною доктринами",
                    ],
                    (object)[
                        "fragment_id"=>16,
                        "ordinal"=>3,
                        "label"=>"3.3 Перспективи розвитку доктрини речових прав в Україні",
                    ],
                ],
            ],
            (object)[
                "fragment_id"=>17,
                "ordinal"=>5,
                "label"=>"РОЗДІЛ 4 ВИНИКНЕННЯ, ЗДІЙСНЕННЯ ТА ПРИПИНЕННЯ РЕЧОВИХ ПРАВ",
                "children"=>[
                    (object)[
                        "fragment_id"=>17,
                        "ordinal"=>1,
                        "label"=>"4.1 Виникнення та припинення речових прав",
                        "children"=>[
                            (object)[
                                "fragment_id"=>17,
                                "ordinal"=>1,
                                "label"=>"4.1.1 Співвідношення правових категорій: термінологічна проблематика",
                            ],
                            (object)[
                                "fragment_id"=>18,
                                "ordinal"=>2,
                                "label"=>"4.1.2 Підстави набуття та припинення речових прав",
                            ],
                        ],
                    ],
                    (object)[
                        "fragment_id"=>19,
                        "ordinal"=>2,
                        "label"=>"4.2 Межі здійснення речових прав",
                        "children"=>[
                            (object)[
                                "fragment_id"=>19,
                                "ordinal"=>1,
                                "label"=>"4.2.1 Правова природа понять «межі», «обмеження» та «обтяження» ре- чових прав",
                            ],
                            (object)[
                                "fragment_id"=>20,
                                "ordinal"=>2,
                                "label"=>"4.2.2 Обмеження та обтяження речових прав у цивільному законодавстві України",
                            ],
                        ],
                    ],
                ],
            ],
            (object)[
                "fragment_id"=>21,
                "ordinal"=>6,
                "label"=>"РОЗДІЛ 5 ЦИВІЛЬНО-ПРАВОВИЙ ЗАХИСТ РЕЧОВИХ ПРАВ ТА РЕЧОВИХ ІНТЕРЕСІВ",
                "children"=>[
                    (object)[
                        "fragment_id"=>21,
                        "ordinal"=>1,
                        "label"=>"5.1 Захист суб’єктивного речового права",
                    ],
                    (object)[
                        "fragment_id"=>22,
                        "ordinal"=>2,
                        "label"=>"5.2 Проблеми захисту речового інтересу в цивільному праві України",
                    ],
                ],
            ],
            (object)[
                "fragment_id"=>23,
                "ordinal"=>7,
                "label"=>"СПИСОК ВИКОРИСТАНИХ ДЖЕРЕЛ",
            ],
        ];

        return $object;
    }

}
