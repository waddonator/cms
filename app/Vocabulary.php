<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vocabulary extends Model
{

    protected $connection= 'perseus';
    protected $primaryKey = 'vocabulary_production_type';
    protected $table = 'vocabularies';

    const CREATED_AT = 'last_modified_date';
    const UPDATED_AT = 'last_modified_date';

}
