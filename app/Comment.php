<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Comment extends Model
{

    //protected $dates = ['date_acc', 'date_force', 'reestr_date', 'reg_date', 'finish_date'];

    public function commentlocals()
    {
        return $this->hasMany('App\Commentlocal');
    }

    public function cauthors()
    {
        return $this->belongsToMany('App\Cauthor');
    }

    public function csource()
    {
        return $this->belongsTo('App\Csource');
    }

    public function cdocsubtype()
    {
        return $this->belongsTo('App\Cdocsubtype');
    }

    public function act()
    {
        return $this->belongsTo('App\Act');
    }

    /**
     * Функции формирования "метаданных" и "контента"
     * "Контент" комментариев всегда пустой
     */
    public function get_metadata($actversion_id=0)
    {
    	$actversion = Actversion::find($actversion_id);
        $document = (object)[];
        $document->{"@type"} = 'com';
        $document->id = (object)["nro"=>$this->nro, "versionId"=>$actversion->versionid, "fragmentId"=>1];
        $document->title = $this->name;
        $document->language = 'UK';
        $document->documentType = 'COMMENTARY';
        $document->noveltyDate = $this->updated_at->format('Y-m-d');
        $document->documentSubTypeId = $this->cdocsubtype_id + 16777216 * 16 + 100000 * 12;
        $document->effectivePeriod = (object)[
                "@type" => "tp",
                "startDate" => $actversion->date_acc->format('Y-m-d'),
            ];
        if (isset($actversion->date_start)){
            $document->effectivePeriod->startDate = $actversion->date_start->format('Y-m-d');
        }
        if (isset($actversion->date_end)){
            $document->effectivePeriod->endDate = $actversion->date_end->format('Y-m-d');
        }
        $document->publicationPlace = (object)[
                "sourcePublicationId" => $this->csource_id + 16777216 * 1 + 100000 * 12,
                "year" => $this->year,
                "issueFrom" => $this->issuefrom,
                "issueTo" => $this->issueto,
                "position" => $this->position,
                "string" => $this->string,
            ];
        $document->coverThumbnail = "data:image/png;base64,/9j/4AAQSkZJRgABAQIAbw";
        $document->exportFileLink = "";
        // авторы
            $document->authors = [];
            foreach ($this->cauthors as $cauthor) {
                $object = (object)[];
                $object->authorId = $cauthor->id + 16777216 * 13 + 100000 * 12;
                if (count($cauthor->cauthorroles)>0){
                    $object->authorRolesIds = [];
                    foreach ($cauthor->cauthorroles as $cauthorrole) {
                        $object->authorRolesIds[] = $cauthorrole->id + 16777216 * 11 + 100000 * 12;
                    }
                }
                $document->authors[] = $object;
            }
        // конец авторов
        // локальные коментарии
            $document->localCommentaries = [];
            foreach ($actversion->commentlocals->where('comment_id','=',$this->id)->where('trash','=',false) as $key => $commentlocal) {
            //foreach ($this->commentlocals as $key => $commentlocal) {
                $object = (object)[];
                $object->id = (object)[
                        "nro" => $commentlocal->nro,
                        "versionId" => $actversion->versionid,
                        "fragmentId" => 1,
                    ];
                $object->label = $commentlocal->name;
                $object->main = true;
                $object->ordinal = $key + 1;
                $object->type = "ARTICLE_COMMENTARY";
                $document->localCommentaries[] = $object;
            }
        // конец локальных коментариев


        return $document;
    }


    public function get_content()
    {
        $content = '';
        return $content;
    }

    /***
     *
     * Функция экспорта всех комментариев в базу Perseus
     *
     ***/
    static public function exportPerseus($area=0, $exported_at=NULL)
    {
        set_time_limit(0);
        $result = 0;
        $elements = Comment::select('id')
            ->where('export', '=', true)
            ->get();

        foreach ($elements as $row) {
            $element = Comment::find($row->id);
            Document::where('nro','=', $element->nro)->delete();
            foreach($element->act->actversions->where('trash','=',false) as $key => $actversion) {
                $model = new Document;
                $model->nro = $element->nro;
                $model->version_id = $actversion->versionid;
                $model->fragment_id = 1;
                $model->document_production_main_type = 'COMMENTARY';
                $model->metadata = pg_escape_bytea(json_encode( $element->get_metadata($actversion->id) , JSON_UNESCAPED_UNICODE));
                $model->content = pg_escape_bytea( $element->get_content() );
                //dump($model);
                $model->save();
            }
            $element->export = false;
            $element->save();
            $result++;
        }
        return $result;
    }


    /***
     *
     * Отображение количества документов
     *
     ***/
    static public function doccount()
    {
        return Comment::count();
    }

    /***
     *
     * Отображение количества документов нуждающихся в экспорте
     *
     ***/
    static public function expcount()
    {
        return Comment::where('export','=',true)->count();
    }

    /***
     *
     * Сводные данные для статистики
     *
     ***/
    static public function stat()
    {
        // $result = (object)[];
        // $result->all = Act::count();
        // $result->trash = Act::where('trash','=',1)->count();
        // $model = Exportdata::find(1);
        // $result->export = Act::where('updated_at','>',$model->exported_at)->count();
        // $result->lastnew = Act::where('trash','=',0)->orderby('created_at','desc')->first();
        // $result->lastedit = Act::where('trash','=',0)->orderby('updated_at','desc')->first();
        // $result->lasttrash = Act::where('trash','=',1)->orderby('updated_at','desc')->first();
        // return $result;
    }

}
