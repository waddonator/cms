<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cauthor extends Model
{
    public $timestamps = FALSE;

    public function cauthorroles()
    {
        return $this->belongsToMany('App\Cauthorrole');
    }

    public function comments()
    {
        return $this->belongsToMany('App\Comment');
    }
}
