<?php
namespace App\Widgets;

use App\Ract;
use App\Ractversion;
use App\Rbotstatus;
use App\Repllink;

use App\Widgets\Contract\ContractWidget;

use Illuminate\Support\Facades\DB;

class RbotWidget implements ContractWidget 
{
    public function execute(){
        //$labels = ['Нужно загрузить', 'В работе', 'Загружено', 'Контент создан', 'Остались незамененные ссылки', 'Ссылки заменены'];
        $labels = [];
        $elements = Rbotstatus::get();
        foreach ($elements as $key => $element) {
            $labels[$element->id] = $element->name;
        }
        $acts = Ract::select(DB::raw('count(*) as rbotstatus_count, rbotstatus_id'))->groupBy('rbotstatus_id')->orderBy('rbotstatus_id')->get();
        $actversions = Ractversion::select(DB::raw('count(*) as rbotstatus_count, rbotstatus_id'))->groupBy('rbotstatus_id')->orderBy('rbotstatus_id')->get();
        $data = (object)[
            'acts' => (object)[
                'Всего' => 0,
            ],
            'actversions' => (object)[
                'Всего' => 0,
            ]
        ];
        $sum = 0;
        foreach ($acts as $key => $act) {
            $data->acts->{$labels[$act->rbotstatus_id]} = $act->rbotstatus_count;
            $sum += $act->rbotstatus_count;
        }
        $data->acts->{'Всего'} = $sum;
        $sum = 0;
        foreach ($actversions as $key => $actversion) {
            $data->actversions->{$labels[$actversion->rbotstatus_id]} = $actversion->rbotstatus_count;
            $sum += $actversion->rbotstatus_count;
        }
        $data->actversions->{'Всего'} = $sum;

        $double_actversions = Ractversion::select(DB::raw('count(*) as double_count, act_id'))->groupBy(['act_id','versionid'])->havingRaw('count(*) > ?', [1])->count();
        $data->warnings = (object)['Задвоенные редакции' => $double_actversions];
        $data->links = (object)['Библиотека ТОС-ссылок' => Repllink::count()];
        return view('Widgets::rbot', [
            'data' => $data,
        ]);
    }
}