<?php
namespace App\Widgets;

use App\Comment;
use App\Commentlocal;

use App\Widgets\Contract\ContractWidget;

class CommentsWidget implements ContractWidget 
{
    public function execute(){
        $data = (object)[
            'comments' => Comment::count(),
            'commentlocals' => Commentlocal::count(),
        ];
        return view('Widgets::comments', [
            'data' => $data
        ]);
    }
}