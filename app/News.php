<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hexa\Common;

class News extends Model
{
    protected $common;

    public function __construct(/*Common $common*/) {
        $this->common = new Common;
    }    

    protected $dates = ['date_effective', 'date_event'];

    public function ncats()
    {
        return $this->belongsToMany('App\Ncat');
    }


    public function get_metadata()
    {
        $document = (object)[];
        $document->{"@type"} = 'news';
        $document->id = (object)["nro"=>$this->nro, "versionId"=>1, "fragmentId"=>1];
        $document->title = $this->name;
        $document->language = 'UK';
        $document->documentType = 'NEWS';
        $document->documentTypesForBoost = ["NEWS"];
        $document->newsletterTypesIds = [];
        foreach ($this->ncats as $key => $ncat) {
            $document->newsletterTypesIds[] = $ncat->id + 16777216 * 29;
        }
        $document->effectiveDate = $this->date_effective->format('Y-m-d');
        $document->eventDate = $this->date_event->format('Y-m-d');
        // $document->important = $this->important;
        $document->important = false;
        return $document;
    }


    public function get_content()
    {
        $content = '<?xml version="1.0" encoding="utf-8"?><html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemalocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd" xmlns="http://www.w3.org/1999/xhtml"><body><div class="a_body">' . $this->common->HtmlToXml($this->content) . '</div></body></html>';
        return $content;
    }


    /***
     *
     * Функция экспорта всех публикаций в базу Perseus
     *
     ***/
    static public function exportPerseus()
    {
        set_time_limit(0);
        $result = 0;
        $elements = News::select('id')
            ->where('export', '=', true)
            ->get();

        foreach ($elements as $row) {
            $element = News::find($row->id);
            Document::where('nro','=', $element->nro)->delete();
                $model = new Document;
                $model->nro = $element->nro;
                $model->version_id = 1;
                $model->fragment_id = 1;
                $model->document_production_main_type = 'NEWS';
                $model->metadata = pg_escape_bytea(json_encode( $element->get_metadata() , JSON_UNESCAPED_UNICODE));
                $model->content = pg_escape_bytea( $element->get_content() );
                $model->save();
            $element->export = false;
            $element->save();
            $result++;
        }
        return $result;
    }


    /***
     *
     * Отображение количества документов
     *
     ***/
    static public function doccount()
    {
        return News::count();
    }

    /***
     *
     * Отображение количества документов нуждающихся в экспорте
     *
     ***/
    static public function expcount()
    {
        return News::where('export','=',true)->count();
    }

    /***
     *
     * Сводные данные для статистики
     *
     ***/
    static public function stat()
    {
        $result = (object)[];
        $result->all = News::count();
        $result->trash = News::where('trash','=',true)->count();
        $result->export = News::where('export','=',true)->count();
        $result->lastnew = News::where('trash','=', false)->orderby('created_at','desc')->first();
        $result->lastedit = News::where('trash','=', false)->orderby('updated_at','desc')->first();
        $result->lasttrash = News::where('trash','=', true)->orderby('updated_at','desc')->first();
        return $result;
    }

}
