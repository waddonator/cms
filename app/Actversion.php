<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Hexa\Common;
use Carbon\Carbon;

class Actversion extends Model
{
    protected $common;

    public function __construct() {
        $this->common = new Common;
    }    

    protected $dates = ['date_acc', 'date_start', 'date_end'];

    public function act()
    {
        return $this->belongsTo('App\Act');
    }

    // public function comments()
    // {
    //     return $this->hasMany('App\Comment');
    // }

    public function commentlocals()
    {
        return $this->belongsToMany('App\Commentlocal');
    }

    /**
     * 
     * 
     * 
     */
    public function get_metadata()
    {
        $document = (object)[];

        $document->{"@type"} = 'act';
        $document->lawNumber = $this->act->number;
        $document->versionOrdinal = 0;
        //$document->id = (object)["nro"=>($this->act->id + 16777216 * $this->act->asubact_id), "versionId"=> $versionId, "fragmentId"=>1];
        $document->id = (object)["nro"=>($this->act->nro), "versionId"=> $this->versionid, "fragmentId"=>1];
        $document->title = $this->act->name ? str_replace('"', "'", $this->act->name) : $this->act->atype->name . ' б/н';
    # Формирование полного названия 
    # Если кодекс, то только название
    # Если закон, то тип и название
    # Остальные - автор, тип, дата, номер, название
        if ($this->act->atype_id == 7) /* кодекс */:
            $document->fullTitle = $document->title;
        elseif ($this->act->atype_id == 1) /* закон */:
            $document->fullTitle = $this->act->atype->name . ', ' . $document->title;
        else /* все остальные */:
            $document->fullTitle = $document->title;
        endif;
        $document->language = 'UK';
        $document->documentType = mb_strtoupper($this->act->asubact->name . '_ACT');
        $document->noveltyDate = $this->act->date_acc->format('Y-m-d');
        # дата принятия
        $document->enactmentDate = $this->act->date_force->format('Y-m-d');
        $document->documentSubTypeId = $this->act->atype_id + 16777216 * 16;
        $document->actTimeEvaluationType = 'EVALUATED';

            # Дата принятия, если есть дата встпупления в силу - то в последствии заменяем
                $document->versionEffectivePeriod = (object)["@type"=>"tp", "startDate"=>$this->date_acc->format('Y-m-d')];
                if (isset($this->date_start)){
                    $document->versionEffectivePeriod->startDate = $this->date_start->format('Y-m-d');
                }
            # Дата прекращения действия
                //if (isset($endDate)){
                if (isset($this->date_end)){
                    $document->versionEffectivePeriod->endDate = $this->date_end->format('Y-m-d');
                }
                $document->effectivePeriod = (object)["@type"=>"tp", "startDate"=>$this->act->date_force->format('Y-m-d')];
                if (isset($this->act->finish_date)){
                    $document->effectivePeriod->endDate = $this->act->finish_date->format('Y-m-d');
                }
                $document->actDates = [(object)["date"=>$this->act->date_force->format('Y-m-d'), "type"=>"ENTRY_INTO_FORCE", "mainDate"=>true]];

        if ($this->act->sources()->count() > 0){
            foreach ($this->act->sources as $key => $source) {
                $announced_date = Carbon::parse($source->pivot->announced_date);
                $document->publicationPlace= (object)[
                        "sourcePublicationId" => $source->id + 16777216 * 1,
                        "year" => $announced_date->format('Y'),
                        "issueFrom" => $source->pivot->issue_from,
                        "position" => $source->pivot->position,
                        "string"=> $this->get_letters($source->name) . '-' . $source->pivot->issue_from . '-' . $source->pivot->position,
                        "announcedDate" => $announced_date->format('Y-m-d'),
                        "pdfFile" => "",
                        "original" => true,                        
                        "sortString" => $source->name .'#'. $announced_date->format('Y-m-d') .'#'. $source->pivot->issue_from . '#' . $source->pivot->position,
                    ];
                break;
            }
        }
        if ($this->act->publishers()->count() > 0){
            $document->authorsIds = [];
            foreach ($this->act->publishers as $key => $publisher) {
                $document->authorsIds[] = $publisher->id + 16777216 * 13;
            }
        }
        # Убрано, т.к. в этом поле должны быть не ключевые слова
        // if ($this->act->akeywords()->count() > 0){
        //     $document->directHitSignatures = [];
        //     foreach ($this->act->akeywords as $key => $akeyword) {
        //         $document->directHitSignatures[] = $akeyword->name;
        //     }
        // }
        $document->signature = $document->title;
        return $document;
    }


    public function get_content()
    {
        $authors = [];
        foreach ($this->act->publishers as $key => $publisher) {
            $authors[] = $publisher->name;
        }
        $content  = '<?xml version="1.0" encoding="utf-8"?>'/* . "\r\n"*/;
        $content .= '<html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemalocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd" xmlns="http://www.w3.org/1999/xhtml">'/* . "\r\n"*/;
        $content .= '<body>'/* . "\r\n"*/;
            $content .= '<div class="a_hd">'/* . "\r\n"*/;
            $content .= '<div class="text-center"><img src="data:image/gif;base64,R0lGODlhPABQAKIHANHR0e7u7rS0tGVlZYyMjDk5OQAAAP///yH5BAEAAAcALAAAAAA8AFAAAAP/aLrc/jBKd6q9OOvNu1VeKI4bSJ4oZ6Ysu7aYMMDaSx+BItyXTROKAu9jGFYYAaPvsEMJGARlMWMoAE7AhfAU2GKWi+towPCKAODpdzETkbWkrLo3J7Ld5TGjVj822h1vQXh7VH0HD4AbglUeXRSGfA5RHIxmGowLfJIDcgZimHkcTw+bhgYzgpcXnpQaOQsEBZqRpzOwCqAxUJVws2mcFXKrFaQKrhhoYQe/h2moFbifGsYGyKx3zLRrwcKx1LwazUnawLYWyo0Z1dcV6V7NpmvQdofVuhZyTeXO/fQVgvAxYSDwQEAL8WrNUyRHYLppGRJWkEinm7djGaQJxGWG/yIRiwMxYtCYEQ7CbRXPoct2geRIlvzk0fl3IJ2iYgTXfTtpbmFLkxfuZRC0L6bCmTcLBc2JgegFj35AGr3AqB2Spyg/quRpgBxXay8HYe2JFIMcrzgaXHqHASoifzf1XXhow9glt8/iLihaLRc2kVxl2lFUTVEmsPm0oMULNzEDRRCoNvDKuJunbXaZ8ttW+Zy0AgLQBpTjioCsxxOzRj1VYECzVdJqKrVQTQCAzvMcFAW4s9lugxEE+3mw2yXbXZGP1mtwrRnkZX9LKR9eIIBQd1dpz34UpFk/SVu8E7DtO2IsAOhfJ8E9c4u0B2ixSxDD3o4X7hBcBbisO/D0t//qRCNDBDP05YAVY33H2isTQABafFOlNA8xGvF3TDPtpEaWfRrYhFMZSRhzk3/cLKgTYN6RE5s4qr0l1YeIhfTPiuZtOFwVBeSoo3c7lqGjjzvmOJtWuTVo5JEKFonkksmVqCSTULa4RAgEoSfHAOjdBhMJU3ogCCUiRpMdCjmy4AmMbdjFggyduLBMmC6hMMAOXaUwjHV3qMICGsKMGAJ+BNj00G9/gmZBm06UoUU4ZJrRxQAQejkBMY60hsmDJOAHHwq3UVoTGQREuoF3WhQ0So6EjgQEliJoSpMHAKxq6gb7oSqqZDq0qmWoMAAA6q04zWrBflUIy4WsHQDLxK8wRrQkC6tcCJAjr81Sg+oZZCBYLazTRlqrNcpuOxez7mSbqrghADFet+jSYF2g2yYAADs=" alt="Герб Украины" title="Герб Украины"/></div>';
                $content .= '<div class="a_aut">'. implode( ', ', $authors ) .'</div>'/* . "\r\n"*/;
                $content .= '<div class="a_nm">' . $this->act->atype->name . '</div>'/* . "\r\n"*/;
                $content .= '<div class="a_day">№' .  $this->act->number . ' від '. $this->act->date_acc->format('d.m.Y') . '</div>'/* . "\r\n"*/;
                $content .= '<div class="a_tl">' . $this->act->name . '</div>'/* . "\r\n"*/;
            $content .= '</div>'/* . "\r\n"*/;
            $content .= '<div class="a_body">' . $this->common->HtmlToXml($this->content) . '</div>'/* . "\r\n"*/;
        $content .= '</body>'/* . "\r\n"*/;
        $content .= '</html>';

        return $content;
    }


    function get_letters($value='')
    {
        $result = '';
        $arr = explode(" ", $value);
        foreach ($arr as $key => $row) {
            $result .= mb_substr($row, 0, 1);
        }
        return mb_strtoupper($result);
    }    


}
