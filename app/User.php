<?php

namespace App;

use App\Log;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }

    public function uriSegment($segment=0)
    {
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);
        $result = isset($uri_segments[$segment]) ? $uri_segments[$segment] : '';
        return $result;
    }

    public function log($ip = '', $logaction = 1, $object = 0)
    {
        $log = new Log;
        $log->user_id = $this->id;
        $log->logaction_id = $logaction;
        $log->object_id = $object;
        $log->ip = $ip;
        $log->save();
        return true;
    }

}
