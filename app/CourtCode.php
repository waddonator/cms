<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourtCode extends Model
{
    protected $fillable = ['name'];
    public $timestamps = FALSE;

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function instance()
    {
        return $this->belongsTo('App\Instance');
    }

}
