<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{

    protected $connection= 'perseus';
    //protected $primaryKey = ['nro', 'version_id', 'fragment_id'];
    protected $primaryKey = 'nro';
    protected $table = 'documents';

    const CREATED_AT = 'last_modified_date';
    const UPDATED_AT = 'last_modified_date';




}
