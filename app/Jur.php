<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jur extends Model
{

    /***
     *
     * Отображение количества документов
     *
     ***/
    static public function doccount()
    {
        return Jur::count();
    }
}
