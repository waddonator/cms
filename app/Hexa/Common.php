<?php

namespace App\Hexa;

class Common
{

    public function HtmlToXml($content = ''){

        $result = $content;

        # меняем все коды символов на символы        
        $result = str_replace('&laquo;', '«', $result);
        $result = str_replace('&raquo;', '»', $result);
        $result = str_replace('&nbsp;', ' ', $result);
        $result = str_replace('&rsquo;', '’', $result);
        $result = str_replace('&sect;', '§', $result);
        $result = str_replace('&mdash;', '—', $result);
        $result = str_replace('<hr>', '', $result);
        $result = str_replace('<br>', '<br />', $result);

        # меняем ампепсанд на правильный
        $result = str_replace('&', '&amp;', $result);
        $result = str_replace('&amp;amp;', '&amp;', $result);

        # меняем тег "жирный" на правильный
        $result = str_replace('<em ', '<i ', $result);
        $result = str_replace('<em>', '<i>', $result);
        $result = str_replace('</em>', '</i>', $result);

        # добавление зеленой кнопки
        $pattern = '/(<[^>]*unit-menu[^>]*>)/i';
        $replacement = '${1}<span class="unit-menu__btn"><i class="fa fa-plus-circle"></i></span>';
        $result = preg_replace($pattern, $replacement, $result);

        # убираем пустые ссылки
        $pattern = '/(<a[^>]*><\/a>)/i';
        $replacement = '';
        $result = preg_replace($pattern, $replacement, $result);

        # убираем переносы строк
        $result = str_replace(array("\n","\r"),'',$result);
        
        return $result;
    }

}
