<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voc extends Model
{
    protected $table = 'vocs';
    public $timestamps = FALSE;

    public function vocTables()
    {
        return $this->hasMany('App\VocTable');
    }
}
