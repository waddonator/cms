<?php

namespace App;

use App\Akeyword;
use App\Actversion;
use App\Exportdata;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Act extends Model
{

    protected $dates = ['date_acc', 'date_force', 'reestr_date', 'reg_date', 'finish_date'];

    public function actversions()
    {
        return $this->hasMany('App\Actversion');
    }

    public function atype()
    {
        return $this->belongsTo('App\Type');
    }

    public function publishers()
    {
        return $this->belongsToMany('App\Publisher');
    }

    public function sources()
    {
        return $this->belongsToMany('App\Source')->withPivot('announced_date', 'issue_from', 'position', 'string');
    }

    public function akeywords()
    {
        return $this->belongsToMany('App\Akeyword');
    }

    public function asubact()
    {
        return $this->belongsTo('App\Asubact');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * 
     * 
     * 
     */
    public function get_metadata()
    {
        $document = (object)[];
        return $document;
    }


    public function get_content()
    {
        $content = '';
        return $content;
    }

    public function sync_sources($obj = '{}')
    {
        $object = json_decode($obj);
        $sources = [];
        foreach ($object as $key => $value) {
            $sources[] = $key;
        }
        $this->sources()->sync($sources);
        foreach ($this->sources as $key => $value) {
            $value->pivot->announced_date = $object->{$value->pivot->source_id}->f1 ? $object->{$value->pivot->source_id}->f1 : null;
            $value->pivot->issue_from = (int)$object->{$value->pivot->source_id}->f2;
            $value->pivot->position = (int)$object->{$value->pivot->source_id}->f3;
            $value->pivot->save();
        }
        return true;
    }

    public function get_sources()
    {
        $object = (object)[];
        foreach ($this->sources as $key => $value) {
            $object->{$value->pivot->source_id} = (object)[
                    'f1' => Carbon::parse($value->pivot->announced_date)->format('Y-m-d'),
                    'f2' => $value->pivot->issue_from,
                    'f3' => $value->pivot->position,
                ];
        }
        return json_encode($object);
    }

    public function sync_keywords($arr = '[]')
    {
        $arr = json_decode($arr);
        $keywords = [];
        foreach ($arr as $key => $element) {
            $word = Akeyword::where('name', '=', $element)->first();
            if (!$word){
                $word = new Akeyword;
                $word->name = $element;
                $word->save();
            }
            $keywords[] = $word->id;
        }
        $this->akeywords()->sync($keywords);
        return true;
    }

    /***
     *
     * Функция экспорта всех законодательных актов в базу Perseus
     *
     ***/
    //static public function exportPerseus($area=0, $exported_at=NULL)
    static public function exportPerseus()
    {
        set_time_limit(0);
        $result = 0;
        $result2 = 0;
        $elements = Act::select('id')
            ->where('export', '=', true)
            ->get();

        foreach ($elements as $row) {
            $element = Act::find($row->id);
            //Document::where('nro','=', $element->nro)->delete();
            foreach ($element->actversions->where('export', '=', true) as $key => $actversion) {
                $where = [['nro','=', $element->nro]];
                $where[] = ['version_id', '=', $actversion->versionid];
                Document::where($where)->delete();
                $model = new Document;
                $model->nro = $element->nro;
                $model->version_id = $actversion->versionid;
                $model->fragment_id = 1;
                $model->document_production_main_type = 'ACT';
                $model->metadata = pg_escape_bytea(json_encode( $actversion->get_metadata() , JSON_UNESCAPED_UNICODE));
                $model->content = pg_escape_bytea( $actversion->get_content() );
                $model->save();
                $actversion->export = false;
                $actversion->save();
                $result2++;
            }
            $element->export = false;
            $element->save();
            $result++;
        }

        return trans('cms.doc-act') . ': ' . $result . ', ' . trans('cms.doc-actver') . ': ' . $result2;
    }


    /***
     *
     * Отображение количества документов
     *
     ***/
    static public function doccount()
    {
        return Actversion::count();
    }


    /***
     *
     * Отображение количества документов нуждающихся в экспорте
     *
     ***/
    static public function expcount()
    {
        return Act::where('export','=',true)->count() . ' (' . Actversion::where('export','=',true)->count() . ')';
    }


    /***
     *
     * Сводные данные для статистики виджета
     *
     ***/
    static public function stat()
    {
        $result = (object)[];
        $result->all = Act::count();
        $result->trash = Act::where('trash','=',1)->count();
        $model = Exportdata::find(1);
        //$result->export = Act::where('updated_at','>',$model->exported_at)->count();
        $result->export = Act::where('export','=',true)->count();
        $result->lastnew = Act::where('trash','=',0)->orderby('created_at','desc')->first();
        $result->lastedit = Act::where('trash','=',0)->orderby('updated_at','desc')->first();
        $result->lasttrash = Act::where('trash','=',1)->orderby('updated_at','desc')->first();
        return $result;
    }


    /**
     * 
     * Получаем все ТОС-элементы из контента последней редакции акта
     * 
     */
    public function get_tocunits()
    {
        $result = [""=>""];
        $actversion = $this->actversions->where('trash','=',false)->sortByDesc('versionid')->first();
        preg_match_all("|id=\"([^\"]*)\"[^>]*class=\"[^\"]*toc[^>]*\"|U", $actversion->content, $output, PREG_PATTERN_ORDER);
        foreach ($output[1] as $key => $row) {
            $result[$row] = $row;
        }
        return $result;
    }
}
