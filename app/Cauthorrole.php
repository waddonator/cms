<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cauthorrole extends Model
{
    public $timestamps = FALSE;

    public function cauthors()
    {
        return $this->belongsToMany('App\Cauthor');
    }
}
