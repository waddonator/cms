<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Repllink extends Model
{

    public $timestamps = FALSE;

    public function ract()
    {
        return $this->belongsTo('App\Ract','act_id');
    }

}
