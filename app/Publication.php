<?php

namespace App;

use App\Psource;

use Illuminate\Database\Eloquent\Model;
use App\Hexa\Common;

class Publication extends Model
{
    protected $common;
    protected $dates = ['novelty_date'];

    public function __construct(/*Common $common*/) {        
        $this->common = new Common;
    }    

    public function pauthors()
    {
        return $this->belongsToMany('App\Pauthor');
    }

    public function pdocsubtype()
    {
        return $this->belongsTo('App\Pdocsubtype');
    }

    public function pglosstype()
    {
        return $this->belongsTo('App\Pglosstype');
    }


    /**
     * + 16777216 * 16 + 100000 * 9
     * 16 - код справочника, 9 - код типа документа (в данном случае это публикации)
     * нужно, чтобы в общем справочнике хранить данные для разных типов документов
     */
    public function get_metadata()
    {
        $document = (object)[];
        $document->{"@type"} = 'pub';
        $document->id = (object)["nro"=>$this->nro, "versionId"=>1, "fragmentId"=>1];
        $document->title = $this->name;
        $document->language = 'UK';
        $document->documentType = 'ARTICLE';
        $document->noveltyDate = $this->created_at->format('Y-m-d');
        $document->documentSubTypeId = $this->pdocsubtype->id + 16777216 * 16 + 100000 * 9;
        $document->glossTypeId = $this->pglosstype->id + 16777216 * 12 + 100000 * 9;
        $document->subTitle = $this->subtitle;
        $document->abstractText = $this->abstracttext;
        // авторы
            $document->authors = [];
            $authors = [];
            foreach ($this->pauthors as $pauthor) {
                $authors = [];
                $object = (object)[];
                $object->authorId = $pauthor->id + 16777216 * 13 + 100000 * 9;
                $authors[] = $pauthor->name;
                if (count($pauthor->pauthorroles)>0){
                    $object->authorRolesIds = [];
                    foreach ($pauthor->pauthorroles as $pauthorrole) {
                        $object->authorRolesIds[] = $pauthorrole->id + 16777216 * 11 + 100000 * 9;
                    }
                }
                $document->authors[] = $object;
            }
        // конец авторов
        // статус документа (Если нужен - установить дату в validityEndDate и true в showValidity)
            // $document->validityEndDate = $this->novelty_date->format('Y-m-d');
            $document->showValidity = false;
            $document->legalStatusDate = "2004-05-01";
        // конец статуса документа
        // место публикации
            $source = Psource::find($this->psource_id);
            if ($source){
                $document->publicationPlace = (object)[];
                $document->publicationPlace->sourcePublicationId = $source->id + 16777216 * 1 + 100000 * 9;
                $document->publicationPlace->year = $this->year;
                $document->publicationPlace->issueFrom = $this->issuefrom;
                $document->publicationPlace->issueTo = $this->issueto;
                $document->publicationPlace->position = $this->position;
                $document->publicationPlace->string = $source->shortname . ' ' . $this->year . '/' . $this->position . '/' . $this->issuefrom . '-' . $this->issueto;
                $document->publicationPlace->isbns = [];
            }
        // конец места публикации
        $document->pdfLink = $this->pdflink;
        $document->copyrightToPdf = true;
        $document->magazine = true;
        $document->authorsNames = $authors;
        return $document;
    }


    public function get_content()
    {
        $content = '<?xml version="1.0" encoding="utf-8"?><html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemalocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd" xmlns="http://www.w3.org/1999/xhtml"><body><div class="a_body">' . $this->common->HtmlToXml($this->content) . '</div></body></html>';
        return $content;
    }


    /***
     *
     * Функция экспорта всех публикаций в базу Perseus
     *
     ***/
    static public function exportPerseus()
    {
        set_time_limit(0);
        $result = 0;
        $elements = Publication::select('id')
            ->where('export', '=', true)
            ->get();

        foreach ($elements as $row) {
            $element = Publication::find($row->id);
            Document::where('nro','=', $element->nro)->delete();
                $model = new Document;
                $model->nro = $element->nro;
                $model->version_id = 1;
                $model->fragment_id = 1;
                $model->document_production_main_type = 'PUBLICATION';
                $model->metadata = pg_escape_bytea(json_encode( $element->get_metadata() , JSON_UNESCAPED_UNICODE));
                $model->content = pg_escape_bytea( $element->get_content() );
                $model->save();
            $element->export = false;
            $element->save();
            $result++;
        }
        return $result;
    }


    /***
     *
     * Отображение количества документов
     *
     ***/
    static public function doccount()
    {
        return Publication::count();
    }

    /***
     *
     * Отображение количества документов нуждающихся в экспорте
     *
     ***/
    static public function expcount()
    {
        return Publication::where('export','=',true)->count();
    }


    /***
     *
     * Сводные данные для статистики
     *
     ***/
    static public function stat()
    {
        $result = (object)[];
        $result->all = Publication::count();
        $result->trash = Publication::where('trash','=',true)->count();
        $result->export = Publication::where('export','=',true)->count();
        $result->lastnew = Publication::where('trash','=', false)->orderby('created_at','desc')->first();
        $result->lastedit = Publication::where('trash','=', false)->orderby('updated_at','desc')->first();
        $result->lasttrash = Publication::where('trash','=', true)->orderby('updated_at','desc')->first();
        return $result;
    }

}
