<?php

namespace App;

use App\Document;

use Illuminate\Database\Eloquent\Model;

class Exportdata extends Model
{
    protected $dates = ['exported_at'];
    public $timestamps = FALSE;


    /* 
     * определение количества записей в наших таблицах (данные беруться из моделей)
     */
    public function reccount()
    {
        return method_exists(($this->model), 'doccount') ? ($this->model)::doccount() : 0;
    }


    /* 
     * определение количества записей нуждающихся в экспорте
     */
    public function expcount()
    {
        return method_exists(($this->model), 'expcount') ? ($this->model)::expcount() : 0;
    }


    /*
     * определение количества записей в базе Perseus
     */
    public function percount()
    {
        return Document::where('document_production_main_type','=',$this->document_production_main_type)->count();
    }
}
