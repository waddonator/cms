<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pauthorrole extends Model
{
    public $timestamps = FALSE;

    public function pauthors()
    {
        return $this->belongsToMany('App\Pauthor');
    }
}
