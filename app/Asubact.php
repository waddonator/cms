<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asubact extends Model
{
    protected $fillable = ['name'];
    public $timestamps = FALSE;

}
