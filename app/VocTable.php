<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class VocTable extends Model
{
    protected $table = 'voc_tables';
    public $timestamps = FALSE;

    public function voc()
    {
        return $this->belongsTo('App\Voc');
    }

}
