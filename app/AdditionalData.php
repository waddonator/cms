<?php

namespace App;
use App\Block;

use Illuminate\Database\Eloquent\Model;

class AdditionalData extends Model
{

    protected $connection= 'perseus';
    protected $primaryKey = 'perseus_entity_type';
    protected $table = 'additional_data';

    const CREATED_AT = 'last_modified_date';
    const UPDATED_AT = 'last_modified_date';



    /***
     *
     * Справочник blocks
     *
     */
    static function create_blocks()
    {
        $data = [];
        $elements = Block::get();
        foreach ($elements as $key => $element) {
            $data[] = (object)[
                    'id'=>$element->id,
                    'guid'=>$element->guid,
                    'title'=>$element->title,
                    'editorStatus'=>$element->editorStatus,
                ];
        }
        $model = AdditionalData::find('blocks');
        if (!$model){
            $model = new AdditionalData;
            $model->perseus_entity_type = 'blocks';
        }
        $model->data = pg_escape_bytea(json_encode( $data , JSON_UNESCAPED_UNICODE));
        $model->save();
        return true;
    }


    static function create_blocks_map()
    {
        $data = (object)['nrosToBlocksMap'=>(object)[]];
        $elements = Document::select('nro')->get();
        foreach ($elements as $key => $element) {
            /* LEGAL ACTS */
            if ($element->nro > 16777216 && $element->nro < 50331648){
                $data->nrosToBlocksMap->{$element->nro} = [1];
            }
            /* NEWS */
            if ($element->nro > 654311424 && $element->nro < 671088640){
                $data->nrosToBlocksMap->{$element->nro} = [2];
            }            
            /* JURISPRUDENCE */
            if ($element->nro > 520093696 && $element->nro < 536870912){
                $data->nrosToBlocksMap->{$element->nro} = [3];
            }
            /* MONOGRAPH */
            if ($element->nro > 369098752 && $element->nro < 385875967){ 
                $data->nrosToBlocksMap->{$element->nro} = [4];
            }
            /* PUBLICATION */
            if ($element->nro > 150994944 && $element->nro < 167772159){
                $data->nrosToBlocksMap->{$element->nro} = [5];
            }
            /* COMMENTARY */
            if (($element->nro > 201326592 && $element->nro < 218103807) ||
                ($element->nro > 587202560 && $element->nro < 603979775)){
                $data->nrosToBlocksMap->{$element->nro} = [6];
            }
        }
        $model = AdditionalData::find('documents_blocks_relations');
        if (!$model){
            $model = new AdditionalData;
            $model->perseus_entity_type = 'documents_blocks_relations';
        }
        $model->data = pg_escape_bytea(json_encode( $data , JSON_UNESCAPED_UNICODE));
        $model->save();
        return true;
    }

}
