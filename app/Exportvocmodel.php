<?php

namespace App;

use App\Document;

use Illuminate\Database\Eloquent\Model;

class Exportvocmodel extends Model
{
    public $timestamps = FALSE;

    public function exportvoc()
    {
        return $this->belongsTo('App\Exportvoc');
    }

}
