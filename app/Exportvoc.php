<?php

namespace App;

use App\Document;

use Illuminate\Database\Eloquent\Model;

class Exportvoc extends Model
{
    public $timestamps = FALSE;

    public function exportvocmodels()
    {
        return $this->hasMany('App\Exportvocmodel');
    }

}
