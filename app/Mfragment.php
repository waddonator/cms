<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfragment extends Model
{
    protected $dates = ['novelty_date'];

    public function monograph()
    {
        return $this->belongsTo('App\Monograph');
    }

    public function mnodetype()
    {
        return $this->belongsTo('App\Mnodetype');
    }

    public function create_content()
    {
        $result = '';
        $result .= '<?xml version="1.0" encoding="UTF-8"?>';
        $result .= '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd">';
        $result .= '<body>';
        $result .= '<div class="toc c_sec l'. ($this->level + 1).'" id="r'.$this->ordinal.'">';
        
        $model = $this;
        $header = '';

        for ($i=$model->level; $i >= 0; $i--) {
            $temp = '<div class="c_sec-hdr">';
            if (!$i){
                $temp .= '<span class="c_sec-type">' . $model->type . '</span>';
                $temp .= '<span class="c_sec-nr">' . $model->nr . '</span>';
                $temp .= '<div class="c_sec-tl">' . $model->name . '</div>';
            } else {
                $temp .= '<span class="c_sec-nr">' . $model->nr . '</span>';
                $temp .= '<span>' . $model->name . '</span>';
            }
            $temp .= '</div>';
            $model = Mfragment::find($model->parent_id);
            $header = $temp . $header;
        }
        $result .= $header;
        $result .= $this->content;
        $result .= '</div></body></html>';

        return $result;
    }

}
