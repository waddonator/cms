<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Hexa\Common;

class Commentlocal extends Model
{
    protected $common;

    public function __construct() {
        /* Подключаем библиотеку Common */
        $this->common = new Common;
    }    

    public function comment()
    {
        return $this->belongsTo('App\Comment');
    }

    public function actversions()
    {
        return $this->belongsToMany('App\Actversion');
    }

    /**
     * Функции формирования "метаданных" и "контента"
     * "Метаданные" локальных комментариев всегда пустые
     */
    public function get_metadata($actversion_id)
    {
        $actversion = Actversion::find($actversion_id);
        $document = (object)[];
        $document->{"@type"} = 'com';
        $document->id = (object)["nro"=>$this->nro, "versionId"=>$actversion->versionid, "fragmentId"=>1];
        $document->title = $this->name;
        $document->language = 'UK';
        return $document;
    }


    public function get_content()
    {
        $content = '<?xml version="1.0" encoding="utf-8"?><html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemalocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd" xmlns="http://www.w3.org/1999/xhtml"><body><div class="a_body">' . $this->common->HtmlToXml($this->content) . '</div></body></html>';
        return $content;
    }


    /***
     * Функция экспорта всех локальных комментариев в базу Perseus
     ***/
    static public function exportPerseus()
    {
        set_time_limit(0);
        $result = 0;
        $elements = Commentlocal::select('id')
            ->where('export', '=', true)
            ->get();

        foreach ($elements as $row) {
            $element = Commentlocal::find($row->id);
            Document::where('nro','=', $element->nro)->delete();
            foreach($element->actversions->where('trash','=',false) as $key => $actversion) {
                $model = new Document;
                $model->nro = $element->nro;
                $model->version_id = $actversion->versionid;
                $model->fragment_id = 1;
                $model->document_production_main_type = 'COMMENTARY';
                $model->metadata = pg_escape_bytea(json_encode( $element->get_metadata($actversion->id) , JSON_UNESCAPED_UNICODE));
                $model->content = pg_escape_bytea( $element->get_content() );
                //dump($model);
                $model->save();
                $result++;
            }
            $element->export = false;
            $element->save();
        }
        return $result;
    }


    /***
     * Отображение количества документов
     ***/
    static public function doccount()
    {
        return Commentlocal::count();
    }


    /***
     * Отображение количества документов нуждающихся в экспорте
     ***/
    static public function expcount()
    {
        return Commentlocal::where('export','=',true)->count();
    }
}
