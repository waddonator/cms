<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Commentnro;

class DocumentsRelation extends Model
{

    protected $connection= 'perseus';
    protected $primaryKey = 'document_relation_type';
    protected $table = 'documents_relations';

    const CREATED_AT = 'last_modified_date';
    const UPDATED_AT = 'last_modified_date';

    /***
     *
     * Справочник act_commentary
     *
     */
    static function act_commentary()
    {
        $data = [];

        $models = Commentnro::get();
        foreach ($models as $key => $model) {
            $name = preg_replace("/\(([\d]+)\)/i","$1",$model->tocunit);
            $name = preg_replace("/([a-zA-Z]+)/i"," $1. ",$name);
            $name = ucwords(trim($name));
            $data[] = (object)[
                    "@type" => "ad",
                    "id" => $model->nro . "_" . $model->comment->act->nro,
                    "activeNro" => $model->nro,
                    "passiveNro" => $model->comment->act->nro,
                    "units" => [
                            (object)[
                                    "@type" => "du",
                                    "unit" => (object)[
                                            "id" => $model->tocunit,
                                            "name" => $name,
                                            "weight" => 1
                                        ]
                                ]
                        ]
                ];
        }

        $model = DocumentsRelation::find('act_commentary');
        if (!$model){
            $model = new DocumentsRelation;
            $model->document_relation_type = 'act_commentary';
        }
        $model->data = pg_escape_bytea(json_encode( $data , JSON_UNESCAPED_UNICODE));
        $model->save();
        return true;
    }

}
