<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['auth']], function() {

    Route::get('/singin',    ['as'=>'singin', 'uses'=>'GeneralController@singin']);

    Route::group(['middleware' => ['isAdmin']], function() {
        Route::resource('users', 'UserController');
        Route::resource('roles', 'RoleController', ['except' => 'show']);
        Route::resource('permissions', 'PermissionController', ['except' => 'show']);
    });

    Route::group(['prefix' => 'documents', 'namespace' => 'Documents',], function() {

        Route::group(['middleware' => ['isAdmin']], function() {
            Route::group(['prefix' => 'racts'], function() {
                Route::group(['prefix' => 'ractversions'], function() {
                    Route::get('rmeta/{id}',    ['as'=>'ractversions.rmeta', 'uses'=>'RactversionController@rmeta']);
                    Route::get('jcontent/{id}',    ['as'=>'ractversions.jcontent', 'uses'=>'RactversionController@jcontent']);
                    Route::get('rwork/{id}',    ['as'=>'ractversions.rwork', 'uses'=>'RactversionController@rwork']);
                });
                Route::get('rmeta/{id}',    ['as'=>'racts.rmeta', 'uses'=>'RactController@rmeta']);
                Route::get('repllink/{id}',    ['as'=>'racts.repllink', 'uses'=>'RactController@repllink']);
                Route::resource('ractversions', 'RactversionController');
            });
            Route::resource('racts', 'RactController');
        });

        Route::group(['middleware' => ['isActs']], function() {
            Route::group(['prefix' => 'acts'], function() {
                Route::resource('actversions', 'ActversionController');
                Route::group(['prefix' => 'comments'], function() {
                    Route::resource('commentlocals', 'CommentlocalController');
                });
                Route::resource('comments', 'CommentController');
                Route::get('trash',    ['as'=>'act-trash', 'uses'=>'ActController@trash']);
            });
            Route::resource('acts', 'ActController');
        });

        Route::group(['prefix' => 'news'], function() {
            Route::get('trash',    ['as'=>'news-trash', 'uses'=>'NewsController@trash']);
        });
        Route::resource('news', 'NewsController');

        Route::resource('jurs', 'JurController');
        Route::resource('monographs', 'MonographController');
        Route::group(['prefix' => 'monographs'], function() {
            Route::resource('mfragments', 'MfragmentController');
        });

        Route::group(['middleware' => ['isPubs']], function() {
            Route::group(['prefix' => 'publications'], function() {
                Route::get('trash',    ['as'=>'publication-trash', 'uses'=>'PublicationController@trash']);
            });
            Route::resource('publications', 'PublicationController');
        });
    });

    Route::group(['prefix' => 'vocabularies', 'namespace' => 'Vocabularies'], function() {
        Route::resource('regions', 'RegionController');

        Route::resource('publishers', 'PublisherController');
        Route::resource('types', 'TypeController');
        Route::resource('sources', 'SourceController');
        Route::resource('asubacts', 'AsubactController');
        Route::resource('akeywords', 'AkeywordController');

        Route::resource('ncats', 'NcatController');

        Route::resource('justice-kind', 'JusticeKindController');
        Route::resource('judgment-code', 'JudgmentCodeController');
        Route::resource('court-code', 'CourtCodeController');
        Route::resource('cause-category', 'CauseCategoryController');
        Route::resource('instances', 'InstanceController');
        Route::resource('judges', 'JudgeController');

        Route::resource('mauthors', 'MauthorController');
        Route::resource('mnodetypes', 'MnodetypeController');

        Route::resource('pauthors', 'PauthorController');
        Route::resource('pauthorroles', 'PauthorroleController');
        Route::resource('pglosstypes', 'PglosstypeController');
        Route::resource('psources', 'PsourceController');
        Route::resource('pdocsubtypes', 'PdocsubtypeController');

        Route::resource('cauthors', 'CauthorController');
        Route::resource('cauthorroles', 'CauthorroleController');
        Route::resource('csources', 'CsourceController');
        Route::resource('cdocsubtypes', 'CdocsubtypeController');
    });

    Route::group(['prefix' => 'tools', 'middleware' => ['isAdmin']], function() {

        Route::resource('exportdatas', 'ExportdataController');
        Route::resource('exportvocs',  'ExportvocController');
        Route::resource('exportvocmodels', 'ExportvocmodelController');
        Route::resource('blocks', 'BlockController');
        
        Route::post('/export',    ['as'=>'export', 'uses'=>'ExportController@index']);
        Route::post('/exportvoc', ['as'=>'exportvoc', 'uses'=>'ExportController@exportvoc']);

        Route::resource('logactions', 'LogactionController');
        Route::resource('logs', 'LogController');

        Route::get('/tools',       ['as'=>'tools', 'uses'=>'GeneralController@tools']);
        Route::get('/reincrement', ['as'=>'reincrement', 'uses'=>'ToolController@reincrement']);
        Route::get('/lexinform',   ['as'=>'lexinform', 'uses'=>'ToolController@lexinform']);
        Route::get('/blocks-map',  ['as'=>'blocks-map', 'uses'=>'GeneralController@create_blocks_map']);
        Route::get('/relations',   ['as'=>'relations', 'uses'=>'ToolController@relations']);
        # страница для тестов
        Route::get('/dev',   ['as'=>'dev', 'uses'=>'ToolController@dev']);
        Route::get('/link',  ['as'=>'link','uses'=>'ToolController@link']);
        Route::get('/toc',   ['as'=>'toc', 'uses'=>'ToolController@toc']);
        # страница для RADA BOT
        Route::get('/radabot',   ['as'=>'radabot', 'uses'=>'ToolController@radabot']);
        Route::resource('ractions', 'RactionController', ['except' => 'show']);

        Route::get('rbotstatuses/datatable',    ['as'=>'rbotstatuses.datatable','uses'=>'RbotstatusController@datatable']);
        Route::post('rbotstatuses/datatable',    ['as'=>'rbotstatuses.datatable','uses'=>'RbotstatusController@datatable']);
        Route::resource('rbotstatuses', 'RbotstatusController', ['except' => 'show']);
    });

    Route::match(['get', 'post'],'/account',    ['as'=>'account', 'uses'=>'GeneralController@account']);

});

Route::post( '/logout', ['as'=>'logout', 'uses'=>'GeneralController@logout']);

Route::get('/',    ['as'=>'home', 'uses'=>'GeneralController@index']);

