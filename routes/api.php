<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function() {
    Route::get('docs', ['uses' => 'RadaController@docs']);
    Route::get('lastmeta', ['uses' => 'RadaController@lastmeta']);
    Route::get('emptyversions', ['uses' => 'RadaController@emptyversions']);
    Route::get('getradadocument', ['uses' => 'RadaController@getRadaDocument']);
    //Route::get('getradadoublecontent', ['uses' => 'RadaController@getRadaDoubleContent']);
    Route::get('updateddocs', ['uses' => 'RadaController@updatedDocs']);
    Route::get('buildcontent', ['uses' => 'RadaController@buildContent']);
    Route::get('changelinks/{id?}', ['uses' => 'RadaController@changeLinks']);

});
