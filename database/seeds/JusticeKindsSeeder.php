<?php

use App\JusticeKind;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class JusticeKindsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('justice_kinds.json');
        JusticeKind::insert(json_decode($data, true));
    }
}
