<?php

use App\Voc;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class VocsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('vocs.json');
        Voc::insert(json_decode($data, true));
    }
}
