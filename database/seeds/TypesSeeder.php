<?php

use App\Type;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class TypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('types.json');
        Type::insert(json_decode($data, true));
    }
}
