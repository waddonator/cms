<?php

use App\Source;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class SourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('sources.json');
        Source::insert(json_decode($data, true));
    }
}
