<?php

use App\Block;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class BlocksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('blocks.json');
        Block::insert(json_decode($data, true));
    }
}
