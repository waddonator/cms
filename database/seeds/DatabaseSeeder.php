<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\Role_has_permission;
use App\User;
use App\Model_has_role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // DB::table('permissions')->insert([
        //     [
        //             'name'=>'admin',
        //             'guard_name'=>'web',
        //     ],
        //     [
        //             'name'=>'acts',
        //             'guard_name'=>'web',
        //     ],
        //     [
        //             'name'=>'news',
        //             'guard_name'=>'web',
        //     ],
        //     [
        //             'name'=>'jurisprudence',
        //             'guard_name'=>'web',
        //     ]
        // ]);


        // DB::table('roles')->insert([
        //     [
        //             'name'=>'admin',
        //             'guard_name'=>'web',
        //     ],
        //     [
        //             'name'=>'lawyer',
        //             'guard_name'=>'web',
        //     ],
        //     [
        //             'name'=>'content manager',
        //             'guard_name'=>'web',
        //     ],
        //     [
        //             'name'=>'judge',
        //             'guard_name'=>'web',
        //     ]
        // ]);

        // DB::table('role_has_permissions')->insert([
        //     [
        //             'permission_id'=>1,
        //             'role_id'=>1,
        //     ],
        //     [
        //             'permission_id'=>2,
        //             'role_id'=>2,
        //     ],
        //     [
        //             'permission_id'=>3,
        //             'role_id'=>3,
        //     ],
        //     [
        //             'permission_id'=>4,
        //             'role_id'=>4,
        //     ]
        // ]);



        // User::create([
        //             'email'=>'waddon@narod.ru',
        //             'name'=>'Вадим Донец',
        //             'password'=>'killer666',
        //         ]);
        // User::create([
        //             'email'=>'admin@cms.activelex.com',
        //             'name'=>'Администратор',
        //             'password'=>'l8qSJd6hWvNicKht',
        //         ]);
        // User::create([
        //             'email'=>'lawyer@cms.activelex.com',
        //             'name'=>'Юрист',
        //             'password'=>'ZwNCJoU7s8ZbZjBn',
        //         ]);
        // User::create([
        //             'email'=>'content@cms.activelex.com',
        //             'name'=>'Контент менеджер',
        //             'password'=>'TYanisZxGlA2gkDa',
        //         ]);
        // User::create([
        //             'email'=>'judge@cms.activelex.com',
        //             'name'=>'Судья',
        //             'password'=>'VAYxxjUXLUpfmZzA',
        //         ]);

        // DB::table('model_has_roles')->insert([
        //     [
        //             'role_id'=>1,
        //             'model_id'=>1,
        //             'model_type'=>'App\User',
        //     ],
        //     [
        //             'role_id'=>1,
        //             'model_id'=>2,
        //             'model_type'=>'App\User',
        //     ],
        //     [
        //             'role_id'=>2,
        //             'model_id'=>3,
        //             'model_type'=>'App\User',
        //     ],
        //     [
        //             'role_id'=>3,
        //             'model_id'=>4,
        //             'model_type'=>'App\User',
        //     ],
        //     [
        //             'role_id'=>4,
        //             'model_id'=>5,
        //             'model_type'=>'App\User',
        //     ]
        // ]);


        // $this->call(InstancesSeeder::class);
        // $this->call(RegionsSeeder::class);
        // $this->call(JusticeKindsSeeder::class);
        // $this->call(JudgmentCodesSeeder::class);
        // $this->call(CourtCodesSeeder::class);
        // $this->call(CauseCategoriesSeeder::class);

        // $this->call(VocsSeeder::class);

        // $this->call(PublishersSeeder::class);
        // $this->call(TypesSeeder::class);
        // $this->call(BlocksSeeder::class);

        // $this->call(SourcesSeeder::class);

        $this->call(AsubactsSeeder::class);
        $this->call(AkeywordsSeeder::class);

    }
}
