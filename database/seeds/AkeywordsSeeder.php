<?php

use App\Akeyword;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class AkeywordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('akeywords.json');
        Akeyword::insert(json_decode($data, true));
    }
}
