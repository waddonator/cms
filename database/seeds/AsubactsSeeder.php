<?php

use App\Asubact;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class AsubactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('asubacts.json');
        Asubact::insert(json_decode($data, true));
    }
}
