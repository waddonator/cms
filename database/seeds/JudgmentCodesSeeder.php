<?php

use App\JudgmentCode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class JudgmentCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('judgment_codes.json');
        JudgmentCode::insert(json_decode($data, true));
    }
}
