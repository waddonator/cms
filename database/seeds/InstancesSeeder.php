<?php

use App\Instance;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class InstancesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('instances.json');
        Instance::insert(json_decode($data, true));
    }
}
