<?php

use App\Publisher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class PublishersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('publishers.json');
        Publisher::insert(json_decode($data, true));
    }
}
