<?php

use App\CourtCode;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class CourtCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('court_codes.json');
        CourtCode::insert(json_decode($data, true));
    }
}
