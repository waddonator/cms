<?php

use App\CauseCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class CauseCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('cause_сategories.json');
        CauseCategory::insert(json_decode($data, true));
    }
}
