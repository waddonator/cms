<?php

use App\Region;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = Storage::disk('db')->get('regions.json');
        Region::insert(json_decode($data, true));
    }
}
