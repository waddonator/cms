<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsInPublications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publications', function (Blueprint $table) {
            $table->boolean('trash')->default('false');
            $table->boolean('export')->default('false');
            $table->integer('nro')->default('0')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('publications', function (Blueprint $table) {
            $table->dropColumn('nro');
            $table->dropColumn('export');
            $table->dropColumn('trash');
        });
    }
}
