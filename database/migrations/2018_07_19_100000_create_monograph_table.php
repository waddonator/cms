<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonographTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mauthors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('mnodetypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('monographs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('mauthor_id')->unsigned();
            $table->foreign('mauthor_id')->references('id')->on('mauthors')->onDelete('cascade');
            $table->timestamp('novelty_date');
            $table->timestamps();
        });

        Schema::create('mfragments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('monograph_id')->unsigned();
            $table->foreign('monograph_id')->references('id')->on('monographs')->onDelete('cascade');
            $table->integer('mnodetype_id')->unsigned();
            $table->foreign('mnodetype_id')->references('id')->on('mnodetypes')->onDelete('cascade');
            $table->integer('parent_id')->unsigned();
            $table->integer('level')->unsigned()->default(0);
            $table->string('label')->default('');
            $table->string('type')->default('');
            $table->string('nr')->default('');
            $table->integer('ordinal')->unsigned()->default(0);
            $table->longtext('content');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfragments');
        Schema::dropIfExists('monographs');
        Schema::dropIfExists('mnodetypes');
        Schema::dropIfExists('mauthors');
    }
}
