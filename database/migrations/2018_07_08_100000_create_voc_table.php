<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('vocs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vocabulary_production_type')->default('');
            $table->string('type')->default('');
            $table->string('info')->default('');
        });

        Schema::create('voc_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('table')->default('');
            $table->string('model')->default('');
            $table->bigInteger('additional')->default(0);
            $table->string('info')->default('');
            $table->integer('voc_id')->unsigned();
            $table->foreign('voc_id')->references('id')->on('vocs')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voc_tables');
        Schema::dropIfExists('vocs');
    }
}
