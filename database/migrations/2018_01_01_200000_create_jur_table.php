<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('regions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('justice_kinds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('judgment_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('cause_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 257);
        });

        Schema::create('court_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('instance_id')->unsigned();
            $table->foreign('instance_id')->references('id')->on('instances')->onDelete('cascade');
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
        });

        // Schema::create('jurs', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->string('cause_num');
        //     $table->string('judge');
        //     $table->timestamp('adjudication_date');
        //     $table->timestamp('receipt_date');
        //     $table->integer('justice_kind_id')->unsigned();
        //     //$table->foreign('justice_kind_id')->references('id')->on('justice_kinds')->onDelete('cascade');
        //     $table->integer('judgment_code_id')->unsigned();
        //     //$table->foreign('judgment_code_id')->references('id')->on('judgment_codes')->onDelete('cascade');
        //     $table->integer('category_code_id')->unsigned();
        //     //$table->foreign('category_code_id')->references('id')->on('cause_categories')->onDelete('cascade');
        //     $table->integer('court_code_id')->unsigned();
        //     //$table->foreign('court_code_id')->references('id')->on('court_codes')->onDelete('cascade');
        //     $table->timestamps();
        //     $table->longtext('text');
        // });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('jurs');
        Schema::dropIfExists('court_codes');
        Schema::dropIfExists('cause_categories');
        Schema::dropIfExists('judgment_codes');
        Schema::dropIfExists('justice_kinds');
        Schema::dropIfExists('regions');
    }
}
