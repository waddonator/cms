<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppendColumnsInActs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acts', function (Blueprint $table) {
            $table->integer('nro')->default('0')->unsigned();
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
        });

        Schema::table('actversions', function (Blueprint $table) {
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
            $table->timestamp('date_start')->nullable();
            $table->timestamp('date_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('actversions', function (Blueprint $table) {
            $table->dropColumn('export');
            $table->dropColumn('exported');
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
        });
        Schema::table('acts', function (Blueprint $table) {
            $table->dropColumn('nro');
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
        });
    }
}
