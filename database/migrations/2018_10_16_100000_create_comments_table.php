<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('cauthors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('cauthorroles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortname');
        });

        Schema::create('cauthor_cauthorrole', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cauthor_id')->unsigned();
            $table->foreign('cauthor_id')->references('id')->on('cauthors')->onDelete('cascade');
            $table->integer('cauthorrole_id')->unsigned();
            $table->foreign('cauthorrole_id')->references('id')->on('cauthorroles')->onDelete('cascade');
        });

        Schema::create('cdocsubtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('csources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortname');
        });

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('act_id')->unsigned();
            $table->foreign('act_id')->references('id')->on('acts')->onDelete('cascade');
            $table->integer('cdocsubtype_id')->unsigned();
            $table->foreign('cdocsubtype_id')->references('id')->on('pdocsubtypes')->onDelete('cascade');
            $table->integer('csource_id')->unsigned()->default(0);
            $table->integer('year')->default(0);
            $table->integer('issuefrom')->default(0);
            $table->integer('issueto')->default(0);
            $table->integer('position')->default(0);
            $table->string('string')->default('');
            $table->integer('nro')->default('0')->unsigned();
            $table->boolean('trash')->default('false');
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
            $table->timestamps();
        });

        Schema::create('cauthor_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cauthor_id')->unsigned();
            $table->foreign('cauthor_id')->references('id')->on('cauthors')->onDelete('cascade');
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade');
        });

        Schema::create('commentnros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('id')->on('comments')->onDelete('cascade');
            $table->string('tocunit');
            $table->integer('nro')->default('0')->unsigned();
        });

        Schema::create('commentlocals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('comment_id')->unsigned();
            $table->foreign('comment_id')->references('id')->on('actversions')->onDelete('cascade');
            $table->integer('nro')->default('0')->unsigned();
            $table->string('tocunit')->default('');
            $table->boolean('trash')->default('false');
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
            $table->longtext('content');
            $table->timestamps();
        });

        Schema::create('actversion_commentlocal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('actversion_id')->unsigned();
            $table->foreign('actversion_id')->references('id')->on('actversions')->onDelete('cascade');
            $table->integer('commentlocal_id')->unsigned();
            $table->foreign('commentlocal_id')->references('id')->on('commentlocals')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actversion_commentlocal');
        Schema::dropIfExists('commentlocals');
        Schema::dropIfExists('commentnros');
        Schema::dropIfExists('cauthor_comment');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('csources');
        Schema::dropIfExists('cdocsubtypes');
        Schema::dropIfExists('cauthor_cauthorrole');
        Schema::dropIfExists('cauthorroles');
        Schema::dropIfExists('cauthors');
    }
}
