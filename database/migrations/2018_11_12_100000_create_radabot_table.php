<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadabotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('racts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nreg');
            $table->text('name');
            $table->integer('rsubact_id')->unsigned()->default(1);
            $table->integer('rstatus_id')->unsigned()->default(0);
            $table->text('typ');
            $table->text('organs');
            $table->integer('nro')->default('0')->unsigned();
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
            $table->integer('rbotstatus_id')->unsigned()->default(0);
            $table->longtext('rmeta');
            $table->timestamps();
        });

        Schema::create('ractversions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('act_id')->unsigned();
            $table->foreign('act_id')->references('id')->on('racts')->onDelete('cascade');
            $table->string('ed');
            $table->integer('versionid')->default(1);
            $table->boolean('export')->default('false');
            $table->boolean('exported')->default('false');
            $table->integer('rbotstatus_id')->unsigned()->default(0);
            $table->longtext('rmeta');
            $table->longtext('rcontent');
            $table->longtext('content');
            $table->longtext('pmeta');
            $table->longtext('pcontent');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ractversions');
        Schema::dropIfExists('racts');
    }
}
