<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pauthors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('pauthorroles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortname');
        });

        Schema::create('pauthor_pauthorrole', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pauthor_id')->unsigned();
            $table->foreign('pauthor_id')->references('id')->on('pauthors')->onDelete('cascade');
            $table->integer('pauthorrole_id')->unsigned();
            $table->foreign('pauthorrole_id')->references('id')->on('pauthorroles')->onDelete('cascade');
        });

        Schema::create('pglosstypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('psources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortname');
        });

        Schema::create('pdocsubtypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('novelty_date');
            $table->integer('pdocsubtype_id')->unsigned();
            $table->foreign('pdocsubtype_id')->references('id')->on('pdocsubtypes')->onDelete('cascade');
            $table->integer('pglosstype_id')->unsigned();
            $table->foreign('pglosstype_id')->references('id')->on('pglosstypes')->onDelete('cascade');
            $table->string('subtitle')->default('');
            $table->longtext('abstracttext');
            $table->integer('psource_id')->unsigned()->default(0);
            $table->integer('year')->default(0);
            $table->integer('issuefrom')->default(0);
            $table->integer('issueto')->default(0);
            $table->integer('position')->default(0);
            $table->string('pdflink')->default('');
            $table->longtext('content');
            $table->timestamps();
        });

        Schema::create('pauthor_publication', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pauthor_id')->unsigned();
            $table->foreign('pauthor_id')->references('id')->on('pauthors')->onDelete('cascade');
            $table->integer('publication_id')->unsigned();
            $table->foreign('publication_id')->references('id')->on('publications')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pauthor_publication');
        Schema::dropIfExists('publications');
        Schema::dropIfExists('pdocsubtypes');
        Schema::dropIfExists('psources');
        Schema::dropIfExists('pglosstypes');
        Schema::dropIfExists('pauthor_pauthorrole');
        Schema::dropIfExists('pauthorroles');
        Schema::dropIfExists('pauthors');
    }
}
