<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportvocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exportvocs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('vocabulary_production_type')->default('');
            $table->string('type');
            $table->integer('factor')->default('0')->unsigned();
        });
        Schema::create('exportvocmodels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('exportvoc_id')->unsigned();
            $table->foreign('exportvoc_id')->references('id')->on('exportvocs')->onDelete('cascade');
            $table->string('model')->default('');
            $table->integer('factor')->default('0')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exportvocmodels');
        Schema::dropIfExists('exportvocs');
    }
}
