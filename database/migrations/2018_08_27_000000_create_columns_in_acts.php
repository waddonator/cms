<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsInActs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('acts', function (Blueprint $table) {
            $table->integer('trash')->default('0');
        });

        Schema::table('actversions', function (Blueprint $table) {
            $table->integer('trash')->default('0');
            $table->integer('format')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('actversions', function (Blueprint $table) {
            $table->dropColumn('format');
            $table->dropColumn('trash');
        });
        Schema::table('acts', function (Blueprint $table) {
            $table->dropColumn('trash');
        });
    }
}
