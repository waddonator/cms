<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('ractions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('url')->default('');
            $table->integer('sec')->default('0')->unsigned();
            $table->text('response_header');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ractions');
    }
}
