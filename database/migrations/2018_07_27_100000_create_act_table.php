<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asubacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('akeywords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('acts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->integer('atype_id')->unsigned();
            $table->foreign('atype_id')->references('id')->on('types')->onDelete('cascade');
            $table->integer('asubact_id')->unsigned();
            $table->foreign('asubact_id')->references('id')->on('asubacts')->onDelete('cascade');
            $table->string('number')->default('');
            $table->timestamp('date_acc')->nullable();
            $table->timestamp('date_force')->nullable();
            $table->timestamp('reg_date')->nullable();
            $table->string('reg_number')->default('')->nullable();
            $table->string('reestr_code')->default('')->nullable();
            $table->timestamp('reestr_date')->nullable();
            $table->integer('finisher_id')->unsigned()->default(0);
            $table->timestamp('finish_date')->nullable();
            $table->timestamps();
        });
        Schema::create('actversions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('act_id')->unsigned();
            $table->foreign('act_id')->references('id')->on('acts')->onDelete('cascade');
            $table->timestamp('date_acc')->nullable();
            $table->integer('modifier_id')->unsigned()->default(0);
            $table->longtext('content');
            $table->timestamps();
        });
        Schema::create('act_publisher', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('act_id')->unsigned();
            $table->foreign('act_id')->references('id')->on('acts')->onDelete('cascade');
            $table->integer('publisher_id')->unsigned();
            $table->foreign('publisher_id')->references('id')->on('publishers')->onDelete('cascade');
        });
        Schema::create('act_source', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('act_id')->unsigned();
            $table->foreign('act_id')->references('id')->on('acts')->onDelete('cascade');
            $table->integer('source_id')->unsigned();
            $table->foreign('source_id')->references('id')->on('sources')->onDelete('cascade');
            $table->string('string')->default('')->nullable();
            $table->timestamp('announced_date')->nullable();
            $table->integer('issue_from')->unsigned()->nullable();
            $table->integer('position')->unsigned()->nullable();
        });
        Schema::create('act_akeyword', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('act_id')->unsigned();
            $table->foreign('act_id')->references('id')->on('acts')->onDelete('cascade');
            $table->integer('akeyword_id')->unsigned();
            $table->foreign('akeyword_id')->references('id')->on('akeywords')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('act_akeyword');
        Schema::dropIfExists('act_source');
        Schema::dropIfExists('act_publisher');
        Schema::dropIfExists('actversions');
        Schema::dropIfExists('acts');
        Schema::dropIfExists('akeywords');
        Schema::dropIfExists('asubacts');
    }
}
