<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ncats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamp('date_effective')->nullable();
            $table->timestamp('date_event')->nullable();
            $table->boolean('important')->default('false');
            $table->boolean('trash')->default('false');
            $table->boolean('export')->default('false');
            $table->integer('nro')->default('0')->unsigned();
            $table->longtext('content');
            $table->timestamps();
        });
        Schema::create('ncat_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ncat_id')->unsigned();
            $table->foreign('ncat_id')->references('id')->on('ncats')->onDelete('cascade');
            $table->integer('news_id')->unsigned();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ncat_news');
        Schema::dropIfExists('news');
        Schema::dropIfExists('ncats');
    }
}
