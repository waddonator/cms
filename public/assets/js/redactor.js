$(function(){
    $(document).ready(function(){
        var t=document.createElement("textarea");
        $(t).addClass("source").attr("rows","20").attr("cols","70").css("display","block").css("margin","0 0 20px 0").css("width","100%").appendTo($(".redactor"));
        var e=document.createElement("input");
        $(e).addClass("generate").attr("type","button").attr("value","Generate").on("click",function(){
            chapel.init(chapel);
            content = $(".source").val().replace(/style=\\?"[^"]+"/gi,"").replace(/class=\\?"[^"]+"/gi,'class=""').replace(/id=\\?"[^"]+"/gi,'id=""');
            var t= content.replace(/<\/?par[^>]*>/gi,""),e=/(\<\w*)((\s\/\>)|(.*\<\/\w*\>))*/gm.test(t),a=e?t:chapel.text.normalize(t);
            $(".source").val(a);
            // $.ajax({
            //  url:"redactor/rpc.php",
            //  type:"POST",
            //  data:{
            //      text:'<!DOCTYPE html><html><head><meta http-equiv=Content-Type content="text/html;charset=utf-8"></head><body>'+a+"</body></html>"},
            //      success:function(t){
            //          var a=t.replace(/<html\s+xmlns=[^>]+>/i,'<html xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.w3.org/1999/xhtml" xsi:schemaLocation="http://www.w3.org/1999/xhtml ../XSD/andro-xhtml.xsd">');
            //          e&&(a=(a=(a=a.replace(/style=\\?"[^"]+"/gi,"")).replace(/class=\\?"[^"]+"/gi,'class=""')).replace(/id=\\?"[^"]+"/gi,'id=""')),$(".source").val(a)
            //  },
            //  error:function(t){
            //      console.log(t.responseText)
            //  }
            // })
        }).appendTo($(".redactor"))
    })
});
