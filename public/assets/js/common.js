$(document).ready(function(){
    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)',
        skyblue: 'rgb(135, 206, 235)',
    };

    $(".hexa-minimize").click(function(e){
        e.preventDefault();
        $(this).parents(".panel-heading").siblings(".panel-body").slideToggle("fast");
        $(this).parents(".card-header").siblings(".card-body").slideToggle("fast");
        $(this).parents(".card-header").siblings(".card-footer").slideToggle("fast");
    });

    $(".hexa-check").click(function(e){
        e.preventDefault();
        $(this).parents(".panel-heading").siblings(".panel-body").find("[type='checkbox']").prop("checked",true);
        $(this).parents(".card-header").siblings(".card-body").find("[type='checkbox']").prop("checked",true);
    });

    $(".hexa-uncheck").click(function(e){
        e.preventDefault();
        $(this).parents(".card-header").siblings(".card-body").find("[type='checkbox']").prop("checked",false);
    });

    $(".hexa-export").click(function(e){
        e.preventDefault();
        var table = $(this).parents('.card-header').siblings('.card-body').find('table').first();
        var content = '<thead><tr><th>Column</th></tr></thead><tbody><tr><td>Content</td></tr></tbody>';
        if (table) {content = table.html();}
        $("#test_table").html(content);
        excel = new ExcelGen({
            "src_id": "test_table",
            "show_header": true
        });
        excel.generate();
    });

    /* Удаление класса "is-invalid" при изменении знаяения у удаление сообщения об ошибке */
    $(".is-invalid").change(function(){
        $(this).siblings(".invalid-feedback").remove();
        $(this).removeClass('is-invalid');
        $("[name='"+$(this).attr('name')+"']").removeClass('is-invalid');
        $(this).parents(".custom-control").siblings(".invalid-feedback").remove();
    });

    /* Передача урла в модальное окно удаления */
    $(".btn-remove").click(function(){
        $("#remove-form").attr('action',$(this).attr('href'));
    });
    $(".btn-adduser").click(function(){
        $("#adduser-form").attr('action',$(this).attr('href'));
    });
    $(".btn-send").click(function(){
        $("#send-form").attr('action',$(this).attr('href'));
    });

    /* Установка класса "Active" для активного меню и откыртие родительских аккордеонов */
    // var url = window.location.origin + window.location.pathname;
    // var arr = window.location.pathname.split('/');
    //console.log(arr[1]);
    //$('[data-url="'+arr[1]+'"]').addClass('active');
    // var element = $('ul.navbar-nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent();
    // element.parents('ul').addClass('show');

    // $('a.active').parents('ul').addClass('show');

});
