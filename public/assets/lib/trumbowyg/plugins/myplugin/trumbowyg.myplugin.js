(function ($) {
    'use strict';

    // My plugin default options
    var defaultOptions = {
    };

    // If my plugin is a button
    function buildButtonDef(trumbowyg) {
        return {
            fn: function() {
                // действия при нажатии кнопки

                // получаем весь код
                    var new_content = '';
                    var content = trumbowyg.html();

                // убираем стили, переносы строк и пробелы между тегами
                    content = content.replace( /\sstyle=".*?"/g, '');
                    content = content.replace( /\r?\n/g, '');
                    content = content.replace( /&nbsp;/g, ' ');
                    content = content.replace( /(>)[ ]+?(<)/g, "$1$2");

                // убираем пустые элементы div и p
                    content = content.replace( /<div[^<]*?><\/div>/g, '');
                    content = content.replace( /<p><\/p>/g, '');

                // убираем таблицу с гербом
                    content = content.replace( /<table.*?img.*?table>/g, '');

                // заменяем таблицы на hexa-метки и запоминаем их в массив
                    var tables = content.match(/<table.*?table>/g);
                    content = content.replace( /<table.*?table>/g, '[hexatable]');
                    console.log(tables);

                // заменяем абзацы на hexa-метки и запоминаем их в массив
                    var texts = content.match(/<p.*?<a\sname="n[0-9]+?"><\/a>.*?<\/p>/g);
                    content = content.replace( /<p.*?<a\sname="n[0-9]+?"><\/a>.*?<\/p>/g, '[hexatext]');
                // заменяем линии hr на метки
                    content = content.replace( /<hr[^>]*?>/g, '[hexahr]');

                // вытаскиваем только hexa-метки и формируем новый контент из них
                    var hexa_content = content.match(/\[hexa[^\]]*?\]/g);
                    console.log(hexa_content);
                    var ta = 0;
                    var te = 0;
                    var temp = '';
                    $.each( hexa_content, function( key, value ) {
                        if (value=='[hexatable]') {
                            temp = tables[ta].replace(/rvps15/g,'text-right');
                            temp = temp.replace(/rvps12/g,'text-center');
                            ta++;
                        }
                        if (value=='[hexatext]') {
                            var temp_id = texts[te].match(/<a\sname="(n[0-9]+?)"><\/a>/);
                            var temp = texts[te].replace( /<p\s[^>]*?>/, '<p id="'+temp_id[1]+'" class="a_par toc unit-menu">'); // формируем абзац в формате ТОС
                            temp = temp.replace( /<a\sname="(n[0-9]+?)"><\/a>/, '<span class="unit-menu__btn"><i class="fa fa-plus-circle"></i></span>'); // меняем пустую ссылку на кружочек
                            te++;
                        }
                        if (value=='[hexahr]') {
                            temp = '<hr>';
                        }

                        new_content += temp;
                    });

                // установить новый контент
                    trumbowyg.html(new_content);

            }
        }
    }

    $.extend(true, $.trumbowyg, {
        // Add some translations
        langs: {
            en: {
                myplugin: 'TOC структура'
            }
        },
        // Add our plugin to Trumbowyg registred plugins
        plugins: {
            myplugin: {
                init: function(trumbowyg) {
                    // Fill current Trumbowyg instance with my plugin default options
                    trumbowyg.o.plugins.myplugin = $.extend(true, {},
                        defaultOptions,
                        trumbowyg.o.plugins.myplugin || {}
                    );

                    // If my plugin is a paste handler, register it
                    trumbowyg.pasteHandlers.push(function(pasteEvent) {
                        buildButtonDef(trumbowyg);
                        //alert('paste');
                        // My plugin paste logic
                    });

                    // If my plugin is a button
                    trumbowyg.addBtnDef('myplugin', buildButtonDef(trumbowyg));
                },
                tagHandler: function(element, trumbowyg) {
                    return [];
                },
                destroy: function() {
                }
            }
        }
    })
})(jQuery);