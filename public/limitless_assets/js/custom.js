/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */

$(document).ready(function(){

        // Logout dialog
        $('#logout').on('click', function(e) {
            e.preventDefault();
            bootbox.confirm({
                title: 'Выход',
                message: 'Вы уверены, что хотите выйти из системы?',
                buttons: {
                    confirm: {
                        label: 'Выход',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Отмена',
                        className: 'btn-link'
                    }
                },
                callback: function (result) {
                    if (result){
                        document.getElementById('logout-form').submit();
                    }
                }
            });
        });
	/* Фильтрование */
    $('.btn-filter').click(function(e){
        e.preventDefault();
        document.getElementById('filter-form').submit();
    });
    /* Удаление */
    $('.btn-remove').click(function(e){
        e.preventDefault();
        $("#remove-form").attr('action',$(this).attr('href'));
    });
    $('.btn-trash').click(function(e){
        e.preventDefault();
        $("#trash-form").attr('action',$(this).attr('href'));
    });
    $('.btn-restore').click(function(e){
        e.preventDefault();
        $("#restore-form").attr('action',$(this).attr('href'));
    });

    /* Открытие родительских аккордеонов */
    // $('.nav-link.active').parents('.nav-item-submenu').addClass('nav-item-open');
    // $('.nav-link.active').parents('.nav-group-sub').show();

    /* Скролинг вверх */
    $(window).scroll(function() {
        if($(this).scrollTop() != 0) { 
            $('#GotoTop').fadeIn(); 
        } else { 
            $('#GotoTop').fadeOut(); 
        }
    });

    $('#GotoTop').click(function() {
        $('body,html').animate({scrollTop:0},800);
    });

    /* Удаление класса "is-invalid" при изменении знаяения у удаление сообщения об ошибке */
    $(".is-invalid").change(function(){
        $(this).siblings(".invalid-feedback").remove();
        $(this).removeClass('is-invalid');
        $("[name='"+$(this).attr('name')+"']").removeClass('is-invalid');
        $(this).parents(".custom-control").siblings(".invalid-feedback").remove();
    });

    
});

